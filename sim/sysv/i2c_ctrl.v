
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

module i2c_ctrl
    #(
        parameter real       CLK_RATE_HZ  = 100e6,
        parameter real       SCL_RATE_HZ  = 100e3,
        parameter int        DADR_BYTE    = 'b0111_1110,
        parameter bit        IRQ_ENBL     = 1'b1,
        parameter bit        TARG_ENBL    = 1'b1,
        parameter bit [15:0] PRESCALE     = 16'($rtoi((CLK_RATE_HZ / (5.0 * SCL_RATE_HZ)) - 1.0))
    )
    (
        output [7:0]  data_o,
        output [7:0]  stat_o,

        input  [7:0]  cmdr_data_i,
        input         cmdr_strb_i,
        input  [7:0]  txdr_data_i,
        input         txdr_strb_i,

        input         scl_i,
        output        scl_o,
        input         sda_i,
        output        sda_o,

        input         clk_i,
        input         rst_async_i,
        input         rst_sync_i
    );


    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    localparam bit FALSE = 1'b0;
    localparam bit TRUE  = 1'b1;


    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    wire        byte_ack_out;
    wire        byte_cmd_ack;
    wire [7:0]  byte_dout;
    wire        byte_ctrl_alost;
    wire        byte_ctrl_busy;
    wire        byte_targ_activ;
    wire        byte_targ_done;
    wire        byte_targ_dat_av;
    wire        byte_targ_dat_rq;

    bit         stat_7_rxack;
    bit         stat_5_alost;
    bit         stat_4_targ_mode;
    bit         stat_3_targ_dat_av;
    bit         stat_2_targ_dat_req;
    bit         stat_1_tip;
    bit         stat_0_irq;

    bit  [7:0]  reg_cmdr;
    bit  [7:0]  reg_txdr;


    //- Input Assignment -----------------------------------------------------
    // Command register
    // [7] STA  - Transaction start
    // [6] ST0  - Transaction stop
    // [5] RD   - Transaction read
    // [4] WR   - Transaction write
    // [3] ACK  - Transaction acknowledge
    // [2] RSVD
    // [1] TACK - Target acknowledge
    // [0] IACK - Interrupt acknowledge
    wire        cmdr_7_sta          = reg_cmdr[7];
    wire        cmdr_6_sto          = reg_cmdr[6];
    wire        cmdr_5_rd           = reg_cmdr[5];
    wire        cmdr_4_wr           = reg_cmdr[4];
    wire        cmdr_3_ack          = reg_cmdr[3];
    wire        cmdr_2_rsvd         = reg_cmdr[2];
    wire        cmdr_1_tack         = reg_cmdr[1];
    wire        cmdr_0_iack         = reg_cmdr[0];

    // Status register
    // [7] RxACK
    //         1 - NAK
    //         0 - ACK
    // [6] Controller busy
    //         1 - After STA
    //         0 - After STO
    // [5] Controller arb lost
    //         1 - When one of the following
    //             * Unrequested STO
    //             * SDA remains low after Controller stops driving it
    //         0 - Otherwise
    // [4] Target mode
    // [3] Target data available
    // [2] Target data request
    // [1] Transfer in progress
    //         1 - Transfering data
    //         0 - Transfer complete
    // [0] Interrupt flag
    //         1 - Interrupt pending when
    //             * Byte transfer complete
    //             * Arbitration lost
    //         0 - Otherwise
    //
    wire       stat_6_busy = byte_ctrl_busy;
    wire [7:0] reg_stat = {
        stat_7_rxack,
        stat_6_busy,
        stat_5_alost,
        stat_4_targ_mode,
        stat_3_targ_dat_av,
        stat_2_targ_dat_req,
        stat_1_tip,
        stat_0_irq
    };


    //- Task and Function: impure --------------------------------------------
    function bit set_irq();
        bit ret = FALSE;

        if (IRQ_ENBL) begin
            ret = ret | byte_cmd_ack;
            ret = ret | byte_ctrl_alost;
            ret = ret | byte_targ_done;
            ret = ret | byte_targ_dat_av;
            ret = ret | byte_targ_dat_rq;
        end

        return ret;
    endfunction

    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    i2c_master_byte_ctrl i_byte
    (
        .ack_in             ( cmdr_3_ack           ), // i
        .ack_out            ( byte_ack_out         ), // o
        .clk                ( clk_i                ), // i
        .clk_cnt            ( PRESCALE             ), // i [15:0]
        .cmd_ack            ( byte_cmd_ack         ), // o
        .din                ( reg_txdr[7:0]        ), // i [7:0]
        .dout               ( byte_dout[7:0]       ), // o [7:0]
        .ena                ( TRUE                 ), // i
        .i2c_al             ( byte_ctrl_alost      ), // o
        .i2c_busy           ( byte_ctrl_busy       ), // o
        .my_addr            ( DADR_BYTE[7:1]       ), // i [6:0]
        .nReset             ( rst_async_i          ), // i
        .read               ( cmdr_5_rd            ), // i
        .rst                ( rst_sync_i           ), // i
        .scl_i              ( scl_i                ), // i
        .scl_o              ( /* NC */             ), // o
        .scl_oen            ( scl_o                ), // o
        .sda_i              ( sda_i                ), // i
        .sda_o              ( /* NC */             ), // o
        .sda_oen            ( sda_o                ), // o
        .sl_cont            ( cmdr_1_tack          ), // i
        .slave_act          ( byte_targ_activ      ), // o
        .slave_cmd_ack      ( byte_targ_done       ), // o
        .slave_dat_avail    ( byte_targ_dat_av     ), // o
        .slave_dat_req      ( byte_targ_dat_rq     ), // o
        .slave_en           ( TARG_ENBL            ), // i
        .start              ( cmdr_7_sta           ), // i
        .stop               ( cmdr_6_sto           ), // i
        .write              ( cmdr_4_wr            )  // i
    );

    //- RTL ------------------------------------------------------------------
    always @(posedge clk_i) begin
        if (rst_sync_i) begin
            stat_7_rxack        <= FALSE;
            stat_5_alost        <= FALSE;
            stat_4_targ_mode    <= FALSE;
            stat_3_targ_dat_av  <= FALSE;
            stat_2_targ_dat_req <= FALSE;
            stat_0_irq          <= FALSE;
            stat_1_tip          <= FALSE;
            reg_cmdr[7:0]       <= '0;
            reg_txdr[7:0]       <= '0;
        end else begin
            stat_7_rxack        <= byte_ack_out;
            stat_5_alost        <= byte_ctrl_alost | (stat_5_alost & (~cmdr_7_sta));
            stat_3_targ_dat_av  <= byte_targ_dat_av;
            stat_2_targ_dat_req <= byte_targ_dat_rq;
            stat_1_tip          <= cmdr_5_rd | cmdr_4_wr;

            if (byte_cmd_ack) begin
                stat_4_targ_mode <= byte_targ_activ;
            end

            if (stat_0_irq) begin
                if (cmdr_0_iack) begin
                    stat_0_irq <= FALSE;
                end
            end else begin
                stat_0_irq <= set_irq();
            end

            // reg_cmdr upper bits
            if (byte_cmd_ack | byte_ctrl_alost) begin
                reg_cmdr[7:4] <= '0;
            end else if (cmdr_strb_i) begin
                reg_cmdr[7:4] <= cmdr_data_i[7:4];
            end

            // reg_cmdr lower bits
            if (cmdr_strb_i) begin
                reg_cmdr[3]   <= cmdr_data_i[3];
                reg_cmdr[2:0] <= cmdr_data_i[2:0]; // was ..._i[3:0]
            end else begin
                reg_cmdr[2:0] <= '0;
            end
            
            if (txdr_strb_i) begin
                reg_txdr[7:0] <= txdr_data_i[7:0];
            end
        end
    end


    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    assign data_o[7:0] = byte_dout[7:0];
    assign stat_o[7:0] = reg_stat[7:0];


    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------
endmodule

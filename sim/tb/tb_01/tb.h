
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TB_H_
    #define _TB_H_

    #define SC_INCLUDE_DYNAMIC_PROCESSES

    #include <string>
    #include <systemc>
    #include <tb_base.h>
    #include <tb_msg.h>
    #include <tb_clk.h>
    #include <tb_rst.h>
    #include <tb_timeout.h>
    #include <tb_rptcyc.h>
    #include <tb_and.h>
    #include <tb_i2c_ctrl_spec.h>
    #include <tb_i2c_ctrl.h>
    #include <tb_i2c_ram_256x8.h>
    #include <Vtb_top.h>
    #include <verilated_cov.h>

    namespace tb
    {
        typedef std::unique_ptr<Vtb_top> pVtb_top;
        typedef std::unique_ptr<And<4>>  pAnd4;

        namespace Parameters
        {
            const uint32_t I2CC_FIFO_COUNT = 10;
        }

        class tb : public sc_core::sc_module, public Base
        {
            private:
                const std::string COVERAGE_FNAM = "coverage.dat";

                enum t_i2c_sel : int
                {
                    CTLR_11,
                    CTLR_12,
                    TARG_11,
                    TARG_12
                };

            public:
                pVtb_top            i_tb;
                pClk                i_clk;
                pRst                i_rst;
                pTimeout            i_to;
                pRptCyc             i_cyc;
                pI2C_RAM_256x8      i_ram1;
                pI2C_RAM_256x8      i_ram2;
                pI2C_Ctrl           i_i2cc1;
                pI2C_Ctrl           i_i2cc2;
                pAnd4               i_sda;
                pAnd4               i_scl;

                pI2CC_FIFO            i_i2cc_fifo[Parameters::I2CC_FIFO_COUNT];

                sc_signal <bool>      i2c_sdas[4];
                sc_signal <bool>      i2c_scls[4];

                sc_signal <bool>      i2c_sda;
                sc_signal <bool>      i2c_scl;

                sc_signal <uint32_t>  ctlr_11_data;
                sc_signal <uint32_t>  ctlr_11_stat;
                sc_signal <uint32_t>  ctlr_11_cmdr_data;
                sc_signal <bool>      ctlr_11_cmdr_strb;
                sc_signal <uint32_t>  ctlr_11_txdr_data;
                sc_signal <bool>      ctlr_11_txdr_strb;

                sc_signal <uint32_t>  ctlr_12_data;
                sc_signal <uint32_t>  ctlr_12_stat;
                sc_signal <uint32_t>  ctlr_12_cmdr_data;
                sc_signal <bool>      ctlr_12_cmdr_strb;
                sc_signal <uint32_t>  ctlr_12_txdr_data;
                sc_signal <bool>      ctlr_12_txdr_strb;

                sc_signal <bool>      tb_rst;
                sc_signal <bool>      tb_clk;

                SC_HAS_PROCESS(tb);

                //-- Constructor/Destructor ------------------------------------
                tb(sc_core::sc_module_name);
                ~tb(void);

                //-- Virtual ---------------------------------------------------
                virtual void before_end_of_elaboration(void);
                virtual void end_of_elaboration(void);
                virtual void vltr_tracing(void);
                virtual void cleanup(void);

                //-- Top-level Thread ------------------------------------------
                void tb_run(void);
                void tb_stop(void);

                //-- Tests -----------------------------------------------------
                bool test_start(const pMsg &arg_msg);
                bool test_end(const pMsg &arg_msg);
                bool test_debug(const pMsg &arg_msg, const std::string &arg_sel);
                bool test_dadr_nack(const pMsg &arg_msg, const std::string &arg_sel);
                bool subtest_rdwr_rand(const pMsg &arg_msg, const std::string &arg_sel, const pI2C_Ctrl &arg_ctlr);
                bool test_rdwr_sys(const pMsg &arg_msg, const std::string &arg_sel);
                void subtest_arbitration(const pMsg &arg_msg, const std::string &arg_sel, const pI2C_Ctrl &arg_ctlr);
                bool test_arbitration(const pMsg &arg_msg, const std::string &arg_sel);
                bool test_i2cc_targ(const pMsg &arg_msg, const std::string &arg_sel);
                bool regression(const pMsg &arg_msg, const std::string &arg_sel);
        };
    }
#endif


// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

module tb_top
    #(
        parameter real CLK_RATE_HZ  = 100e6,
        parameter real SCL_RATE_HZ  = 800e3,
        parameter int  DADR_BYTE_C1 = 'b0111_0110,
        parameter int  DADR_BYTE_C2 = 'b0111_1110
    )
    (
        output [7:0]  ctlr_11_data_o,
        output [7:0]  ctlr_11_stat_o,
        input  [7:0]  ctlr_11_cmdr_data_i,
        input         ctlr_11_cmdr_strb_i,
        input  [7:0]  ctlr_11_txdr_data_i,
        input         ctlr_11_txdr_strb_i,
        output        ctlr_11_scl_o,
        input         ctlr_11_scl_i,
        output        ctlr_11_sda_o,
        input         ctlr_11_sda_i,

        output [7:0]  ctlr_12_data_o,
        output [7:0]  ctlr_12_stat_o,
        input  [7:0]  ctlr_12_cmdr_data_i,
        input         ctlr_12_cmdr_strb_i,
        input  [7:0]  ctlr_12_txdr_data_i,
        input         ctlr_12_txdr_strb_i,
        output        ctlr_12_scl_o,
        input         ctlr_12_scl_i,
        output        ctlr_12_sda_o,
        input         ctlr_12_sda_i,

        input         clk_i,
        input         rst_i
    );

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    logic [3:0] rst_sync;

    wire        i2c_1_scl;
    wire        i2c_1_sda;

    wire        ctrl_11_scl;
    wire        ctrl_11_sda;

    wire        ctrl_12_scl;
    wire        ctrl_12_sda;


    //- Input Assignment -----------------------------------------------------
    wire  rst_async_not = ~rst_i;

    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    i2c_ctrl
    #(
        .CLK_RATE_HZ ( CLK_RATE_HZ  ),
        .SCL_RATE_HZ ( SCL_RATE_HZ  ),
        .DADR_BYTE   ( DADR_BYTE_C1 )
    )
    i_ctrl_11
    (
        .data_o      ( ctlr_11_data_o[7:0]       ), // [7:0]
        .stat_o      ( ctlr_11_stat_o[7:0]       ), // [7:0]
        .cmdr_data_i ( ctlr_11_cmdr_data_i[7:0]  ), // [7:0]
        .cmdr_strb_i ( ctlr_11_cmdr_strb_i       ), //
        .txdr_data_i ( ctlr_11_txdr_data_i[7:0]  ), // [7:0]
        .txdr_strb_i ( ctlr_11_txdr_strb_i       ), //
        .scl_i       ( ctlr_11_scl_i             ), //
        .scl_o       ( ctlr_11_scl_o             ), //
        .sda_i       ( ctlr_11_sda_i             ), //
        .sda_o       ( ctlr_11_sda_o             ), //
        .clk_i       ( clk_i                     ), //
        .rst_async_i ( rst_async_not             ), //
        .rst_sync_i  ( rst_sync[3]               )  //
    );

    i2c_ctrl
    #(
        .CLK_RATE_HZ ( CLK_RATE_HZ  ),
        .SCL_RATE_HZ ( SCL_RATE_HZ  ),
        .DADR_BYTE   ( DADR_BYTE_C2 )
    )
    i_ctrl_12
    (
        .data_o      ( ctlr_12_data_o[7:0]       ), // [7:0]
        .stat_o      ( ctlr_12_stat_o[7:0]       ), // [7:0]
        .cmdr_data_i ( ctlr_12_cmdr_data_i[7:0]  ), // [7:0]
        .cmdr_strb_i ( ctlr_12_cmdr_strb_i       ), //
        .txdr_data_i ( ctlr_12_txdr_data_i[7:0]  ), // [7:0]
        .txdr_strb_i ( ctlr_12_txdr_strb_i       ), //
        .scl_i       ( ctlr_12_scl_i             ), //
        .scl_o       ( ctlr_12_scl_o             ), //
        .sda_i       ( ctlr_12_sda_i             ), //
        .sda_o       ( ctlr_12_sda_o             ), //
        .clk_i       ( clk_i                     ), //
        .rst_async_i ( rst_async_not             ), //
        .rst_sync_i  ( rst_sync[3]               )  //
    );


    //- RTL ------------------------------------------------------------------
    always @(posedge clk_i or negedge rst_async_not) begin
        if (rst_async_not == 1'b0) begin
            rst_sync[3:0] = '1;
        end else begin
            rst_sync[3:0] = {rst_sync[2:0], 1'b0};
        end
    end


    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

endmodule

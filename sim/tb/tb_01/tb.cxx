
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;
    using namespace Parameters;

    //-- Constructor/Destructor ------------------------------------------------
    tb::tb(sc_module_name arg_nm) : Base()
    {
        // -- Get PlusArgs -----------------------------------------------------
        double   clk_rate       = get_plusarg_double("CLK_RATE_HZ");
        sc_time  time_needed    = sc_time(get_plusarg_double("TIME_NEEDED_SEC"), SC_SEC);
        sc_time  time_report    = sc_time(get_plusarg_double("TIME_REPORT_SEC"), SC_SEC);
        uint32_t dadr_i2cc1     = get_plusarg_int("DADR_BYTE_C1");
        uint32_t dadr_i2cc2     = get_plusarg_int("DADR_BYTE_C2");
        uint32_t dadr_ram1      = get_plusarg_int("DADR_BYTE_T1");
        uint32_t dadr_ram2      = get_plusarg_int("DADR_BYTE_T2");
        uint32_t scl_rand_ram1  = get_plusarg_int("SCL_STRETCH_T1");
        uint32_t scl_rand_ram2  = get_plusarg_int("SCL_STRETCH_T2");

        // -- Instantiation ----------------------------------------------------
        this->msg       = pMsg(new Msg(this->name()));
        this->i_clk     = pClk(new Clk("i_clk", clk_rate, 0.5, sc_time(1.0, SC_NS), true));
        this->i_rst     = pRst(new Rst("i_rst", true));
        this->i_to      = pTimeout(new Timeout("i_to", time_needed));
        this->i_cyc     = pRptCyc(new RptCyc("i_cyc", time_report));
        this->i_ram1    = pI2C_RAM_256x8(new I2C_RAM_256x8("i_ram1", dadr_ram1, scl_rand_ram1));
        this->i_ram2    = pI2C_RAM_256x8(new I2C_RAM_256x8("i_ram2", dadr_ram2, scl_rand_ram2));
        this->i_i2cc1   = pI2C_Ctrl(new I2C_Ctrl("i_i2cc1", dadr_i2cc1, I2CC_FIFO_COUNT));
        this->i_i2cc2   = pI2C_Ctrl(new I2C_Ctrl("i_i2cc2", dadr_i2cc2, I2CC_FIFO_COUNT));
        this->i_sda     = pAnd4(new And<4>("i_sda"));
        this->i_scl     = pAnd4(new And<4>("i_scl"));
        this->i_tb      = pVtb_top(new Vtb_top("i_tb"));

        for (int i = 0 ; i < I2CC_FIFO_COUNT ; i++)
        {
            this->i_i2cc_fifo[i] = pI2CC_FIFO(new I2CC_FIFO(1));
        }

        // -- Interconnect -----------------------------------------------------
        this->i_clk->clk_o ( this->tb_clk );
        this->i_rst->rst_o ( this->tb_rst );

        this->i_tb->ctlr_11_data_o      ( this->ctlr_11_data       );
        this->i_tb->ctlr_11_stat_o      ( this->ctlr_11_stat       );
        this->i_tb->ctlr_11_cmdr_data_i ( this->ctlr_11_cmdr_data  );
        this->i_tb->ctlr_11_cmdr_strb_i ( this->ctlr_11_cmdr_strb  );
        this->i_tb->ctlr_11_txdr_data_i ( this->ctlr_11_txdr_data  );
        this->i_tb->ctlr_11_txdr_strb_i ( this->ctlr_11_txdr_strb  );
        this->i_tb->ctlr_11_sda_o       ( this->i2c_sdas[CTLR_11]  );
        this->i_tb->ctlr_11_sda_i       ( this->i2c_sda            );
        this->i_tb->ctlr_11_scl_o       ( this->i2c_scls[CTLR_11]  );
        this->i_tb->ctlr_11_scl_i       ( this->i2c_scl            );
        this->i_tb->ctlr_12_data_o      ( this->ctlr_12_data       );
        this->i_tb->ctlr_12_stat_o      ( this->ctlr_12_stat       );
        this->i_tb->ctlr_12_cmdr_data_i ( this->ctlr_12_cmdr_data  );
        this->i_tb->ctlr_12_cmdr_strb_i ( this->ctlr_12_cmdr_strb  );
        this->i_tb->ctlr_12_txdr_data_i ( this->ctlr_12_txdr_data  );
        this->i_tb->ctlr_12_txdr_strb_i ( this->ctlr_12_txdr_strb  );
        this->i_tb->ctlr_12_sda_o       ( this->i2c_sdas[CTLR_12]  );
        this->i_tb->ctlr_12_sda_i       ( this->i2c_sda            );
        this->i_tb->ctlr_12_scl_o       ( this->i2c_scls[CTLR_12]  );
        this->i_tb->ctlr_12_scl_i       ( this->i2c_scl            );
        this->i_tb->rst_i               ( this->tb_rst             );
        this->i_tb->clk_i               ( this->tb_clk             );

        this->i_i2cc1->cmdr_data_o      ( this->ctlr_11_cmdr_data  );
        this->i_i2cc1->cmdr_strb_o      ( this->ctlr_11_cmdr_strb  );
        this->i_i2cc1->txdr_data_o      ( this->ctlr_11_txdr_data  );
        this->i_i2cc1->txdr_strb_o      ( this->ctlr_11_txdr_strb  );
        this->i_i2cc1->data_i           ( this->ctlr_11_data       );
        this->i_i2cc1->stat_i           ( this->ctlr_11_stat       );
        this->i_i2cc1->clk_i            ( this->tb_clk             );

        this->i_i2cc2->cmdr_data_o      ( this->ctlr_12_cmdr_data  );
        this->i_i2cc2->cmdr_strb_o      ( this->ctlr_12_cmdr_strb  );
        this->i_i2cc2->txdr_data_o      ( this->ctlr_12_txdr_data  );
        this->i_i2cc2->txdr_strb_o      ( this->ctlr_12_txdr_strb  );
        this->i_i2cc2->data_i           ( this->ctlr_12_data       );
        this->i_i2cc2->stat_i           ( this->ctlr_12_stat       );
        this->i_i2cc2->clk_i            ( this->tb_clk             );

        this->i_ram1->sda_o             ( this->i2c_sdas[TARG_11]  );
        this->i_ram1->sda_i             ( this->i2c_sda            );
        this->i_ram1->scl_o             ( this->i2c_scls[TARG_11]  );
        this->i_ram1->scl_i             ( this->i2c_scl            );

        this->i_ram2->sda_o             ( this->i2c_sdas[TARG_12]  );
        this->i_ram2->sda_i             ( this->i2c_sda            );
        this->i_ram2->scl_o             ( this->i2c_scls[TARG_12]  );
        this->i_ram2->scl_i             ( this->i2c_scl            );

        this->i_sda->and_o              ( this->i2c_sda            );
        this->i_sda->vec_i[CTLR_11]     ( this->i2c_sdas[CTLR_11]  );
        this->i_sda->vec_i[CTLR_12]     ( this->i2c_sdas[CTLR_12]  );
        this->i_sda->vec_i[TARG_11]     ( this->i2c_sdas[TARG_11]  );
        this->i_sda->vec_i[TARG_12]     ( this->i2c_sdas[TARG_12]  );

        this->i_scl->and_o              ( this->i2c_scl            );
        this->i_scl->vec_i[CTLR_11]     ( this->i2c_scls[CTLR_11]  );
        this->i_scl->vec_i[CTLR_12]     ( this->i2c_scls[CTLR_12]  );
        this->i_scl->vec_i[TARG_11]     ( this->i2c_scls[TARG_11]  );
        this->i_scl->vec_i[TARG_12]     ( this->i2c_scls[TARG_12]  );

        // -- Start Threads ----------------------------------------------------
        SC_THREAD(tb_run);
    }

    tb::~tb(void) {}


    //-- Virtual ---------------------------------------------------------------
    void tb::before_end_of_elaboration(void)
    {
        string text = "tb::tb::before_end_of_elaboration()...";

        text = text+NL+this->msg->get_shift_string(1)+"__cplusplus is"+SP+to_string(__cplusplus);
        text = text+NL+this->msg->get_shift_string(1)+"SYSTEMC_VERSION is"+SP+to_string(SYSTEMC_VERSION);
        text = text+NL+this->msg->get_shift_string(1)+"unsigned is"+SP+to_string(sizeof(unsigned))+SP+"bytes";
        text = text+NL+this->msg->get_shift_string(1)+"unsigned long is"+SP+to_string(sizeof(unsigned long))+SP+"bytes";
        text = text+NL+this->msg->get_shift_string(1)+"unsigned long long is"+SP+to_string(sizeof(unsigned long long))+SP+"bytes";

        this->msg->inf(text);
        this->sysc_tracing_open();
        this->msg->inf("tb::tb::before_end_of_elaboration() done.");
    }

    void tb::end_of_elaboration(void)
    {
        this->msg->inf("tb::tb::end_of_elaboration()...");

        sc_trace(trace_sysc, this->tb_rst,            "tb_rst"            );
        sc_trace(trace_sysc, this->tb_clk,            "tb_clk"            );
        sc_trace(trace_sysc, this->i2c_sda,           "i2c_sda"           );
        sc_trace(trace_sysc, this->i2c_scl,           "i2c_scl"           );
        sc_trace(trace_sysc, this->i2c_sdas[0],       "i2c_sdas(0)"       );
        sc_trace(trace_sysc, this->i2c_sdas[1],       "i2c_sdas(1)"       );
        sc_trace(trace_sysc, this->i2c_sdas[2],       "i2c_sdas(2)"       );
        sc_trace(trace_sysc, this->i2c_sdas[3],       "i2c_sdas(3)"       );
        sc_trace(trace_sysc, this->i2c_scls[0],       "i2c_scls(0)"       );
        sc_trace(trace_sysc, this->i2c_scls[1],       "i2c_scls(1)"       );
        sc_trace(trace_sysc, this->i2c_scls[2],       "i2c_scls(2)"       );
        sc_trace(trace_sysc, this->i2c_scls[3],       "i2c_scls(3)"       );
        sc_trace(trace_sysc, this->ctlr_11_data,      "ctlr_11_data"      );
        sc_trace(trace_sysc, this->ctlr_11_stat,      "ctlr_11_stat"      );
        sc_trace(trace_sysc, this->ctlr_11_cmdr_data, "ctlr_11_cmdr_data" );
        sc_trace(trace_sysc, this->ctlr_11_cmdr_strb, "ctlr_11_cmdr_strb" );
        sc_trace(trace_sysc, this->ctlr_11_txdr_data, "ctlr_11_txdr_data" );
        sc_trace(trace_sysc, this->ctlr_11_txdr_strb, "ctlr_11_txdr_strb" );
        sc_trace(trace_sysc, this->ctlr_12_data,      "ctlr_12_data"      );
        sc_trace(trace_sysc, this->ctlr_12_stat,      "ctlr_12_stat"      );
        sc_trace(trace_sysc, this->ctlr_12_cmdr_data, "ctlr_12_cmdr_data" );
        sc_trace(trace_sysc, this->ctlr_12_cmdr_strb, "ctlr_12_cmdr_strb" );
        sc_trace(trace_sysc, this->ctlr_12_txdr_data, "ctlr_12_txdr_data" );
        sc_trace(trace_sysc, this->ctlr_12_txdr_strb, "ctlr_12_txdr_strb" );

        this->msg->inf("tb::tb::end_of_elaboration() done.");
    }

    void tb::vltr_tracing(void)
    {
        this->msg->inf("Verilator tracing setup...");
        this->i_tb->trace(this->tfp, 99);
        this->vltr_tracing_open();
        this->msg->inf("Verilator tracing ready...");
    }

    void tb::cleanup(void)
    {
        this->msg->inf("Final Verilator model cleanup...");
        this->i_tb->final();
        this->msg->inf("Verilator model cleaned.");
        this->vltr_tracing_close();
    }


    //-- Top-level Thread ------------------------------------------------------
    void tb::tb_run(void)
    {
        pMsg    msg = pMsg(new Msg(this->msg->get_context()));
        string  sel = get_plusarg("SEL");
        bool    run = false;

        msg->push_tag("run");

        if (sel.empty())
        {
            msg->inf("No test specified, requesting stop");
            this->tb_stop();
        }

        msg->inf("Test parameter is"+SP+DQ+sel+DQ);

        // Wait for elaboration to complete
        wait(5, SC_NS);

        if (sel == "test-debug")       /* TEST_SEL_ITEM */ run = this->test_debug(msg, sel);
        if (sel == "test-dadr-nack")   /* TEST_SEL_ITEM */ run = this->test_dadr_nack(msg, sel);
        if (sel == "test-arbitration") /* TEST_SEL_ITEM */ run = this->test_arbitration(msg, sel);
        if (sel == "test-rdwr-rand")   /* TEST_SEL_ITEM */ run = this->subtest_rdwr_rand(msg, sel, this->i_i2cc1);
        if (sel == "test-rdwr-sys")    /* TEST_SEL_ITEM */ run = this->test_rdwr_sys(msg, sel);
        if (sel == "test-i2cc-targ")   /* TEST_SEL_ITEM */ run = this->test_i2cc_targ(msg, sel);
        if (sel == "regression")       /* TEST_SEL_ITEM */ run = this->regression(msg, sel);

        if (run == false)
        {
            msg->err("Unknown test specified, requesting stop");
            this->tb_stop();
        }

        wait(5, SC_US);

        this->tb_stop();
    }

    void tb::tb_stop(void)
    {
        pMsg msg = pMsg(new Msg(this->msg->get_context()));

        msg->push_tag("stop");
        msg->inf("End of test");
        msg->report(get_plusarg("SEL"));
        this->i_cyc->performance();
        msg->inf("Requesting stop...");
        run_req_end = true;
        return;
    }


    //-- Tests -----------------------------------------------------------------
    bool tb::test_start(const pMsg &arg_msg)
    {
        wait(50, SC_NS);

        if (get_plusarg("COV") == "1")
        {
            VerilatedCov::zero();
            arg_msg->inf("Zeroed all coverage");
        }

        this->i_clk->sync();
        this->i_rst->deactivate();
        wait(50, SC_NS);
        return true;
    }

    bool tb::test_end(const pMsg &arg_msg)
    {
        string fnam = "";

        if (get_plusarg("COV") == "1")
        {
            fnam = get_plusarg("WRK")+FS+COVERAGE_FNAM;

            VerilatedCov::write(fnam.c_str());
            arg_msg->inf("Wrote coverage file"+SP+fnam);
        }

        return true;
    }

    bool tb::test_debug(const pMsg &arg_msg, const string &arg_sel)
    {
        uint32_t        dadr = get_plusarg_int("DADR_BYTE_T1") & 0xFF;
        deque<uint32_t> sadr = {0xA5};
        deque<uint32_t> wdat = {0x01, 0x01, 0x12, 0x08, 0x22, 0x20};
        uint32_t        wlen = wdat.size();
        uint32_t        clid = this->i_i2cc1->fifo_id_req();
        deque<uint32_t> rdat = {};
        bool            nack;
        stringstream    ss;

        // 1. start
        arg_msg->push_tag("test_debug");
        arg_msg->inf("Start");

        if (arg_sel == "test-debug")
        {
            this->test_start(arg_msg);
        }

        // 2. test
        this->i_ram1->clr_ram_data();
        this->i_clk->sync();
        this->i_i2cc1->i2c_wr(clid, dadr, sadr, wdat, nack);

        if (nack)
        {
            arg_msg->nok("Write NACKed");
        }
        else
        {
            arg_msg->aok("Write ACKed");
        }

        wait(20000, SC_NS);
        this->i_clk->sync();
        this->i_i2cc1->i2c_rd(clid, dadr, sadr, wlen, rdat);

        for (int i = 0 ; i < wlen ; i++)
        {
            ss = stringstream();
            ss << "[" << to_string(i);
            ss << "] exp 0x" << setfill('0') << setw(2) << hex << wdat[i];
            ss << SP << "obs 0x" << setfill('0') << setw(2) << hex << rdat[i];

            if (wdat[i] == rdat[i])
            {
                arg_msg->aok(ss.str());
            }
            else
            {
                arg_msg->nok(ss.str());
            }
        }

        // 3. end
        if (arg_sel == "test-debug")
        {
            this->test_end(arg_msg);
        }

        arg_msg->inf("End");
        arg_msg->pop_tag();
        return true;
    }

    bool tb::test_dadr_nack(const pMsg &arg_msg, const string &arg_sel)
    {
        deque<uint32_t> sadr = {0xA5};
        deque<uint32_t> wdat = {0x01, 0x01, 0x12, 0x08, 0x22, 0x20};
        uint32_t        clid = this->i_i2cc1->fifo_id_req();
        bool            nack;

        arg_msg->push_tag("test_dadr_nack");
        arg_msg->inf("Start with I2C client FIFO ID"+SP+to_string(clid));

        if (arg_sel == "test-dadr-nack")
        {
            this->test_start(arg_msg);
        }

        this->i_clk->sync();
        this->i_i2cc1->i2c_wr(clid, 0x4C, sadr, wdat, nack);

        if (nack)
        {
            arg_msg->aok("Write to bad address NACKed");
        }
        else
        {
            arg_msg->nok("Write to bad address ACKed");
        }

        if (arg_sel == "test-dadr-nack")
        {
            this->test_end(arg_msg);
        }

        arg_msg->inf("End");
        arg_msg->pop_tag();
        return true;
    }

    bool tb::subtest_rdwr_rand(const pMsg &arg_msg, const string &arg_sel, const pI2C_Ctrl &arg_ctlr)
    {
        const uint32_t  loop_count = 10;
        const uint32_t  data_mask  = 0xff;
        const string    ctlr_name  = arg_ctlr->name();
        const uint32_t  ctlr_clid  = arg_ctlr->fifo_id_req();
        const uint32_t  dadr_0     = get_plusarg_int("DADR_BYTE_T2");
        const uint32_t  dadr_1     = get_plusarg_int("DADR_BYTE_T1");

        seed_seq        ctlr_seed (ctlr_name.begin(), ctlr_name.end());

        uint32_t        rand_read;
        uint32_t        rand_dadr;
        uint32_t        rand_sadr;
        uint32_t        rand_bcnt;
        double          rand_wait;
        deque<uint32_t> sel_sadr;
        uint32_t        sel_dadr;
        uint32_t        ram_idx;
        deque<uint32_t> ram_dat;
        t_dre           engine;
        t_uri           dist_read(0, 1);
        t_uri           dist_dadr(0, 1);
        t_uri           dist_sadr(0, 255);
        t_uri           dist_bcnt(1, 20);
        t_uri           dist_wdat(0, 255);
        t_urd           dist_wait(5e-6, 20e-6);
        stringstream    ss;
        uint32_t        obs_dat;
        uint32_t        exp_dat;
        bool            nack;

        // 1. begin test
        arg_msg->push_tag("subtest_rdwr_rand");
        arg_msg->inf("Start");

        if (arg_sel == "test-rdwr-rand")
        {
            this->test_start(arg_msg);
        }

        // 2. test
        // 2.1. Seed the engine
        engine.seed(ctlr_seed);

        // 2.2. load rams
        arg_msg->inf("Load RAMs");
        for (uint32_t i = 0 ; i < 256 ; i++)
        {
            this->i_ram1->set_ram_data(i, i);
            this->i_ram2->set_ram_data(i, i);
        }

        // 2.3. transaction
        arg_msg->inf("Request transactions");
        for (int i = 0 ; i < loop_count ; i++)
        {
            rand_read = dist_read(engine);
            rand_dadr = dist_dadr(engine);
            rand_sadr = dist_sadr(engine);
            rand_bcnt = dist_bcnt(engine);
            rand_wait = dist_wait(engine);

            if (rand_dadr == 0) sel_dadr = dadr_0;
            if (rand_dadr == 1) sel_dadr = dadr_1;

            if (false)
            {
                ss = stringstream();
                ss << "Loop" << SP << to_string(i);
                ss << NL << arg_msg->get_shift_string(1) << "DADR 0x" << setfill('0') << setw(2) << hex << sel_dadr;
                ss << NL << arg_msg->get_shift_string(1) << "SADR 0x" << setfill('0') << setw(2) << hex << rand_sadr;
                ss << NL << arg_msg->get_shift_string(1) << "RCNT 0x" << setfill('0') << setw(2) << hex << rand_bcnt;
                arg_msg->dbg(ss.str());
            }

            if (rand_read)
            {
                ram_dat.clear();
                sel_sadr.clear();
                sel_sadr.push_back(rand_sadr);
                arg_ctlr->i2c_rd(ctlr_clid, sel_dadr, sel_sadr, rand_bcnt, ram_dat);

                for (uint32_t j = 0 ; j < rand_bcnt ; j++)
                {
                    obs_dat = ram_dat[j] & data_mask;
                    ram_idx = (rand_sadr + j) % 256;

                    if (rand_dadr == 0) exp_dat = this->i_ram2->get_ram_data(ram_idx);
                    if (rand_dadr == 1) exp_dat = this->i_ram1->get_ram_data(ram_idx);

                    exp_dat = exp_dat & data_mask;

                    ss = stringstream();
                    ss << "Loop [" << to_string(i);
                    ss << "] exp 0x" << setfill('0') << setw(2) << hex << exp_dat;
                    ss << SP << "obs 0x" << setfill('0') << setw(2) << hex << obs_dat;

                    if (obs_dat == exp_dat)
                    {
                        arg_msg->aok(ss.str());
                    }
                    else
                    {
                        arg_msg->nok(ss.str());
                    }
                }
            }
            else
            {
                ram_dat.clear();
                sel_sadr.clear();
                sel_sadr.push_back(rand_sadr);

                for (uint32_t j = 0 ; j < rand_bcnt ; j++)
                {
                    ram_dat.push_back(dist_wdat(engine));
                }

                arg_ctlr->i2c_wr(ctlr_clid, sel_dadr, sel_sadr, ram_dat, nack);

                for (uint32_t j = 0 ; j < rand_bcnt ; j++)
                {
                    exp_dat = ram_dat[j] & data_mask;
                    ram_idx = (rand_sadr + j) % 256;

                    if (rand_dadr == 0) obs_dat = this->i_ram2->get_ram_data(ram_idx);
                    if (rand_dadr == 1) obs_dat = this->i_ram1->get_ram_data(ram_idx);

                    obs_dat = obs_dat & data_mask;

                    ss = stringstream();
                    ss << "Loop [" << to_string(i);
                    ss << "] exp 0x" << setfill('0') << setw(2) << hex << exp_dat;
                    ss << SP << "obs 0x" << setfill('0') << setw(2) << hex << obs_dat;

                    if (obs_dat == exp_dat)
                    {
                        arg_msg->aok(ss.str());
                    }
                    else
                    {
                        arg_msg->nok(ss.str());
                    }
                }
            }

            wait(rand_wait, SC_SEC);
        }

        // 3. end test
        if (arg_sel == "test-rdwr-rand")
        {
            this->test_end(arg_msg);
        }

        arg_msg->inf("End");
        arg_msg->pop_tag();
        return true;
    }

    bool tb::test_rdwr_sys(const pMsg &arg_msg, const string &arg_sel)
    {
        string text = arg_msg->get_context()+PT+"test-rdwr-sys";
        pMsg   msg1 = pMsg(new Msg(text+PT+"icc1"));
        pMsg   msg2 = pMsg(new Msg(text+PT+"icc2"));

        // 1. begin test
        if (arg_sel == "test-rdwr-sys")
        {
            this->test_start(arg_msg);
        }

        // 2. test
        SC_FORK
            sc_spawn(sc_bind(&tb::subtest_rdwr_rand, this, sc_cref(msg1), arg_sel, sc_cref(this->i_i2cc1) )),
            sc_spawn(sc_bind(&tb::subtest_rdwr_rand, this, sc_cref(msg2), arg_sel, sc_cref(this->i_i2cc2) )),
        SC_JOIN


        // 3. end test
        if (arg_sel == "test-rdwr-sys")
        {
            this->test_end(arg_msg);
        }

        return true;
    }

    void tb::subtest_arbitration(const pMsg &arg_msg, const string &arg_sel, const pI2C_Ctrl &arg_ctlr)
    {
        const uint32_t  data_mask  = 0xff;
        const string    ctlr_name  = arg_ctlr->name();
        const uint32_t  ctlr_clid  = arg_ctlr->fifo_id_req();

        seed_seq        ctlr_seed (ctlr_name.begin(), ctlr_name.end());

        deque<uint32_t> ram_sadr;
        uint32_t        ram_dadr = get_plusarg_int("DADR_BYTE_T1");
        uint32_t        ram_rcnt = 4;
        uint32_t        ram_idx;
        deque<uint32_t> ram_dat;
        t_dre           engine;
        t_uri           dist_sadr(0, 255);
        uint32_t        rand_sadr;
        stringstream    ss;
        uint32_t        obs_dat;
        uint32_t        exp_dat;

        // 1. begin test
        arg_msg->push_tag("subtest_arbitration");
        arg_msg->inf("Start");

        // 2. test
        // 2.1. Seed the engine
        engine.seed(ctlr_seed);

        // 2.2. load rams
        arg_msg->inf("Load RAMs");
        for (uint32_t i = 0 ; i < 256 ; i++)
        {
            this->i_ram1->set_ram_data(i, i);
        }

        // 2.3. transaction
        arg_msg->inf("Request transaction");

        rand_sadr = dist_sadr(engine);

        if (true)
        {
            ss = stringstream();
            ss << "Test parameters";
            ss << NL << arg_msg->get_shift_string(1) << "DADR 0x" << setfill('0') << setw(2) << hex << ram_dadr;
            ss << NL << arg_msg->get_shift_string(1) << "SADR 0x" << setfill('0') << setw(2) << hex << rand_sadr;
            ss << NL << arg_msg->get_shift_string(1) << "RCNT 0x" << setfill('0') << setw(2) << hex << ram_rcnt;
            arg_msg->dbg(ss.str());
        }

        ram_dat.clear();
        ram_sadr.clear();
        ram_sadr.push_back(rand_sadr & 0xFF);
        arg_ctlr->i2c_rd(ctlr_clid, ram_dadr, ram_sadr, ram_rcnt, ram_dat);

        for (uint32_t j = 0 ; j < ram_rcnt ; j++)
        {
            obs_dat = ram_dat[j] & data_mask;
            ram_idx = (rand_sadr + j) % 256;
            exp_dat = this->i_ram1->get_ram_data(ram_idx);
            exp_dat = exp_dat & data_mask;

            ss = stringstream();
            ss << "Compare exp 0x" << setfill('0') << setw(2) << hex << exp_dat;
            ss << SP << "obs 0x" << setfill('0') << setw(2) << hex << obs_dat;

            if (obs_dat == exp_dat)
            {
                arg_msg->aok(ss.str());
            }
            else
            {
                arg_msg->nok(ss.str());
            }
        }

        // 3. end test
        arg_msg->inf("End");
        arg_msg->pop_tag();
    }

    bool tb::test_arbitration(const pMsg &arg_msg, const string &arg_sel)
    {
        string text = arg_msg->get_context()+PT+"test-arbitration";
        pMsg   msg1 = pMsg(new Msg(text+PT+"icc1"));
        pMsg   msg2 = pMsg(new Msg(text+PT+"icc2"));

        // 1. begin test
        if (arg_sel == "test-arbitration")
        {
            this->test_start(arg_msg);
        }

        // 2. test
        SC_FORK
            sc_spawn(sc_bind(&tb::subtest_arbitration, this, sc_cref(msg1), arg_sel, sc_cref(this->i_i2cc1) )),
            sc_spawn(sc_bind(&tb::subtest_arbitration, this, sc_cref(msg2), arg_sel, sc_cref(this->i_i2cc2) )),
        SC_JOIN


        // 3. end test
        if (arg_sel == "test-arbitration")
        {
            this->test_end(arg_msg);
        }

        return true;
    }

    bool tb::test_i2cc_targ(const pMsg &arg_msg, const string &arg_sel)
    {
        uint32_t        dadr = get_plusarg_int("DADR_BYTE_C2");
        deque<uint32_t> sadr = {};
        deque<uint32_t> wdat = {0x5A};
        uint32_t        clid = this->i_i2cc1->fifo_id_req();
        uint32_t        rcnt = 1;
        deque<uint32_t> rdat;
        uint32_t        rexp = 0x3C;
        bool            nack;
        stringstream    ss;

        arg_msg->push_tag("test_i2cc_targ");
        arg_msg->inf("Start with I2C controller 1 client FIFO ID"+SP+to_string(clid));

        if (arg_sel == "test-i2cc-targ")
        {
            this->test_start(arg_msg);
        }

        // 1. Write byte to target DADR_BYTE_C2
        this->i_clk->sync();
        this->i_i2cc1->i2c_wr(clid, dadr, sadr, wdat, nack);

        if (nack)
        {
            arg_msg->nok("Write to bad address NACKed");
        }
        else
        {
            arg_msg->aok("Write to address ACKed");
        }

        // 2. Read byte from target DADR_BYTE_C2
        this->i_clk->sync();
        this->i_i2cc1->i2c_rd(clid, dadr, sadr, rcnt, rdat);

        if (rdat.size() == 0)
        {
            arg_msg->nok("Read returns no data");
        }
        else
        {
            ss = stringstream();
            ss << "Read obs:0x" << setfill('0') << setw(2) << hex << rdat[0];
            ss << CM+SP << "exp:0x" << setfill('0') << setw(2) << hex << rexp;

            if (rdat[0] == rexp)
            {
                arg_msg->aok(ss.str());
            }
            else
            {
                arg_msg->nok(ss.str());
            }
        }

        if (arg_sel == "test-i2cc-targ")
        {
            this->test_end(arg_msg);
        }

        arg_msg->inf("End");
        arg_msg->pop_tag();
        return true;
    }

    bool tb::regression(const pMsg &arg_msg, const string &arg_sel)
    {
        arg_msg->push_tag(arg_sel);
        arg_msg->inf("Start");
        this->test_start(arg_msg);
        this->i_clk->sync();

        this->test_dadr_nack(arg_msg, arg_sel);
        this->test_arbitration(arg_msg, arg_sel);
        this->test_rdwr_sys(arg_msg, arg_sel);
        this->test_i2cc_targ(arg_msg, arg_sel);
        this->test_rdwr_sys(arg_msg, arg_sel);


        this->test_end(arg_msg);
        arg_msg->inf("End");
        arg_msg->pop_tag();
        return true;
    }
}


// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TB_I2C_CTRL_H_
    #define _TB_I2C_CTRL_H_

    #define SC_INCLUDE_DYNAMIC_PROCESSES

    #include <memory>
    #include <deque>
    #include <string>
    #include <iomanip>
    #include <utility>
    #include <systemc>
    #include <tb_msg.h>
    #include <tb_i2c_targ_phy.h>
    #include <tb_i2c_ctrl_spec.h>

    namespace tb
    {
        namespace Parameters
        {
            const uint32_t I2C_STAT_7_RXACK  = (uint32_t)0x80;
            const uint32_t I2C_STAT_6_BUSY   = (uint32_t)0x40;
            const uint32_t I2C_STAT_5_CLARB  = (uint32_t)0x20;
            const uint32_t I2C_STAT_4_TMODE  = (uint32_t)0x10;
            const uint32_t I2C_STAT_3_TDAV   = (uint32_t)0x08;
            const uint32_t I2C_STAT_2_TDRQ   = (uint32_t)0x04;
            const uint32_t I2C_STAT_1_CTIP   = (uint32_t)0x02;
            const uint32_t I2C_STAT_0_CIRQ   = (uint32_t)0x01;
            const uint32_t I2C_STAT_NONE     = (uint32_t)0x00;

            const uint32_t I2C_CMDR_7_STA    = (uint32_t)0x80;
            const uint32_t I2C_CMDR_6_STO    = (uint32_t)0x40;
            const uint32_t I2C_CMDR_5_RD     = (uint32_t)0x20;
            const uint32_t I2C_CMDR_4_WR     = (uint32_t)0x10;
            const uint32_t I2C_CMDR_3_ACK    = (uint32_t)0x08;
            const uint32_t I2C_CMDR_2_RSVD   = (uint32_t)0x04;
            const uint32_t I2C_CMDR_1_RSVD   = (uint32_t)0x02;
            const uint32_t I2C_CMDR_0_IACK   = (uint32_t)0x01;
            const uint32_t I2C_CMDR_NONE     = (uint32_t)0x00;
        }

        class I2C_Ctrl : public sc_module
        {
            private:
                const std::string GTKW_CMDR_32_FNAM     = "i2c_ctrl_cmdr_32.filter";
                const std::string GTKW_CMDR_08_FNAM     = "i2c_ctrl_cmdr_08.filter";
                const uint32_t    DRV_ID                = 0;
                const uint32_t    TRG_ID                = 1;
                const uint32_t    CMDR_DADR_WR          = 0x90; // STA+WR
                const uint32_t    CMDR_DATA_WR          = 0x10; // WR
                const uint32_t    CMDR_DATA_RD          = 0x20; // RD
                const uint32_t    CMDR_LAST_WR          = 0x50; // WR+STO
                const uint32_t    CMDR_LAST_RD          = 0x68; // RD+STO+NAK
                const uint32_t    CMDR_STOP             = 0x40; // STO
                const uint32_t    CMDR_TACK             = 0x02; // TACK
                const uint32_t    CMDR_IACK             = 0x01; // IACK

                pMsg              msg;
                uint32_t          dadr_byte;
                uint32_t          fifo_count;
                uint32_t          fifo_id_nxt;
                uint32_t          fifo_id_drv;
                uint32_t          fifo_id_trg;
                pI2CC_FIFOs       fifos;

            public:
                I2C_Ctrl_Spec        spec;
                sc_signal <bool>     stat_rxack;
                sc_signal <bool>     stat_busy;
                sc_signal <bool>     stat_clarb;
                sc_signal <bool>     stat_tmode;
                sc_signal <bool>     stat_tdav;
                sc_signal <bool>     stat_tdrq;
                sc_signal <bool>     stat_ctip;
                sc_signal <bool>     stat_cirq;
                sc_out    <uint32_t> cmdr_data_o;
                sc_out    <bool>     cmdr_strb_o;
                sc_out    <uint32_t> txdr_data_o;
                sc_out    <bool>     txdr_strb_o;
                sc_in     <uint32_t> data_i;
                sc_in     <uint32_t> stat_i;
                sc_in     <bool>     clk_i;

                SC_HAS_PROCESS(I2C_Ctrl);

                //-- Constructor/Destructor ------------------------------------
                I2C_Ctrl(sc_module_name arg_nm, uint32_t arg_dadr_byte, uint32_t arg_fifo_count);
                ~I2C_Ctrl(void);

                //-- Virtual ---------------------------------------------------
                virtual void end_of_elaboration(void);

                //-- Top-level Thread ------------------------------------------
                void stat(void);
                void target(void);
                void driver(void);

                //-- I2C Methods -----------------------------------------------
                void byte_rd(const uint32_t &arg_clid, const uint32_t &arg_cmdr, uint32_t &ref_rxdr, uint32_t &ref_stat);
                void byte_wr(const uint32_t &arg_clid, const uint32_t &arg_cmdr, const uint32_t &arg_txdr, uint32_t &ref_stat);
                void byte_wr_stop(const uint32_t &arg_clid);
                void byte_wr_iack(const uint32_t &arg_clid);
                void byte_wr_tack(const uint32_t &arg_clid, const uint32_t &arg_txdr);
                void i2c_wr
                (
                    const uint32_t             &arg_clid,
                    const uint32_t             &arg_dadr_byte,
                    const std::deque<uint32_t> &arg_sadr,
                    const std::deque<uint32_t> &arg_wdat,
                    bool                       &ref_nack
                );
                void i2c_rd
                (
                    const uint32_t             &arg_clid,
                    const uint32_t             &arg_dadr_byte,
                    const std::deque<uint32_t> &arg_sadr,
                    const uint32_t             &arg_cnt,
                    std::deque<uint32_t>       &ref_rdat
                );
                bool get_stat_rxack(const uint32_t &arg_stat);
                bool get_stat_busy(const uint32_t &arg_stat);
                bool get_stat_clarb(const uint32_t &arg_stat);
                bool get_stat_tmode(const uint32_t &arg_stat);
                bool get_stat_tdav(const uint32_t &arg_stat);
                bool get_stat_tdrq(const uint32_t &arg_stat);
                bool get_stat_ctip(const uint32_t &arg_stat);
                bool get_stat_cirq(const uint32_t &arg_stat);


                //-- Utility ---------------------------------------------------
                std::string decode_cmdr(const uint32_t &arg_cmdr);
                void        create_gtkw_cmdr_filter(void);
                uint32_t    fifo_id_req(void);
        };

        typedef std::unique_ptr<I2C_Ctrl> pI2C_Ctrl;
    }
#endif

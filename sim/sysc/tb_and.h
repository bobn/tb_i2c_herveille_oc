
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TB_AND_H_
    #define _TB_AND_H_

    #include <systemc>
    #include <tb_base.h>

    namespace tb
    {
        template <int T>
        class And : public sc_core::sc_module
        {
            public:
                SC_HAS_PROCESS(And);

                And(sc_core::sc_module_name);
                ~And(void);

                sc_core::sc_out <bool> and_o;
                sc_core::sc_in  <bool> vec_i[T];

                void run(void);
        };

        template <int T>
        And<T>::And(sc_module_name arg_nm) : sc_module (arg_nm)
        {
            SC_THREAD(run);
            //begin
                for (int idx = 0 ; idx < T ; idx++)
                {
                    sensitive << vec_i[idx];
                }
            //end
        }

        template <int T>
        And<T>::~And(void) {}

        template <int T>
        void And<T>::run(void)
        {
            bool y = true;

            while (true)
            {
                wait();

                y = true;

                for (int idx = 0 ; idx < T ; idx++)
                {
                    if (vec_i[idx] == false)
                    {
                        y = false;
                    }
                }

                and_o.write(y);
            }
        }

        typedef std::unique_ptr<And<2>> pAnd2;
    }
#endif

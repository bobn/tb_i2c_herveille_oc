
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_i2c_ctrl.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;
    using namespace Parameters;

    //-- Constructor/Destructor ------------------------------------------------
    I2C_Ctrl::I2C_Ctrl(sc_module_name arg_nm, uint32_t arg_dadr_byte, uint32_t arg_fifo_count) : sc_module (arg_nm)
    {
        this->msg         = pMsg(new Msg(this->name()));
        this->dadr_byte   = arg_dadr_byte & I2CT_DADR_MASK;
        this->fifo_count  = arg_fifo_count;
        this->fifo_id_nxt = 0;
        this->fifo_id_drv = this->fifo_id_req();
        this->fifo_id_trg = this->fifo_id_req();
        this->spec.clid   = 0;
        this->spec.cmdr   = 0;
        this->spec.txdr   = 0;
        this->spec.rxdr   = 0;
        this->spec.stat   = 0;

        for (int i = 0 ; i < this->fifo_count ; i++)
        {
            this->fifos.push_back(pI2CC_FIFO(new I2CC_FIFO(1)));
        }

        this->msg->inf("Created"+SP+to_string(this->fifo_count)+SP+"spec fifos");

        SC_THREAD(driver);
        SC_THREAD(target);
        SC_THREAD(stat);
    }

    I2C_Ctrl::~I2C_Ctrl(void) {}


    //-- Virtual ---------------------------------------------------------------
    void I2C_Ctrl::end_of_elaboration(void)
    {
        this->msg->inf("tb::I2C_Ctrl::end_of_elaboration()...");
        this->create_gtkw_cmdr_filter();
        sc_trace(trace_sysc, this->cmdr_data_o,      this->name()+PT+"cmdr_data_o"      );
        sc_trace(trace_sysc, this->cmdr_strb_o,      this->name()+PT+"cmdr_strb_o"      );
        sc_trace(trace_sysc, this->txdr_data_o,      this->name()+PT+"txdr_data_o"      );
        sc_trace(trace_sysc, this->txdr_strb_o,      this->name()+PT+"txdr_strb_o"      );
        sc_trace(trace_sysc, this->data_i,           this->name()+PT+"data_i"           );
        sc_trace(trace_sysc, this->stat_i,           this->name()+PT+"stat_i"           );
        sc_trace(trace_sysc, this->spec,             this->name()+PT+"spec"             );
        sc_trace(trace_sysc, this->stat_rxack,       this->name()+PT+"stat_rxack"       );
        sc_trace(trace_sysc, this->stat_busy,        this->name()+PT+"stat_busy"        );
        sc_trace(trace_sysc, this->stat_clarb,       this->name()+PT+"stat_clarb"       );
        sc_trace(trace_sysc, this->stat_tmode,       this->name()+PT+"stat_tmode"       );
        sc_trace(trace_sysc, this->stat_tdav,        this->name()+PT+"stat_tdav"        );
        sc_trace(trace_sysc, this->stat_tdrq,        this->name()+PT+"stat_tdrq"        );
        sc_trace(trace_sysc, this->stat_ctip,        this->name()+PT+"stat_ctip"        );
        sc_trace(trace_sysc, this->stat_cirq,        this->name()+PT+"stat_cirq"        );

        this->msg->inf("tb::I2C_Ctrl::end_of_elaboration() done.");
    }


    //-- Top-level Thread ------------------------------------------------------
    void I2C_Ctrl::stat(void)
    {
        pMsg msg = pMsg(new Msg(string(this->name())+".stat"));

        this->stat_rxack.write(false);
        this->stat_busy.write(false);
        this->stat_clarb.write(false);
        this->stat_tmode.write(false);
        this->stat_tdav.write(false);
        this->stat_tdrq.write(false);
        this->stat_ctip.write(false);
        this->stat_cirq.write(false);

        msg->inf("Start");

        while (true)
        {
            wait(this->stat_i.value_changed_event());

            this->stat_rxack.write ( this->get_stat_rxack(this->stat_i.read()) );
            this->stat_busy.write  ( this->get_stat_busy(this->stat_i.read())  );
            this->stat_clarb.write ( this->get_stat_clarb(this->stat_i.read()) );
            this->stat_tmode.write ( this->get_stat_tmode(this->stat_i.read()) );
            this->stat_tdav.write  ( this->get_stat_tdav(this->stat_i.read())  );
            this->stat_tdrq.write  ( this->get_stat_tdrq(this->stat_i.read())  );
            this->stat_ctip.write  ( this->get_stat_ctip(this->stat_i.read())  );
            this->stat_cirq.write  ( this->get_stat_cirq(this->stat_i.read())  );
        }
    }

    void I2C_Ctrl::target(void)
    {
        pMsg            msg  = pMsg(new Msg(string(this->name())+".target"));
        uint32_t        txdr = 0x3C;
        uint32_t        stat;
        uint32_t        wdat;
        uint32_t        ret;
        I2C_Ctrl_Spec   spec;
        stringstream    ss;

        if (this->fifo_id_trg != TRG_ID)
        {
            msg->err("target(): FIFO ID !="+SP+to_string(TRG_ID)+PT+SP+"Stop.");
            run_req_end = true;
            wait();
        }

        ss = stringstream();
        ss << "Starting; DADR is 0x" << setfill('0') << setw(2) << hex << this->dadr_byte;
        ss << CM+SP << "FIFO ID is 0x" << setfill('0') << setw(2) << hex << TRG_ID;
        msg->inf(ss.str());

        while (true)
        {
            wait(this->stat_i.value_changed_event());

            stat = this->stat_i.read();
            wdat = this->data_i.read();

            if (this->get_stat_tdav(stat))
            {
                ss = stringstream();
                ss << "Target data available, 0x" << setfill('0') << setw(2) << hex << wdat;
                ss << CM+SP << "Requesting IACK and TACK";
                this->msg->inf(ss.str());

                this->byte_wr_tack(TRG_ID, 0);
                this->byte_wr_iack(TRG_ID);
            }
            else if (this->get_stat_tdrq(stat))
            {
                ss = stringstream();
                ss << "Target data requested, sending 0x" << setfill('0') << setw(2) << hex << txdr;
                this->msg->inf(ss.str());

                this->byte_wr_tack(TRG_ID, txdr);
                this->byte_wr_iack(TRG_ID);
            }
        }
    }

    void I2C_Ctrl::driver(void)
    {
        pMsg          msg  = pMsg(new Msg(string(this->name())+".driver"));
        sc_time       lim  = sc_time(200, SC_US);
        sc_time       bgn;
        uint32_t      clid;
        stringstream  ss;

        if (this->fifo_id_drv != DRV_ID)
        {
            msg->err("driver(): FIFO ID !="+SP+to_string(DRV_ID)+PT+SP+"Stop.");
            run_req_end = true;
            wait();
        }

        this->cmdr_data_o.write((uint32_t)0);
        this->cmdr_strb_o.write(false);
        this->txdr_data_o.write((uint32_t)0);
        this->txdr_strb_o.write(false);

        ss = stringstream();
        ss << "Starting; FIFO ID is 0x" << setfill('0') << setw(2) << hex << DRV_ID;
        msg->inf(ss.str());

        while (true)
        {
            // 1. wait for request
            this->fifos[DRV_ID]->read(this->spec);

            if (this->spec.clid >= this->fifo_count)
            {
                ss = stringstream();
                ss << "driver(): received client FIFO ID (" << this->spec.clid << ")";
                ss << SP << ">=" << SP << this->fifo_count;
                ss << PT+SP << "Stop.";
                msg->err(ss.str());
                run_req_end = true;
                wait();
            }

            if (false)
            {
                ss = stringstream();
                ss << "received" << SP << this->spec;
                msg->dbg(ss.str());
            }

            // 2. update spec.clid
            clid            = this->spec.clid;
            this->spec.clid = DRV_ID;

            // 3. drive cmdr & txdr
            wait(this->clk_i.negedge_event());
            this->cmdr_data_o.write(this->spec.cmdr & 0xFF);
            this->cmdr_strb_o.write(true);
            this->txdr_data_o.write(this->spec.txdr & 0xFF);
            this->txdr_strb_o.write(true);

            // 4. clear cmdr & txdr
            wait(this->clk_i.negedge_event());
            this->cmdr_data_o.write((uint32_t)0);
            this->cmdr_strb_o.write(false);
            this->txdr_data_o.write((uint32_t)0);
            this->txdr_strb_o.write(false);

            // 5. manage command responses
            if ((this->spec.cmdr == CMDR_IACK) || (this->spec.cmdr == CMDR_TACK))
            {
                // 5.1. manage IRQ acknowledge command
                this->fifos[clid]->write(this->spec);
                continue;
            }
            else if (this->spec.cmdr == CMDR_STOP)
            {
                // 5.2. manage stop command
                this->spec.stat = this->stat_i.read();
                this->spec.rxdr = this->data_i.read();

                if (not (this->get_stat_busy(this->spec.stat)))
                {
                    msg->dbg("STO command requested but the bus is not busy.  Is this a valid condition?");
                    msg->err("Code needs fix per the debug message.  Requesting stop...");
                    run_req_end = true;
                    wait();
                }

                // 5.2.1. wait for BUSY low
                bgn = sc_time_stamp();
                while (true)
                {
                    wait(this->clk_i.negedge_event());

                    if ((sc_time_stamp() - bgn) > lim)
                    {
                        msg->err("Timeout waiting for BUSY low.  Requesting stop...");
                        run_req_end = true;
                        wait();
                    }

                    this->spec.stat = this->stat_i.read();
                    this->spec.rxdr = this->data_i.read();

                    if (not (this->get_stat_busy(this->spec.stat)))
                    {
                        break;
                    }
                }

                // 5.2.2. send response
                this->fifos[clid]->write(this->spec);
                continue;
            }
            else
            {
                // 5.3. manage other commands
                // 5.3.1. wait for TIP high
                bgn = sc_time_stamp();
                while (true)
                {
                    wait(this->clk_i.negedge_event());

                    if ((sc_time_stamp() - bgn) > lim)
                    {
                        msg->err("Timeout waiting for TIP high.  Requesting stop...");
                        run_req_end = true;
                        wait();
                    }

                    this->spec.stat = this->stat_i.read();
                    this->spec.rxdr = this->data_i.read();

                    if (this->get_stat_ctip(this->spec.stat))
                    {
                        break;
                    }
                }

                // 5.3.2. wait for TIP low
                bgn = sc_time_stamp();
                while (true)
                {
                    wait(this->clk_i.negedge_event());

                    if ((sc_time_stamp() - bgn) > lim)
                    {
                        msg->err("Timeout waiting for TIP low.  Requesting stop...");
                        run_req_end = true;
                        wait();
                    }

                    this->spec.stat = this->stat_i.read();
                    this->spec.rxdr = this->data_i.read();

                    if (not (this->get_stat_ctip(this->spec.stat)))
                    {
                        break;
                    }
                }

                if (false)
                {
                    ss = stringstream();
                    ss << "clid" << SP << clid;
                    ss << SP << "<--" << SP << this->spec;
                    msg->dbg(ss.str());
                }


                // 5.3.3. send response
                this->fifos[clid]->write(this->spec);
                continue;
            }
        }
    }


    //-- I2C Methods -----------------------------------------------------------
    void I2C_Ctrl::byte_rd(const uint32_t &arg_clid, const uint32_t &arg_cmdr, uint32_t &ref_rxdr, uint32_t &ref_stat)
    {
        I2C_Ctrl_Spec spec;
        stringstream  ss;

        spec.clid = arg_clid;
        spec.cmdr = arg_cmdr;
        spec.txdr = 0;
        spec.rxdr = 0;
        spec.stat = 0;

        ss = stringstream();
        ss << "byte_rd() <-[" << DRV_ID;
        ss << "]--" << SP << this->decode_cmdr(arg_cmdr);
        this->msg->inf(ss.str());

        this->fifos[DRV_ID]->write(spec);
        this->fifos[arg_clid]->read(spec);

        if (spec.clid != DRV_ID)
        {
            ss = stringstream();
            ss << "byte_rd(): received from client FIFO ID" << SP << spec.clid;
            ss << CM+SP << "but expected ID" << SP << DRV_ID;
            ss << PT+SP << "Stop.";
            msg->err(ss.str());
            run_req_end = true;
            wait();
        }

        ref_rxdr = spec.rxdr;
        ref_stat = spec.stat;

        ss = stringstream();
        ss << "byte_rd() --[" << arg_clid;
        ss << "]-> 0x" << setfill('0') << setw(2) << hex << spec.rxdr;

        if (this->get_stat_rxack(spec.stat))
        {
            ss << SP << "NACK";
        }
        else
        {
            ss << SP << "ACK";
        }

        if (this->get_stat_clarb(spec.stat))
        {
            ss << "+ARBL";
        }

        this->msg->inf(ss.str());
    }

    void I2C_Ctrl::byte_wr(const uint32_t &arg_clid, const uint32_t &arg_cmdr, const uint32_t &arg_txdr, uint32_t &ref_stat)
    {
        I2C_Ctrl_Spec spec;
        stringstream  ss;

        spec.clid = arg_clid;
        spec.cmdr = arg_cmdr;
        spec.txdr = arg_txdr;
        spec.rxdr = 0;
        spec.stat = 0;

        ss = stringstream();
        ss << "byte_wr() <-[" << DRV_ID;
        ss << "]-- 0x" << setfill('0') << setw(2) << hex << arg_txdr;
        ss << SP << this->decode_cmdr(arg_cmdr);
        this->msg->inf(ss.str());

        this->fifos[DRV_ID]->write(spec);
        this->fifos[arg_clid]->read(spec);

        if (spec.clid != DRV_ID)
        {
            ss = stringstream();
            ss << "byte_wr(): received from client FIFO ID" << SP << spec.clid;
            ss << CM+SP << "but expected ID" << SP << DRV_ID;
            ss << PT+SP << "Stop.";
            msg->err(ss.str());
            run_req_end = true;
            wait();
        }

        ref_stat = spec.stat;

        ss = stringstream();
        ss << "byte_wr() --[" << to_string(arg_clid) << "]->";

        if (this->get_stat_rxack(spec.stat))
        {
            ss << SP << "NACK";
        }
        else
        {
            ss << SP << "ACK";
        }

        if (this->get_stat_clarb(spec.stat))
        {
            ss << "+ARBL";
        }

        this->msg->inf(ss.str());
    }

    void I2C_Ctrl::byte_wr_stop(const uint32_t &arg_clid)
    {
        uint32_t stat;

        this->msg->inf("byte_wr_stop()");
        this->byte_wr(arg_clid, CMDR_STOP, 0, stat);
    }

    void I2C_Ctrl::byte_wr_iack(const uint32_t &arg_clid)
    {
        I2C_Ctrl_Spec spec;

        uint32_t stat = this->stat_i.read();

        if (this->get_stat_cirq(stat))
        {
            spec.clid = arg_clid;
            spec.cmdr = CMDR_IACK;
            spec.txdr = 0;
            spec.rxdr = 0;
            spec.stat = 0;

            this->fifos[DRV_ID]->write(spec);
            this->fifos[arg_clid]->read(spec);

            stat = spec.stat;
        }
    }

    void I2C_Ctrl::byte_wr_tack(const uint32_t &arg_clid, const uint32_t &arg_txdr)
    {
        I2C_Ctrl_Spec spec;

        spec.clid = arg_clid;
        spec.cmdr = CMDR_TACK;
        spec.txdr = arg_txdr;
        spec.rxdr = 0;
        spec.stat = 0;

        this->fifos[DRV_ID]->write(spec);
        this->fifos[arg_clid]->read(spec);
    }

    void I2C_Ctrl::i2c_wr
    (
        const uint32_t        &arg_clid,
        const uint32_t        &arg_dadr_byte,
        const deque<uint32_t> &arg_sadr,
        const deque<uint32_t> &arg_wdat,
        bool                  &ref_nack
    )
    {
        uint32_t     targ_dadr = arg_dadr_byte & I2CT_DADR_MASK;
        uint32_t     bytes     = arg_wdat.size();
        uint32_t     last;
        uint32_t     txdr;
        uint32_t     cmdr;
        uint32_t     stat;
        stringstream ss;

        ref_nack = false;

        if (bytes < (uint32_t)1)
        {
            this->msg->err("i2c_wr(): Empty write data buffer.  Requesting stop...");
            run_req_end = true;
            wait();
        }
        else
        {
            last = bytes - (uint32_t)1;
        }

        ss = stringstream();
        ss << "i2c_wr(): Writing" << SP << bytes << SP+"byte(s) to dadr (byte)";
        ss << SP+"0x" << setfill('0') << setw(2) << hex << targ_dadr;

        if (arg_sadr.size())
        {
            ss << CM+SP+"sadr";
            for (uint32_t sadr : arg_sadr) { ss << SP+"0x" << setfill('0') << setw(2) << hex << sadr; }
        }

        this->msg->inf(ss.str());

        // retry upon arb lost
        while (true)
        {
            // 1. Device addr write transmit request
            this->byte_wr_iack(arg_clid);

            while (this->get_stat_busy(this->stat_i.read()))
            {
                wait(this->stat_i.value_changed_event());
            }

            this->byte_wr(arg_clid, CMDR_DADR_WR, targ_dadr, stat);

            if (this->get_stat_clarb(stat))
            {
                continue;
            }

            if (this->get_stat_rxack(stat))
            {
                this->byte_wr_iack(arg_clid);
                this->byte_wr_stop(arg_clid);
                ref_nack = true;
                return;
            }

            // 2. Sub-address transmit request
            if (arg_sadr.size())
            {
                for (uint32_t sadr : arg_sadr)
                {
                    this->byte_wr_iack(arg_clid);
                    this->byte_wr(arg_clid, CMDR_DATA_WR, (sadr & 0xFF), stat);

                    if (this->get_stat_clarb(stat))
                    {
                        break;
                    }

                    if (this->get_stat_rxack(stat))
                    {
                        this->byte_wr_iack(arg_clid);
                        this->byte_wr_stop(arg_clid);
                        ref_nack = true;
                        return;
                    }
                }

                if (this->get_stat_clarb(stat))
                {
                    continue;
                }
            }

            // 3. Data byte transmit requests
            for (int i = 0 ; i < bytes ; i++)
            {
                txdr = arg_wdat[i] & 0xFF;
                cmdr = (i == last) ? CMDR_LAST_WR : CMDR_DATA_WR;

                this->byte_wr_iack(arg_clid);
                this->byte_wr(arg_clid, cmdr, txdr, stat);

                if (this->get_stat_clarb(stat))
                {
                    break;
                }

                if (this->get_stat_rxack(stat))
                {
                    this->byte_wr_iack(arg_clid);
                    this->byte_wr_stop(arg_clid);
                    ref_nack = true;
                    return;
                }
            }

            if (this->get_stat_clarb(stat))
            {
                continue;
            }
            else
            {
                return;
            }
        }
    }

    void I2C_Ctrl::i2c_rd
    (
        const uint32_t        &arg_clid,
        const uint32_t        &arg_dadr_byte,
        const deque<uint32_t> &arg_sadr,
        const uint32_t        &arg_cnt,
        deque<uint32_t>       &ref_rdat
    )
    {
        uint32_t     targ_dadr = arg_dadr_byte & I2CT_DADR_MASK;
        uint32_t     bytes     = arg_cnt;
        uint32_t     last;
        uint32_t     rxdr;
        uint32_t     stat;
        stringstream ss;

        if (bytes < (uint32_t)1)
        {
            this->msg->err("i2c_rd(): No bytes requested.  Quit.");
            return;
        }
        else
        {
            last = bytes - 1;
        }

        ss = stringstream();
        ss << "i2c_rd(): Reading" << SP << bytes;
        ss << SP+"byte(s) from dadr (byte)" << SP+"0x" << setfill('0') << setw(2) << hex << targ_dadr;

        if (arg_sadr.size())
        {
            ss << CM+SP+"sadr";
            for (uint32_t sadr : arg_sadr) { ss << SP+"0x" << setfill('0') << setw(2) << hex << sadr; }
        }

        this->msg->inf(ss.str());

        // retry upon arb lost
        while (true)
        {
            // 1. Device addr write transmit request
            this->byte_wr_iack(arg_clid);

            while (this->get_stat_busy(this->stat_i.read()))
            {
                wait(this->stat_i.value_changed_event());
            }

            if (arg_sadr.size())
            {
                this->byte_wr(arg_clid, CMDR_DADR_WR, targ_dadr, stat);
            }
            else
            {
                this->byte_wr(arg_clid, CMDR_DADR_WR, (targ_dadr | I2CT_RDWR_BIT), stat);
            }

            if (this->get_stat_clarb(stat))
            {
                continue;
            }

            if (this->get_stat_rxack(stat))
            {
                this->byte_wr_iack(arg_clid);
                this->byte_wr_stop(arg_clid);
                return;
            }

            // 2. Sub-address and restart
            if (arg_sadr.size())
            {
                // 2.1. Sub-address transmit request
                for (uint32_t sadr : arg_sadr)
                {
                    this->byte_wr_iack(arg_clid);
                    this->byte_wr(arg_clid, CMDR_DATA_WR, (sadr & 0xFF), stat);

                    if (this->get_stat_clarb(stat))
                    {
                        break;
                    }

                    if (this->get_stat_rxack(stat))
                    {
                        this->byte_wr_iack(arg_clid);
                        this->byte_wr_stop(arg_clid);
                        return;
                    }
                }

                if (this->get_stat_clarb(stat))
                {
                    continue;
                }

                // 2.2. Device addr read restart request
                this->byte_wr_iack(arg_clid);
                this->byte_wr(arg_clid, CMDR_DADR_WR, (targ_dadr | I2CT_RDWR_BIT), stat);

                if (this->get_stat_clarb(stat))
                {
                    continue;
                }

                if (this->get_stat_rxack(stat))
                {
                    this->byte_wr_iack(arg_clid);
                    this->byte_wr_stop(arg_clid);
                    return;
                }
            }

            // 4. Data byte read receive requests
            ref_rdat.clear();

            for (int i = 0 ; i < bytes ; i++)
            {
                if (i == last)
                {
                    this->byte_wr_iack(arg_clid);
                    this->byte_rd(arg_clid, CMDR_LAST_RD, rxdr, stat);
                    ref_rdat.push_back(rxdr);
                    return;
                }
                else
                {
                    this->byte_wr_iack(arg_clid);
                    this->byte_rd(arg_clid, CMDR_DATA_RD, rxdr, stat);
                    ref_rdat.push_back(rxdr);

                    if (this->get_stat_clarb(stat))
                    {
                        break;
                    }

                    if (this->get_stat_rxack(stat))
                    {
                        this->byte_wr_iack(arg_clid);
                        this->byte_wr_stop(arg_clid);
                        return;
                    }
                }
            }

            if (this->get_stat_clarb(stat))
            {
                continue;
            }
        }
    }

    bool I2C_Ctrl::get_stat_rxack(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_7_RXACK;
    }

    bool I2C_Ctrl::get_stat_busy(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_6_BUSY;
    }

    bool I2C_Ctrl::get_stat_clarb(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_5_CLARB;
    }

    bool I2C_Ctrl::get_stat_tmode(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_4_TMODE;
    }

    bool I2C_Ctrl::get_stat_tdav(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_3_TDAV;
    }

    bool I2C_Ctrl::get_stat_tdrq(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_2_TDRQ;
    }

    bool I2C_Ctrl::get_stat_ctip(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_1_CTIP;
    }

    bool I2C_Ctrl::get_stat_cirq(const uint32_t &arg_stat)
    {
        return arg_stat & I2C_STAT_0_CIRQ;
    }



    //-- Utility ---------------------------------------------------------------
    string I2C_Ctrl::decode_cmdr(const uint32_t &arg_cmdr)
    {
        int    idx = 0;
        string ret = "";

        for (int i = 0 ; i < 8 ; i++)
        {
            idx = 8 - 1 - i;

            if (arg_cmdr & (1 << idx))
            {
                switch (idx)
                {
                    case 0 : ret = ((ret == "") ? "IACK" : ret+"+IACK" ); break;
                    case 1 : ret = ((ret == "") ? "TACK" : ret+"+TACK" ); break;
                    case 3 : ret = ((ret == "") ? "NAK"  : ret+"+NAK"  ); break;
                    case 4 : ret = ((ret == "") ? "WR"   : ret+"+WR"   ); break;
                    case 5 : ret = ((ret == "") ? "RD"   : ret+"+RD"   ); break;
                    case 6 : ret = ((ret == "") ? "STO"  : ret+"+STO"  ); break;
                    case 7 : ret = ((ret == "") ? "STA"  : ret+"+STA"  ); break;
                    default : break;
                }
            }
        }

        return ret;
    }

    void I2C_Ctrl::create_gtkw_cmdr_filter(void)
    {
        string   work = Base::get_plusarg("WRK");
        ofstream ofs;

        ofs.open(work+FS+GTKW_CMDR_32_FNAM);

        ofs << "#" << endl;
        ofs << "# tb::I2C_Ctrl::cmdr" << endl;
        ofs << "#" << endl;

        ofs << setfill('0') << setw(8) << hex << CMDR_DADR_WR << SP << "STA+WR"     << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_DATA_WR << SP << "WR"         << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_DATA_RD << SP << "RD"         << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_LAST_WR << SP << "WR+STO"     << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_LAST_RD << SP << "RD+STO+NAK" << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_STOP    << SP << "STO"        << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_TACK    << SP << "TACK"       << endl;
        ofs << setfill('0') << setw(8) << hex << CMDR_IACK    << SP << "IACK"       << endl;

        ofs.close();
        this->msg->inf("Created GTKW filter file"+SP+work+FS+GTKW_CMDR_32_FNAM);

        ofs.open(work+FS+GTKW_CMDR_08_FNAM);

        ofs << "#" << endl;
        ofs << "# RTL i2c_ctrl" << endl;
        ofs << "#" << endl;

        ofs << setfill('0') << setw(2) << hex << CMDR_DADR_WR << SP << "STA+WR"     << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_DATA_WR << SP << "WR"         << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_DATA_RD << SP << "RD"         << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_LAST_WR << SP << "WR+STO"     << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_LAST_RD << SP << "RD+STO+NAK" << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_STOP    << SP << "STO"        << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_TACK    << SP << "TACK"       << endl;
        ofs << setfill('0') << setw(2) << hex << CMDR_IACK    << SP << "IACK"       << endl;

        ofs.close();
        this->msg->inf("Created GTKW filter file"+SP+work+FS+GTKW_CMDR_08_FNAM);
    }

    uint32_t I2C_Ctrl::fifo_id_req(void)
    {
        uint32_t ret = this->fifo_count;

        if (this->fifo_id_nxt < this->fifo_count)
        {
            ret = this->fifo_id_nxt;
            this->fifo_id_nxt++;
        }
        else
        {
            msg->err("fifo_id_req(): no more FIFOs available.  Stop.");
            run_req_end = true;
            wait();
        }

        return ret;
    }
}

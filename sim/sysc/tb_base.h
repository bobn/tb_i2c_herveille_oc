
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TBBASE_H_
    #define _TBBASE_H_

    #include <random>
    #include <systemc>
    #include <tb_msg.h>
    #include <verilated_vcd_sc.h>

    namespace tb
    {
        extern bool            run_req_end;
        extern sc_trace_file * trace_sysc;

        typedef std::default_random_engine              t_dre;
        typedef std::uniform_int_distribution<uint32_t> t_uri;
        typedef std::uniform_real_distribution<double>  t_urd;

        class Base
        {
            protected:
                pMsg              msg;
                VerilatedVcdSc *  tfp;

            public:

                //-- Constructor/Destructor ------------------------------------
                Base(void);
                ~Base(void);

                //-- PlusArg Get -----------------------------------------------
                static void get_plusarg(const std::string & arg_key, std::string & ref_val);
                static std::string get_plusarg(const std::string & arg_key);
                static double get_plusarg_double(const std::string & arg_key);
                static int get_plusarg_int(const std::string & arg_key);

                //-- Tracing ---------------------------------------------------
                virtual void vltr_tracing(void) = 0;
                void vltr_tracing_open(void);
                void vltr_tracing_close(void);
                void sysc_tracing_open(void);

                //-- Simulate --------------------------------------------------
                void await_sysc_elab(void);
                void simulate(void);
                virtual void cleanup(void) = 0;
        };

        //-- const_input() -----------------------------------------------------
        //       In i_mod
        //           sc_core::sc_in <bool>      strb_i;
        //           sc_core::sc_in <uint32_t>  data_i;
        //       In i_tb
        //           this->i_mod->strb_i ( const_input(true)        );
        //           this->i_mod->data_i ( const_input((uint32_t)0) );
        //--
        template<typename T>
        sc_core::sc_signal_in_if<T> const & const_input( T const & v )
        {
            sc_core::sc_signal<T>* sig_p = new sc_core::sc_signal<T>();
            sig_p->write( v );
            return *sig_p;
        }

        //-- open_output -------------------------------------------------------
        //       In i_mod
        //           sc_core::sc_out <bool>      strb_o;
        //           sc_core::sc_out <uint32_t>  data_o;
        //       In i_tb
        //           this->i_mod->strb_o ( open_output );
        //           this->i_mod->data_o ( open_output );
        //--
        static struct
        {
            template < typename T >
            operator sc_core::sc_signal_inout_if<T> & () const
            {
                return *(new sc_core::sc_signal<T>());
            }
        }
        const open_output = {};
    }
#endif


// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_i2c_targ_phy.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;
    using namespace Parameters;

    //-- Constructor/Destructor ------------------------------------------------
    I2C_Targ_Phy::I2C_Targ_Phy(sc_module_name arg_nm, uint32_t arg_stretch_enbl) : sc_module (arg_nm)
    {
        this->msg          = pMsg(new Msg(this->name()));
        this->stretch_enbl = (arg_stretch_enbl & 0x1);

        SC_THREAD(scl);
        SC_THREAD(sda);
        SC_THREAD(fsm);
    }

    I2C_Targ_Phy::~I2C_Targ_Phy(void) {}


    //-- Virtual ---------------------------------------------------------------
    void I2C_Targ_Phy::end_of_elaboration(void)
    {
        this->msg->inf("tb::I2C_Targ_Phy::end_of_elaboration()...");

        sc_trace(trace_sysc, this->sda_i,         this->name()+PT+"sda_i"         );
        sc_trace(trace_sysc, this->sda_o,         this->name()+PT+"sda_o"         );
        sc_trace(trace_sysc, this->sda_seq,       this->name()+PT+"sda_seq"       );
        sc_trace(trace_sysc, this->sda_idx,       this->name()+PT+"sda_idx"       );
        sc_trace(trace_sysc, this->scl_i,         this->name()+PT+"scl_i"         );
        sc_trace(trace_sysc, this->scl_o,         this->name()+PT+"scl_o"         );
        sc_trace(trace_sysc, this->dbg_fsm_type,  this->name()+PT+"dbg_fsm_type"  );
        sc_trace(trace_sysc, this->dbg_fsm_state, this->name()+PT+"dbg_fsm_state" );
        sc_trace(trace_sysc, this->dbg_bit_eot,   this->name()+PT+"dbg_bit_eot"   );
        sc_trace(trace_sysc, this->dbg_bit_data,  this->name()+PT+"dbg_bit_data"  );

        this->create_gtkw_fsm_filter();
        this->create_gtkw_typ_filter();

        this->msg->inf("tb::I2C_Targ_Phy::end_of_elaboration() done.");
    }


    //-- Top-level Thread ------------------------------------------------------
    void I2C_Targ_Phy::scl(void)
    {
        double   clk_rate_hz      = Base::get_plusarg_double("CLK_RATE_HZ");
        double   scl_rate_hz      = Base::get_plusarg_double("SCL_RATE_HZ");
        double   scl_prescale     = clk_rate_hz / (5.0 * scl_rate_hz);
        double   clk_period_sec   = 1.0 / clk_rate_hz;
        double   scl_low_sec      = scl_prescale * 3.0 * clk_period_sec;
        double   scl_stretch_1    = scl_low_sec * 0.90;
        double   scl_stretch_2    = 0.0;
        string   txt              = "";

        t_dre    engine;
        t_urd    distribution(0.0, scl_low_sec);

        this->scl_o.write(true);

        txt = "SCL Initialized:";

        if (this->stretch_enbl)
        {
            txt = txt+NL+this->msg->get_shift_string(1)+"CLK_RATE_HZ         -"+SP+to_string(clk_rate_hz);
            txt = txt+NL+this->msg->get_shift_string(1)+"SCL_RATE_HZ         -"+SP+to_string(scl_rate_hz);
            txt = txt+NL+this->msg->get_shift_string(1)+"Prescale            -"+SP+to_string(scl_prescale);
            txt = txt+NL+this->msg->get_shift_string(1)+"SCL low (us)        -"+SP+to_string(scl_low_sec * 1e6);
            txt = txt+NL+this->msg->get_shift_string(1)+"SCL stretch 1 (us)  -"+SP+to_string(scl_stretch_1 * 1e6);
        }
        else
        {
            txt = txt+SP+"stretch disabled";
        }

        this->msg->inf(txt);

        while (true)
        {
            wait(this->scl_i.negedge_event());
            wait(scl_stretch_1, SC_SEC);

            if (this->scl_i.read() == true)
            {
                this->msg->nok("Early rising edge detected for SCL at stretch 1");
            }
            else if (this->stretch_enbl)
            {
                scl_stretch_2 = distribution(engine);

                if (false)
                {
                    txt = "Negedge SCL, stretch (us):"+SP+to_string(scl_stretch_2 * 1e6);
                    this->msg->dbg(txt);
                }

                this->scl_o.write(false);

                wait(scl_stretch_2, SC_SEC);

                this->scl_o.write(true);
            }
            else
            {
                this->scl_o.write(true);
            }
        }
    }

    void I2C_Targ_Phy::sda(void)
    {
        bool     sda_bit = true;
        sc_bv<9> sda_seq = I2CT_SEQ_NAK;

        this->sda_seq.write(sda_seq);
        this->sda_o.write(sda_bit);

        this->msg->inf("SDA Initialized.");

        while (true)
        {
            wait (this->seq_i.value_changed_event() | this->sda_idx.value_changed_event());

            sda_seq = this->seq_i.read();
            sda_seq = sda_seq.reverse();
            sda_bit = (bool)sda_seq[this->sda_idx];

            this->sda_seq.write(sda_seq);
            this->sda_o.write(sda_bit);
        }
    }

    void I2C_Targ_Phy::fsm(void)
    {
        uint32_t     bit_idx     = 0;
        sc_bv<9>     bit_data    = 0;
        bool         bit_eot     = false;
        t_i2c_evt    fsm_type    = I2CT_IDLE;
        sc_bv<8>     fsm_data    = 0;
        t_fsm        fsm_state   = FSM_STOP;
        stringstream ss;
        bool         sda;
        bool         scl;

        this->evt_typ_o.write(I2CT_IDLE);
        this->data_o.write(I2CT_SEQ_NAK);
        this->sda_idx.write(bit_idx);
        this->dbg_bit_data.write(bit_data.to_uint());
        this->dbg_bit_eot.write(bit_eot);
        this->dbg_fsm_type.write(fsm_type);
        this->dbg_fsm_state.write(fsm_state);

        this->msg->inf("FSM Initialized.");

        while (true)
        {
            wait(this->sda_i.value_changed_event() | this->scl_i.value_changed_event());

            sda = this->sda_i.read();
            scl = this->scl_i.read();

            switch (fsm_state)
            {
                case FSM_STOP :
                    bit_data  = 0;
                    bit_idx   = 0;
                    bit_eot   = false;

                    if (!scl)
                    {
                        fsm_type  = I2CT_ERR_STOP_SCL_LOW;
                        fsm_state = FSM_RCOV1;

                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second << ": SCL low before SDA; waiting for STOP";
                        this->msg->nok(ss.str());
                    }
                    else if (!sda)
                    {
                        fsm_type  = I2CT_START;
                        fsm_state = FSM_START;
                    }

                    break;
                case FSM_START :
                    bit_data  = 0;
                    bit_idx   = 0;
                    bit_eot   = false;

                    if (sda)
                    {
                        fsm_type  = I2CT_ERR_START_SDA_HIGH;
                        fsm_state = FSM_RCOV1;

                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second << ": SDA changes while SCL high; waiting for STOP";
                        this->msg->nok(ss.str());
                    }
                    else if (!scl)
                    {
                        fsm_state = FSM_CHANGE;
                    }

                    break;
                case FSM_CHANGE :
                    bit_data[bit_idx] = sda;

                    if (scl)
                    {
                        fsm_state = FSM_HOLD;
                    }

                    break;
                case FSM_HOLD :
                    if (!scl)
                    {
                        if (bit_idx >= 8)
                        {
                            if (bit_data[8])
                            {
                                fsm_type  = I2CT_NAK;
                            }
                            else
                            {
                                fsm_type  = I2CT_ACK;
                            }

                            bit_data  = 0;
                            bit_idx   = 0;
                            bit_eot   = true;
                        }
                        else if (bit_idx == 7)
                        {
                            if ((fsm_type == I2CT_ASTB) && bit_data[7])
                            {
                                fsm_type  = I2CT_RSTB;
                            }
                            else if (fsm_type == I2CT_ASTB)
                            {
                                fsm_type  = I2CT_WSTB;
                            }
                            else if (fsm_type == I2CT_XFER)
                            {
                                fsm_type  = I2CT_DSTRB;
                            }

                            fsm_data  = bit_data.range(7, 0);
                            fsm_data  = fsm_data.reverse();
                            bit_idx   = bit_idx + 1;
                            bit_eot   = false;
                        }
                        else if (bit_idx == 6)
                        {
                            if (fsm_type == I2CT_START)
                            {
                                fsm_type  = I2CT_ASTB;
                            }
                            else if (fsm_type == I2CT_RESTART)
                            {
                                fsm_type  = I2CT_ASTB;
                            }

                            bit_idx   = bit_idx + 1;
                            bit_eot   = false;
                        }
                        else
                        {
                            if (fsm_type == I2CT_ACK)
                            {
                                fsm_type  = I2CT_XFER;
                            }

                            bit_idx   = bit_idx + 1;
                            bit_eot   = false;
                        }

                        fsm_state = FSM_CHANGE;
                    }
                    else if (sda)
                    {
                        fsm_type  = I2CT_STOP;
                        bit_eot   = false;
                        fsm_state = FSM_STOP;
                    }
                    else if (!sda)
                    {
                        if (bit_eot)
                        {
                            fsm_type  = I2CT_RESTART;
                            bit_eot   = false;
                            fsm_state = FSM_START;
                        }
                        else
                        {
                            ss = stringstream();
                            ss << fsm_pair_array[fsm_state].second << ": SDA changes during hold for capture; waiting for STOP";
                            this->msg->nok(ss.str());

                            fsm_type  = I2CT_ERR_HOLD_SDA_CHANGE;
                            fsm_state = FSM_RCOV1;
                        }
                    }
                    break;
                case FSM_RCOV1 :
                    if (scl && sda)
                    {
                        fsm_state = FSM_RCOV2;
                    }

                    break;
                case FSM_RCOV2 :
                    if (!scl)
                    {
                        fsm_state = FSM_RCOV1;
                    }
                    else if (!sda)
                    {
                        fsm_type  = I2CT_START;
                        fsm_state = FSM_START;
                    }

                    break;
                case FSM_ERR :
                    break;
                default :
                    fsm_type  = I2CT_ERR_HALT;
                    fsm_state = FSM_ERR;

                    this->msg->nok("Case 'default'; halting FSM");
                    break;
            }

            this->evt_typ_o.write(fsm_type);
            this->data_o.write(fsm_data.to_uint());
            this->sda_idx.write(bit_idx);

            if (DBG_SIGNALS)
            {
                this->dbg_bit_data.write(bit_data.to_uint());
                this->dbg_bit_eot.write(bit_eot);
                this->dbg_fsm_type.write(fsm_type);
                this->dbg_fsm_state.write(fsm_state);
            }
        }
    }

    //-- Utility ---------------------------------------------------------------
    void I2C_Targ_Phy::create_gtkw_fsm_filter(void)
    {
        string   fnam = Base::get_plusarg("WRK")+FS+GTKW_FSM_FILTER_FNAM;
        ofstream ofs;

        ofs.open(fnam);

        ofs << "#" << endl;
        ofs << "# tb::I2C_Targ_Phy::fsm_state" << endl;
        ofs << "#" << endl;

        for (uint32_t i = 0 ; i < FSM_SIZE ; i++)
        {
            ofs << setfill('0') << setw(8) << hex << fsm_pair_array[i].first;
            ofs << SP;
            ofs << fsm_pair_array[i].second;
            ofs << endl;
        }

        ofs.close();
        this->msg->inf("Created GTKW filter file"+SP+fnam);
    }

    void I2C_Targ_Phy::create_gtkw_typ_filter(void)
    {
        string   fnam = Base::get_plusarg("WRK")+FS+GTKW_TYP_FILTER_FNAM;
        ofstream ofs;

        ofs.open(fnam);

        ofs << "#" << endl;
        ofs << "# tb::I2C_Targ_Phy::fsm_type" << endl;
        ofs << "#" << endl;

        for (uint32_t i = 0 ; i < I2CT_SIZE ; i++)
        {
            ofs << setfill('0') << setw(8) << hex << i2c_typ_pair_array[i].first;
            ofs << SP;
            ofs << i2c_typ_pair_array[i].second;
            ofs << endl;
        }

        ofs.close();
        this->msg->inf("Created GTKW filter file"+SP+fnam);
    }
}

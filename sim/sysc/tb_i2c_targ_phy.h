
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TB_I2C_TARG_PHY_H_
    #define _TB_I2C_TARG_PHY_H_

    #define SC_INCLUDE_DYNAMIC_PROCESSES

    #include <memory>
    #include <string>
    #include <iomanip>
    #include <utility>
    #include <fstream>
    #include <systemc>
    #include <tb_base.h>
    #include <tb_msg.h>

    namespace tb
    {
        namespace Parameters
        {
            const uint32_t I2CT_SEQ_NAK   = 0x1FF;
            const uint32_t I2CT_SEQ_ACK   = 0x1FE;
            const uint32_t I2CT_DADR_MASK = 0x0FE;
            const uint32_t I2CT_RDWR_BIT  = 0x001;

            enum t_i2c_evt : uint32_t
            {
                I2CT_IDLE,
                I2CT_START,
                I2CT_RESTART,
                I2CT_XFER,
                I2CT_ASTB,
                I2CT_RSTB,
                I2CT_WSTB,
                I2CT_DSTRB,
                I2CT_ACK,
                I2CT_NAK,
                I2CT_STOP,
                I2CT_ERR_HALT,
                I2CT_ERR_STOP_SCL_LOW,
                I2CT_ERR_HOLD_SDA_CHANGE,
                I2CT_ERR_START_SDA_HIGH,
                I2CT_ERR_OTHER,
                I2CT_SIZE
            };

            typedef std::pair <t_i2c_evt, std::string> t_i2c_evt_pair;

            const std::array <t_i2c_evt_pair, I2CT_SIZE> i2c_typ_pair_array
            {
                {
                    { I2CT_IDLE,                  "I2CT_IDLE"                 },
                    { I2CT_START,                 "I2CT_START"                },
                    { I2CT_RESTART,               "I2CT_RESTART"              },
                    { I2CT_XFER,                  "I2CT_XFER"                 },
                    { I2CT_ASTB,                  "I2CT_ASTB"                 },
                    { I2CT_RSTB,                  "I2CT_RSTB"                 },
                    { I2CT_WSTB,                  "I2CT_WSTB"                 },
                    { I2CT_DSTRB,                 "I2CT_DSTRB"                },
                    { I2CT_ACK,                   "I2CT_ACK"                  },
                    { I2CT_NAK,                   "I2CT_NAK"                  },
                    { I2CT_STOP,                  "I2CT_STOP"                 },
                    { I2CT_ERR_HALT,              "I2CT_ERR_HALT"             },
                    { I2CT_ERR_STOP_SCL_LOW,      "I2CT_ERR_STOP_SCL_LOW"     },
                    { I2CT_ERR_HOLD_SDA_CHANGE,   "I2CT_ERR_HOLD_SDA_CHANGE"  },
                    { I2CT_ERR_START_SDA_HIGH,    "I2CT_ERR_START_SDA_HIGH"   },
                    { I2CT_ERR_OTHER,             "I2CT_ERR_OTHER"            },
                },
            };
        }

        class I2C_Targ_Phy : public sc_core::sc_module
        {
            private:
                const bool        DBG_SIGNALS          = true;
                const std::string GTKW_FSM_FILTER_FNAM = "i2c_targ_phy_fsm.filter";
                const std::string GTKW_TYP_FILTER_FNAM = "i2c_targ_phy_typ.filter";

                enum t_fsm : uint32_t
                {
                    FSM_STOP,
                    FSM_START,
                    FSM_CHANGE,
                    FSM_HOLD,
                    FSM_RCOV1,
                    FSM_RCOV2,
                    FSM_ERR,
                    FSM_SIZE
                };

                typedef std::pair <t_fsm, std::string>         t_i2c_fsm_pair;

                const std::array <t_i2c_fsm_pair, FSM_SIZE> fsm_pair_array
                {
                    {
                        { FSM_STOP,    "FSM_STOP"   },
                        { FSM_START,   "FSM_START"  },
                        { FSM_CHANGE,  "FSM_CHANGE" },
                        { FSM_HOLD,    "FSM_HOLD"   },
                        { FSM_RCOV1,   "FSM_RCOV1"  },
                        { FSM_RCOV2,   "FSM_RCOV2"  },
                        { FSM_ERR,     "FSM_ERR"    },
                    },
                };

                pMsg     msg;
                uint32_t stretch_enbl;

            public:
                sc_core::sc_out < bool     >  sda_o;
                sc_core::sc_out < bool     >  scl_o;
                sc_core::sc_out < uint32_t >  evt_typ_o;
                sc_core::sc_out < uint32_t >  data_o;
                sc_core::sc_in  < uint32_t >  seq_i;
                sc_core::sc_in  < bool     >  sda_i;
                sc_core::sc_in  < bool     >  scl_i;

                sc_signal <sc_bv<9>> sda_seq;
                sc_signal <uint32_t> sda_idx;

                sc_signal <uint32_t> dbg_bit_data;
                sc_signal <bool>     dbg_bit_eot;
                sc_signal <uint32_t> dbg_fsm_type;
                sc_signal <uint32_t> dbg_fsm_state;

                SC_HAS_PROCESS(I2C_Targ_Phy);

                //-- Constructor/Destructor ------------------------------------
                I2C_Targ_Phy(sc_core::sc_module_name, uint32_t arg_stretch_enbl);
                ~I2C_Targ_Phy(void);

                //-- Virtual ---------------------------------------------------
                virtual void end_of_elaboration(void);

                //-- Top-level Thread ------------------------------------------
                void scl(void);
                void sda(void);
                void fsm(void);

                //-- Utility ---------------------------------------------------
                void create_gtkw_fsm_filter(void);
                void create_gtkw_typ_filter(void);
        };

        typedef std::unique_ptr<I2C_Targ_Phy> pI2C_Targ_Phy;
    }
#endif

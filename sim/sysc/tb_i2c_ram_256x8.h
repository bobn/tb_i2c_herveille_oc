
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TB_I2C_RAM_256X8_H_
    #define _TB_I2C_RAM_256X8_H_

    #define SC_INCLUDE_DYNAMIC_PROCESSES

    #include <memory>
    #include <string>
    #include <iomanip>
    #include <utility>
    #include <unordered_map>
    #include <systemc>
    #include <tb_base.h>
    #include <tb_msg.h>
    #include <tb_i2c_targ_phy.h>

    namespace tb
    {
        class I2C_RAM_256x8 : public sc_core::sc_module
        {
            private:
                const bool        DBG_SIGNALS          = true;
                const std::string GTKW_FSM_FILTER_FNAM = "i2c_ram_256x8_fsm.filter";

                enum t_fsm : uint32_t
                {
                    FSM_IDLE,
                    FSM_START,
                    FSM_DADR,
                    FSM_WR,
                    FSM_WR_ACK,
                    FSM_WR_XFER,
                    FSM_WR_BYTE,
                    FSM_RD,
                    FSM_RD_ACK,
                    FSM_RD_XFER,
                    FSM_HALT,
                    FSM_SIZE
                };

                typedef std::pair <t_fsm, std::string> t_fsm_pair;

                const std::array <t_fsm_pair, FSM_SIZE> fsm_pair_array
                {
                    {
                        { FSM_IDLE,      "FSM_IDLE"    },
                        { FSM_START,     "FSM_START"   },
                        { FSM_DADR,      "FSM_DADR"    },
                        { FSM_WR,        "FSM_WR"      },
                        { FSM_WR_ACK,    "FSM_WR_ACK"  },
                        { FSM_WR_XFER,   "FSM_WR_XFER" },
                        { FSM_WR_BYTE,   "FSM_WR_BYTE" },
                        { FSM_RD,        "FSM_RD"      },
                        { FSM_RD_ACK,    "FSM_RD_ACK"  },
                        { FSM_RD_XFER,   "FSM_RD_XFER" },
                        { FSM_HALT,      "FSM_HALT"    },
                    },
                };

                typedef std::unordered_map<uint32_t, uint32_t> ram_256x8;

                pMsg         msg;
                uint32_t     dadr_byte;
                ram_256x8    ram;

            public:
                pI2C_Targ_Phy i_phy;

                sc_core::sc_out < bool >  sda_o;
                sc_core::sc_out < bool >  scl_o;
                sc_core::sc_in  < bool >  sda_i;
                sc_core::sc_in  < bool >  scl_i;

                sc_signal <uint32_t> phy_evt_typ;
                sc_signal <uint32_t> phy_data;
                sc_signal <uint32_t> phy_seq;

                sc_signal <uint32_t> dbg_fsm_rdat;
                sc_signal <uint32_t> dbg_fsm_idx;
                sc_signal <uint32_t> dbg_fsm_addr;
                sc_signal <uint32_t> dbg_fsm_state;

                SC_HAS_PROCESS(I2C_RAM_256x8);

                //-- Constructor/Destructor ------------------------------------
                I2C_RAM_256x8(sc_core::sc_module_name, uint32_t arg_dadr_byte, uint32_t arg_stretch_enbl);
                ~I2C_RAM_256x8(void);

                //-- Virtual ---------------------------------------------------
                virtual void end_of_elaboration(void);

                //-- Top-level Thread ------------------------------------------
                void fsm(void);

                //-- Utility ---------------------------------------------------
                void     clr_ram_data(void);
                void     set_ram_data(const uint32_t &arg_addr, const uint32_t &arg_data);
                void     get_ram_data(const uint32_t &arg_addr, uint32_t &arg_data);
                uint32_t get_ram_data(const uint32_t &arg_addr);
                void     create_gtkw_fsm_filter(void);
        };

        typedef std::unique_ptr<I2C_RAM_256x8> pI2C_RAM_256x8;
    }
#endif

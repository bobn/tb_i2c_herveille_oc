
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_rst.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;

    Rst::Rst(sc_module_name arg_nm, bool arg_active_high) : sc_module (arg_nm)
    {
        this->msg         = pMsg(new Msg(this->name()));
        this->active_high = arg_active_high;
        this->state       = arg_active_high;
        this->req_level   = this->state;

        SC_THREAD(run);
    }

    Rst::~Rst(void) {}

    void Rst::run(void)
    {
        if (this->state)
        {
            this->msg->inf("Starting with reset active high");
        }
        else
        {
            this->msg->inf("Starting with reset active low");
        }

        this->rst_o.write(this->state);

        while (true)
        {
            wait(req_trig);

            if (req_level == state)
            {
                this->msg->inf("Ignoring request, already at requested level");
            }
            else
            {
                this->state = req_level;

                if (req_level == this->active_high)
                {
                    this->msg->inf("Activating reset");
                }
                else
                {
                    this->msg->inf("Deactivating reset");
                }
            }

            this->rst_o.write(this->state);
        }
    }

    void Rst::activate(void)
    {
        this->req_level = (this->active_high) ? (bool)1 : (bool)0;
        this->req_trig.notify();
    }

    void Rst::deactivate(void)
    {
        this->req_level = (this->active_high) ? (bool)0 : (bool)1;
        this->req_trig.notify();
    }
}

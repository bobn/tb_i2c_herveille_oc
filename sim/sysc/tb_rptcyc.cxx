
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <time.h>
#include <tb_rptcyc.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;

    RptCyc::RptCyc(sc_module_name arg_nm, sc_time arg_time_report) : sc_module (arg_nm)
    {
        this->msg         = pMsg(new Msg(this->name()));
        this->time_report = arg_time_report;

        gettimeofday(&(this->tv0), NULL);
        SC_THREAD(run);
    }

    RptCyc::~RptCyc(void) {}

    void RptCyc::run(void)
    {
        unsigned     cyc     = 0;
        string       rpt_str = this->time_report.to_string()+SP+"elapsed, cycle";
        timeval      tv1;
        timeval      tv2;
        double       diff;

        gettimeofday(&tv1, NULL);
        gettimeofday(&tv2, NULL);

        this->msg->inf("Starting with period"+SP+this->time_report.to_string());

        while (true)
        {
            wait(this->time_report);
            gettimeofday(&tv2, NULL);
            diff = ((tv2.tv_sec * 1e6) + tv2.tv_usec) - ((tv1.tv_sec * 1e6) + tv1.tv_usec);
            cyc = cyc + 1;
            msg->inf(rpt_str+SP+to_string(cyc)+CM+SP+"which took"+SP+to_string(diff/1e6)+SP+"seconds");
            tv1.tv_sec = tv2.tv_sec;
            tv1.tv_usec = tv2.tv_usec;
        }
    }

    void RptCyc::performance(void)
    {
        timeval tv1;
        double  bgn_time_sec;
        double  end_time_sec;
        double  wall_time_sec;
        double  sim_time_msec;
        double  performance;
        string  rpt_str;

        if (sc_time_stamp() == SC_ZERO_TIME)
        {
            this->msg->inf("No sim time elapsed, skipping performance report");
            return;
        }

        gettimeofday(&tv1, NULL);

        bgn_time_sec  = ((this->tv0).tv_sec * 1e6) + (this->tv0).tv_usec;
        end_time_sec  = (tv1.tv_sec * 1e6) + tv1.tv_usec;
        wall_time_sec = (end_time_sec - bgn_time_sec) / 1e6;
        sim_time_msec = (sc_time_stamp()/sc_time(1, SC_MS));
        performance   = wall_time_sec/sim_time_msec;

        rpt_str = "Performance report:";
        rpt_str = rpt_str+NL+this->msg->get_shift_string(1);
        rpt_str = rpt_str+"Wall clock run time is";
        rpt_str = rpt_str+SP+to_string(wall_time_sec);
        rpt_str = rpt_str+SP+"sec";
        rpt_str = rpt_str+NL+this->msg->get_shift_string(1);
        rpt_str = rpt_str+"Sim run time is";
        rpt_str = rpt_str+SP+to_string(sim_time_msec);
        rpt_str = rpt_str+SP+"msec";
        rpt_str = rpt_str+NL+this->msg->get_shift_string(1);
        rpt_str = rpt_str+"Performance is";
        rpt_str = rpt_str+SP+to_string(performance);
        rpt_str = rpt_str+SP+"sec per sim msec";

        this->msg->inf(rpt_str);
    }
}

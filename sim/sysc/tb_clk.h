
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TBCLK_H_
    #define _TBCLK_H_

    #include <memory>
    #include <string>
    #include <systemc>
    #include <tb_msg.h>

    namespace tb
    {
        class Clk : public sc_core::sc_module
        {
            private:
                pMsg                  msg;
                sc_core::sc_clock *   clk_src;
                unsigned long         freq_hz;
                sc_core::sc_time      clk_period;
                double                duty_cycle;
                sc_core::sc_time      start_time;
                bool                  posedge_first;
                std::string           clk_src_nm;

            public:
                SC_HAS_PROCESS(Clk);

                Clk
                (
                    sc_core::sc_module_name arg_nm,
                    unsigned long           arg_hz,
                    double                  arg_duty,
                    sc_core::sc_time        arg_start,
                    bool                    arg_posedge

                );
                ~Clk(void);

                sc_core::sc_out < bool > clk_o;

                void run(void);
                void sync(void);
                void negedge(void);
                void await_value_32(const sc_core::sc_signal<uint32_t>&, const uint32_t&);
                void await_posedge(const sc_core::sc_signal<bool>&);
        };

        typedef std::unique_ptr<Clk> pClk;
    }
#endif

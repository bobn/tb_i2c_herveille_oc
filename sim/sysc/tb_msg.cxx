
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <systemc>
#include <tb_base.h>
#include <tb_msg.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;

    const string LVL_DBG = SP+"DBG"+SP;
    const string LVL_INF = string(5, SP[0]);
    const string LVL_WRN = SP+"WRN"+SP;
    const string LVL_ERR = SP+"ERR"+SP;
    const string LVL_AOK = SP+"AOK"+SP;
    const string LVL_NOK = "*NOK*";

    uint32_t Msg::nok_count = 0;

    Msg::Msg(const string   &arg_tag)
    {
        this->tags.push_back(arg_tag);

        this->context       = arg_tag;
        this->time_base     = sc_time(Base::get_plusarg_double("TIME_BASE_SEC"), SC_SEC);
        this->time_needed   = sc_time(Base::get_plusarg_double("TIME_NEEDED_SEC"), SC_SEC);
        this->time_prec     = Base::get_plusarg_int("TIME_PREC_DIGITS");
        this->scale         = this->time_base.to_double()/sc_get_time_resolution().to_double();
        this->fmt           = "%";
        this->fmt           = this->fmt+to_string((unsigned)ceil(log10(this->time_needed/this->time_base))+this->time_prec+1);
        this->fmt           = this->fmt+PT;
        this->fmt           = this->fmt+to_string(this->time_prec);
        this->fmt           = this->fmt+"f"+SP;
        this->fmt           = this->fmt+this->time_base.to_string().substr(2);
        this->fmt           = this->fmt+"%s%s %s"+NL;
    }
    Msg::~Msg(void) {}

    string & Msg::get_context(void)
    {
        return this->context;
    }

    void Msg::pop_tag(void)
    {
        if (this->tags.empty() > 0) return;

        this->tags.pop_back();

        this->context.clear();
        for (auto it = begin(this->tags) ; it != end(this->tags) ; ++it)
        {
            if (this->context.empty())
            {
                this->context = (*it);
            }
            else
            {
                this->context = (this->context)+PT+(*it);
            }
        }
    }

    void Msg::push_tag(const string& arg_str)
    {

        this->tags.push_back(arg_str);

        this->context.clear();
        for (auto it = begin(this->tags) ; it != end(this->tags) ; ++it)
        {
            if (this->context.empty())
            {
                this->context = (*it);
            }
            else
            {
                this->context = (this->context)+PT+(*it);
            }
        }
    }

    void Msg::msg(const string& arg_str, const string& arg_lvl)
    {
        printf
        (
            this->fmt.c_str(),
            sc_time_stamp().to_double()/this->scale,
            arg_lvl.c_str(),
            (this->context).c_str(),
            arg_str.c_str()
        );
        cout.flush();

        if (arg_lvl == LVL_NOK)
        {
            nok_count++;
        }
    }

    void Msg::dbg(const string& arg_str) { this->msg(arg_str, LVL_DBG); }
    void Msg::inf(const string& arg_str) { this->msg(arg_str, LVL_INF); }
    void Msg::wrn(const string& arg_str) { this->msg(arg_str, LVL_WRN); }
    void Msg::err(const string& arg_str) { this->msg(arg_str, LVL_ERR); }
    void Msg::aok(const string& arg_str) { this->msg(arg_str, LVL_AOK); }
    void Msg::nok(const string& arg_str) { this->msg(arg_str, LVL_NOK); }

    string Msg::get_shift_string(const unsigned arg_level)
    {
        unsigned cols  = (unsigned)ceil(log10(this->time_needed/this->time_base))+this->time_prec+9;

        cols = cols + (arg_level * 4);
        
        return string(cols, SP[0]);
    }

    string Msg::get_hex_string(const uint64_t arg_uint, const unsigned arg_width, const bool arg_fill)
    {
        stringstream ss;

        ss << "0x";

        if (arg_fill)
        {
            ss << setfill('0');
        }

        ss << setw(arg_width) << hex << arg_uint;

        return ss.str();
    }

    string Msg::get_hex_string(const uint64_t arg_uint)
    {
        return get_hex_string(arg_uint, 8, true);
    }

    void Msg::report(const string &arg_str)
    {
        string text = "";

        text = text+"Result";
        text = text+NL+this->get_shift_string(1);

        if (nok_count)
        {
            text = text+"TEST FAILED,"+SP+to_string(nok_count)+SP+"errors,"+SP+SQ+arg_str+SQ;
        }
        else
        {
            text = text+"TEST PASSED for"+SP+SQ+arg_str+SQ;
        }

        this->inf(text);
    }
}

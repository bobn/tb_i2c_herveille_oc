
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TBRST_H_
    #define _TBRST_H_

    #include <memory>
    #include <string>
    #include <systemc>
    #include <tb_msg.h>

    namespace tb
    {
        class Rst : public sc_core::sc_module
        {
            private:
                pMsg              msg;
                bool              active_high;
                bool              state;
                sc_core::sc_event req_trig;
                bool              req_level;

            public:
                SC_HAS_PROCESS(Rst);

                Rst(sc_core::sc_module_name, bool);
                ~Rst(void);

                sc_core::sc_out < bool > rst_o;

                void run(void);
                void activate(void);
                void deactivate(void);
        };

        typedef std::unique_ptr<Rst> pRst;
    }
#endif


// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_timeout.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;

    Timeout::Timeout(sc_module_name arg_nm, sc_time arg_time_needed) : sc_module (arg_nm)
    {
        this->msg         = pMsg(new Msg(this->name()));
        this->time_needed = arg_time_needed;

        SC_THREAD(run);
    }

    Timeout::~Timeout(void) {}

    void Timeout::run(void)
    {
        this->msg->inf("Simulation timeout set to"+SP+this->time_needed.to_string());
        wait(this->time_needed);
        msg->err("Simulation run has timed-out at"+SP+this->time_needed.to_string()+"; requesting stop");
        run_req_end = true;
        wait();
    }
}

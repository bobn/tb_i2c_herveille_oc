
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TBTIMEOUT_H_
    #define _TBTIMEOUT_H_

    #include <memory>
    #include <string>
    #include <systemc>
    #include <tb_base.h>
    #include <tb_msg.h>

    namespace tb
    {
        class Timeout : public sc_core::sc_module
        {
            private:
                pMsg             msg;
                sc_core::sc_time time_needed;

            public:
                SC_HAS_PROCESS(Timeout);

                Timeout(sc_core::sc_module_name, sc_core::sc_time);
                ~Timeout(void);

                void run(void);
        };

        typedef std::unique_ptr<Timeout> pTimeout;
    }
#endif

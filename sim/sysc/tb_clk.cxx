
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_clk.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;

    Clk::Clk
    (
        sc_module_name arg_nm,
        unsigned long  arg_hz,
        double         arg_duty,
        sc_time        arg_start,
        bool           arg_posedge
    )
    : sc_module ( arg_nm )
    {
        this->msg              = pMsg(new Msg(this->name()));
        this->freq_hz          = arg_hz;
        this->duty_cycle       = arg_duty;
        this->start_time       = arg_start;
        this->posedge_first    = arg_posedge;
        this->clk_period       = sc_time((1.0 / (double)this->freq_hz), SC_SEC),
        this->clk_src_nm       = string(arg_nm) + string("_src");

        this->clk_src = new sc_clock
        (
            this->clk_src_nm.c_str(),
            this->clk_period,
            this->duty_cycle,
            this->start_time, 
            this->posedge_first
        );

        SC_THREAD(run);
    }

    Clk::~Clk(void)
    {
        delete this->clk_src;
    }

    void Clk::run(void)
    {
        this->msg->inf("Starting with frequency"+SP+to_string(this->freq_hz)+SP+"Hz");

        while (true) {
            wait(this->clk_src->value_changed_event());

            if (this->clk_src->posedge()) this->clk_o.write((bool)1);
            if (this->clk_src->negedge()) this->clk_o.write((bool)0);
        }
    }

    void Clk::sync(void)
    {
        wait(this->clk_src->posedge_event());
        wait(this->clk_src->negedge_event());
    }

    void Clk::negedge(void)
    {
        wait(this->clk_src->negedge_event());
    }

    void Clk::await_value_32(const sc_signal <uint32_t> &arg_sig, const uint32_t &arg_val)
    {
        while (true)
        {
            wait(arg_sig.value_changed_event());
            if (arg_sig == arg_val) break;
        }
        if (not this->clk_src->negedge()) wait(this->clk_src->negedge_event());
    }

    void Clk::await_posedge(const sc_signal <bool> &arg_sig)
    {
        wait(arg_sig.posedge_event());
        if (not this->clk_src->negedge()) wait(this->clk_src->negedge_event());
    }
}


// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TB_I2C_CTRL_SPEC_H_
    #define _TB_I2C_CTRL_SPEC_H_

    #include <string>
    #include <iomanip>
    #include <systemc>

    namespace tb
    {
        class I2C_Ctrl_Spec
        {
            public:
                uint32_t clid;
                uint32_t cmdr;
                uint32_t txdr;
                uint32_t rxdr;
                uint32_t stat;

                I2C_Ctrl_Spec & operator=(const I2C_Ctrl_Spec & arg_spec)
                {
                    this->clid = arg_spec.clid;
                    this->cmdr = arg_spec.cmdr;
                    this->txdr = arg_spec.txdr;
                    this->rxdr = arg_spec.rxdr;
                    this->stat = arg_spec.stat;
  
                    return *this;
                }
  
                bool operator==(const I2C_Ctrl_Spec & arg_spec)
                const
                {
                    return
                    (
                        this->clid == arg_spec.clid
                        && this->cmdr == arg_spec.cmdr
                        && this->txdr == arg_spec.txdr
                        && this->rxdr == arg_spec.rxdr
                        && this->stat == arg_spec.stat
                    );
                }
        };

        inline std::ostream & operator<<(std::ostream & arg_stream, const I2C_Ctrl_Spec & arg_spec)
        {
            arg_stream << "{";
            arg_stream << "clid:0x" << std::setfill('0') << std::setw(2) << std::hex << arg_spec.clid;
            arg_stream << " ";
            arg_stream << "cmdr:0x" << std::setfill('0') << std::setw(2) << std::hex << arg_spec.cmdr;
            arg_stream << " ";
            arg_stream << "txdr:0x" << std::setfill('0') << std::setw(2) << std::hex << arg_spec.txdr;
            arg_stream << " ";
            arg_stream << "rxdr:0x" << std::setfill('0') << std::setw(2) << std::hex << arg_spec.rxdr;
            arg_stream << " ";
            arg_stream << "stat:0x" << std::setfill('0') << std::setw(2) << std::hex << arg_spec.stat;
            arg_stream << "}";

            return arg_stream;
        }


        inline void sc_trace(sc_core::sc_trace_file * arg_tf, const I2C_Ctrl_Spec & arg_spec, const std::string & nm)
        {
                sc_trace(arg_tf, arg_spec.clid, nm + ".clid");
                sc_trace(arg_tf, arg_spec.cmdr, nm + ".cmdr");
                sc_trace(arg_tf, arg_spec.txdr, nm + ".txdr");
                sc_trace(arg_tf, arg_spec.rxdr, nm + ".rxdr");
                sc_trace(arg_tf, arg_spec.stat, nm + ".stat");
        }

        typedef sc_core::sc_fifo <I2C_Ctrl_Spec> I2CC_FIFO;
        typedef std::unique_ptr<I2CC_FIFO>       pI2CC_FIFO;
        typedef std::deque<pI2CC_FIFO>           pI2CC_FIFOs;
    }
#endif

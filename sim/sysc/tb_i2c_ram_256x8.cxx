
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_i2c_ram_256x8.h>

namespace tb
{
    using namespace std;
    using namespace sc_core;
    using namespace sc_dt;
    using namespace Chars;
    using namespace Parameters;

    //-- Constructor/Destructor ------------------------------------------------
    I2C_RAM_256x8::I2C_RAM_256x8(sc_module_name arg_nm, uint32_t arg_dadr_byte, uint32_t arg_stretch_enbl) : sc_module (arg_nm)
    {
        this->msg       = pMsg(new Msg(this->name()));
        this->dadr_byte = arg_dadr_byte & I2CT_DADR_MASK;
        this->i_phy     = pI2C_Targ_Phy(new I2C_Targ_Phy("i_phy", (arg_stretch_enbl & 0x1)));

        this->i_phy->sda_o       ( this->sda_o        );
        this->i_phy->scl_o       ( this->scl_o        );
        this->i_phy->evt_typ_o   ( this->phy_evt_typ  );
        this->i_phy->data_o      ( this->phy_data     );
        this->i_phy->seq_i       ( this->phy_seq      );
        this->i_phy->sda_i       ( this->sda_i        );
        this->i_phy->scl_i       ( this->scl_i        );

        SC_THREAD(fsm);
    }

    I2C_RAM_256x8::~I2C_RAM_256x8(void) {}


    //-- Virtual ---------------------------------------------------------------
    void I2C_RAM_256x8::end_of_elaboration(void)
    {
        this->msg->inf("tb::I2C_RAM_256x8::end_of_elaboration()...");

        sc_trace(trace_sysc, this->phy_evt_typ,      this->name()+PT+"phy_evt_typ"      );
        sc_trace(trace_sysc, this->phy_data,         this->name()+PT+"phy_data"         );
        sc_trace(trace_sysc, this->phy_seq,          this->name()+PT+"phy_seq"          );
        sc_trace(trace_sysc, this->dbg_fsm_rdat,     this->name()+PT+"dbg_fsm_rdat"     );
        sc_trace(trace_sysc, this->dbg_fsm_idx,      this->name()+PT+"dbg_fsm_idx"      );
        sc_trace(trace_sysc, this->dbg_fsm_addr,     this->name()+PT+"dbg_fsm_addr"     );
        sc_trace(trace_sysc, this->dbg_fsm_state,    this->name()+PT+"dbg_fsm_state"    );

        this->create_gtkw_fsm_filter();

        this->msg->inf("tb::I2C_RAM_256x8::end_of_elaboration() done.");
    }


    //-- Top-level Thread ------------------------------------------------------
    void I2C_RAM_256x8::fsm(void)
    {
        stringstream ss;
        uint32_t     phy_event = 0;
        uint32_t     phy_data  = 0;
        uint32_t     fsm_wdat  = I2CT_SEQ_NAK;
        uint32_t     fsm_rdat  = I2CT_SEQ_NAK;
        uint32_t     fsm_idx   = 0;
        uint32_t     fsm_addr  = 0;
        t_fsm        fsm_state = FSM_IDLE;

        this->phy_seq.write(fsm_wdat);

        this->dbg_fsm_rdat.write(fsm_rdat);
        this->dbg_fsm_idx.write(fsm_idx);
        this->dbg_fsm_addr.write(fsm_addr);
        this->dbg_fsm_state.write(fsm_state);

        ss = stringstream();
        ss << "Starting FSM; DADR is 0x" << setfill('0') << setw(2) << hex << this->dadr_byte;
        this->msg->inf(ss.str());

        while (true)
        {
            wait(this->phy_evt_typ.value_changed_event());

            phy_event = this->phy_evt_typ;
            phy_data  = this->phy_data;
            fsm_wdat  = I2CT_SEQ_NAK;

            switch (fsm_state)
            {
                case FSM_IDLE :
                    fsm_idx = 0;

                    if (phy_event == I2CT_START)
                    {
                        fsm_state = FSM_START;
                    }

                    break;
                case FSM_START :
                    fsm_idx = 0;

                    if (phy_event == I2CT_START)
                    {
                        break;
                    }
                    else if (phy_event == I2CT_ASTB)
                    {
                        fsm_state = FSM_DADR;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_DADR :
                    if ((phy_data & I2CT_DADR_MASK) == this->dadr_byte)
                    {
                        if (phy_event == I2CT_WSTB)
                        {
                            fsm_wdat  = I2CT_SEQ_ACK;
                            fsm_idx   = fsm_idx + 1;
                            fsm_state = FSM_WR;
                        }
                        else if (phy_event == I2CT_RSTB)
                        {
                            fsm_wdat  = I2CT_SEQ_ACK;
                            fsm_idx   = fsm_idx + 1;
                            fsm_state = FSM_RD;
                        }
                        else
                        {
                            ss = stringstream();
                            ss << fsm_pair_array[fsm_state].second;
                            ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                            ss << "; HALT";
                            this->msg->nok(ss.str());

                            fsm_state = FSM_HALT;
                        }
                    }
                    else
                    {
                        fsm_wdat  = I2CT_SEQ_NAK;
                        fsm_state = FSM_IDLE;
                    }

                    break;
                case FSM_WR :
                    if (phy_event == I2CT_ACK)
                    {
                        fsm_state = FSM_WR_ACK;
                    }
                    else if (phy_event == I2CT_NAK)
                    {
                        fsm_state = FSM_IDLE;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_WR_ACK :
                    if (phy_event == I2CT_STOP)
                    {
                        fsm_state = FSM_IDLE;
                    }
                    else if (phy_event == I2CT_XFER)
                    {
                        fsm_state = FSM_WR_XFER;
                    }
                    else if (phy_event == I2CT_RESTART)
                    {
                        fsm_state = FSM_START;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_WR_XFER :
                    if (phy_event == I2CT_DSTRB)
                    {
                        if (fsm_idx < 2)
                        {
                            fsm_addr = phy_data;
                        }
                        else
                        {
                            ram[fsm_addr] = phy_data;

                            if (false)
                            {
                                ss = stringstream();
                                ss << fsm_pair_array[fsm_state].second;
                                ss << ": ram write [0x" << setfill('0') << setw(2) << hex << fsm_addr;
                                ss << "] <-- 0x" << setfill('0') << setw(2) << hex << phy_data;
                                this->msg->dbg(ss.str());
                            }

                            fsm_addr = (fsm_addr + 1) % 256;
                        }

                        fsm_wdat  = I2CT_SEQ_ACK;
                        fsm_idx   = fsm_idx + 1;
                        fsm_state = FSM_WR_BYTE;
                    }
                    else if (phy_event == I2CT_XFER)
                    {
                        break;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_WR_BYTE :
                    if (phy_event == I2CT_ACK)
                    {
                        fsm_state = FSM_WR_ACK;
                    }
                    else if (phy_event == I2CT_NAK)
                    {
                        fsm_state = FSM_IDLE;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_RD :
                    if (phy_event == I2CT_ACK)
                    {
                        fsm_rdat  = ram[fsm_addr];

                        if (false)
                        {
                            ss = stringstream();
                            ss << fsm_pair_array[fsm_state].second;
                            ss << ": ram read [0x" << setfill('0') << setw(2) << hex << fsm_addr;
                            ss << "] --> 0x" << setfill('0') << setw(2) << hex << fsm_rdat;
                            this->msg->dbg(ss.str());
                        }

                        fsm_rdat  = (fsm_rdat << 1) | 0x1;
                        fsm_wdat  = fsm_rdat;
                        fsm_addr  = (fsm_addr + 1) % 256;
                        fsm_state = FSM_RD_ACK;
                    }
                    else if (phy_event == I2CT_NAK)
                    {
                        fsm_state = FSM_IDLE;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_RD_ACK :
                    if (phy_event == I2CT_STOP)
                    {
                        fsm_state = FSM_IDLE;
                    }
                    else if (phy_event == I2CT_XFER)
                    {
                        fsm_wdat  = fsm_rdat;
                        fsm_state = FSM_RD_XFER;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_RD_XFER :
                    if (phy_event == I2CT_DSTRB)
                    {
                        fsm_state = FSM_RD;
                    }
                    else if (phy_event == I2CT_XFER)
                    {
                        fsm_wdat  = fsm_rdat;
                    }
                    else
                    {
                        ss = stringstream();
                        ss << fsm_pair_array[fsm_state].second;
                        ss << ": Unexpected event" << SP << i2c_typ_pair_array[phy_event].second;
                        ss << "; HALT";
                        this->msg->nok(ss.str());

                        fsm_state = FSM_HALT;
                    }

                    break;
                case FSM_HALT :
                    break;
                default :
                    this->msg->nok("FSM case 'default'; HALT");
                    fsm_state = FSM_HALT;
                    break;
            }

            this->phy_seq.write(fsm_wdat);

            if (DBG_SIGNALS)
            {
                this->dbg_fsm_rdat.write(fsm_rdat);
                this->dbg_fsm_idx.write(fsm_idx);
                this->dbg_fsm_addr.write(fsm_addr);
                this->dbg_fsm_state.write(fsm_state);
            }
        }
    }

    //-- Utility ---------------------------------------------------------------
    void I2C_RAM_256x8::clr_ram_data(void)
    {
        ram.clear();
    }

    void I2C_RAM_256x8::set_ram_data(const uint32_t &arg_addr, const uint32_t &arg_data)
    {
        ram[arg_addr % 256] = arg_data;
    }

    void I2C_RAM_256x8::get_ram_data(const uint32_t &arg_addr, uint32_t &arg_data)
    {
        arg_data = ram[arg_addr % 256];
    }

    uint32_t I2C_RAM_256x8::get_ram_data(const uint32_t &arg_addr)
    {
        return ram[arg_addr % 256];
    }

    void I2C_RAM_256x8::create_gtkw_fsm_filter(void)
    {
        string   fnam = Base::get_plusarg("WRK")+FS+GTKW_FSM_FILTER_FNAM;
        ofstream ofs;

        ofs.open(fnam);

        ofs << "#" << endl;
        ofs << "# tb::I2C_RAM_256x8::fsm_state" << endl;
        ofs << "#" << endl;

        for (uint32_t i = 0 ; i < FSM_SIZE ; i++)
        {
            ofs << setfill('0') << setw(8) << hex << fsm_pair_array[i].first;
            ofs << SP;
            ofs << fsm_pair_array[i].second;
            ofs << endl;
        }

        ofs.close();
        this->msg->inf("Created GTKW filter file"+SP+fnam);
    }
}

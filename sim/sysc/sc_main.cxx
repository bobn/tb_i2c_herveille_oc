
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb.h>

int sc_main(int argc, char **argv)
{
    Verilated::commandArgs(argc, argv);
    Verilated::debug(0);
    Verilated::traceEverOn(true);

    std::unique_ptr<tb::tb> tb = (std::unique_ptr<tb::tb>)(new tb::tb("tb"));

    try
    {
        tb->await_sysc_elab();
        tb->vltr_tracing();
        tb->simulate();
        tb->cleanup();
    }
    catch(std::exception& ex)
    {
        cerr << "[ERR] sc_main() caught an exception: " << ex.what() << endl << flush;
    }

    return 0;
}

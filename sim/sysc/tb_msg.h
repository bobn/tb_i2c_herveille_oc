
// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _TBMSG_H_
    #define _TBMSG_H_

    #include <memory>
    #include <deque>
    #include <string>

    namespace tb
    {
        namespace Chars
        {
            const std::string SP = "\x20";
            const std::string CM = ",";
            const std::string PT = ".";
            const std::string CN = ":";
            const std::string FS = "/";
            const std::string NL = "\n";
            const std::string SQ = "\'";
            const std::string DQ = "\"";
            const std::string TB = "\t";
        }

        class Msg
        {
            private:
                static uint32_t nok_count;

            protected:
                std::deque<std::string> tags;
                std::string             context;
                std::string             fmt;
                sc_core::sc_time        time_base;
                sc_core::sc_time        time_needed;
                uint32_t                time_prec;
                double                  scale;

            public:
                Msg(const std::string &arg_tag);
                ~Msg(void);

                std::string & get_context(void);
                void          pop_tag(void);
                void          push_tag(const std::string& arg_str);

                void msg(const std::string& arg_str, const std::string& arg_lvl);
                void dbg(const std::string& arg_str);
                void inf(const std::string& arg_str);
                void wrn(const std::string& arg_str);
                void err(const std::string& arg_str);
                void aok(const std::string& arg_str);
                void nok(const std::string& arg_str);

                void report(const std::string &arg_str);

                std::string get_shift_string(const unsigned arg_level);

                static std::string get_hex_string(const uint64_t arg_uint, const unsigned arg_width, const bool arg_fill);
                static std::string get_hex_string(const uint64_t arg_uint);
        };

        typedef std::unique_ptr<Msg> pMsg;
    }
#endif


// Copyright 2023 Robert Newgard
// 
// This file is part of tb_i2c_herveille_oc.
// 
// tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// tb_i2c_herveille_oc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#include <tb_base.h>

namespace tb
{
    using namespace std;
    using namespace Chars;

    bool            run_req_end = false;
    sc_trace_file * trace_sysc  = nullptr;

    //-- Constructor/Destructor ------------------------------------------------
    Base::Base()
    {
        this->msg  = nullptr;
        this->tfp  = new VerilatedVcdSc;
    }
    Base::~Base(void){}

    //-- PlusArg Get -----------------------------------------------------------
    void Base::get_plusarg(const string & arg_key, string & ref_val)
    {
        size_t pos;
        string tmp;

        tmp = string(Verilated::commandArgsPlusMatch(arg_key.c_str()));
        pos = tmp.find_last_of('=') + 1;

        tmp.replace(0, pos, "");

        ref_val = tmp;
    }

    string Base::get_plusarg(const string & arg_key)
    {
        size_t pos;
        string tmp;

        tmp = string(Verilated::commandArgsPlusMatch(arg_key.c_str()));
        pos = tmp.find_last_of('=') + 1;

        tmp.replace(0, pos, "");
        return tmp;
    }

    double Base::get_plusarg_double(const string & arg_key)
    {
        size_t cnt;
        size_t pos;
        size_t siz;
        string str;
        double dbl;

        str = string(Verilated::commandArgsPlusMatch(arg_key.c_str()));
        pos = str.find_last_of('=') + 1;

        str.replace(0, pos, "");
        siz = str.size();

        dbl = stod(str, &cnt);

        if (cnt < siz)
        {
            cerr << "Failed to convert plusarg"+SP+DQ+arg_key+DQ;
            cerr << SP+"with string value"+SP+DQ+str+DQ;
            cerr << SP+"to double.  Stop.";

            run_req_end = true;

            wait();
        }

        return dbl;
    }

    int Base::get_plusarg_int(const string & arg_key)
    {
        size_t cnt;
        size_t pos;
        size_t siz;
        string str;
        int    ret;

        str = string(Verilated::commandArgsPlusMatch(arg_key.c_str()));
        pos = str.find_last_of('=') + 1;

        str.replace(0, pos, "");
        siz = str.size();

        ret = stoi(str, &cnt, 0);

        if (cnt < siz)
        {
            cerr << "Failed to convert plusarg"+SP+DQ+arg_key+DQ;
            cerr << SP+"with string value"+SP+DQ+str+DQ;
            cerr << SP+"to int.  Stop.";

            run_req_end = true;

            wait();
        }

        return ret;
    }


    //-- Tracing ---------------------------------------------------------------
    void Base::vltr_tracing_open(void)
    {
        string work;
        string fnam;
        string text;

        get_plusarg("WRK", work);
        get_plusarg("VCD_BASENAME_VLTR", fnam);

        if (work.empty())
        {
            fnam = "trace_vltr.vcd";
            text = "Opened Verilator trace file"+SP+DQ+fnam+DQ+PT;
        }
        else
        {
            fnam = work+FS+fnam+".vcd";
            text = "Opened Verilator trace file"+SP+DQ+fnam+DQ+PT;
        }

        this->tfp->open(fnam.c_str());
        this->msg->inf(text);
        return;
    }

    void Base::vltr_tracing_close(void)
    {
        this->msg->inf("Closing Verilator trace...");
        this->tfp->close();
        this->tfp = nullptr;
        this->msg->inf("Verilator trace closed.");
    }

    void Base::sysc_tracing_open(void)
    {
        string work;
        string fnam;

        get_plusarg("WRK", work);
        get_plusarg("VCD_BASENAME_SYSC", fnam);

        if (work.empty())
        {
            fnam = "trace_sysc";
        }
        else
        {
            fnam = work+FS+fnam;
        }

        trace_sysc = sc_create_vcd_trace_file(fnam.c_str());

        this->msg->inf("Opened SystemC trace file"+SP+DQ+fnam+".vcd"+DQ+PT);
        sc_report_handler::set_actions(SC_INFO, SC_DO_NOTHING);
        trace_sysc->set_time_unit(1, SC_PS);
        sc_report_handler::set_actions(SC_INFO, SC_DEFAULT_INFO_ACTIONS);
        return;
    }

    //-- Simulate --------------------------------------------------
    void Base::await_sysc_elab(void)
    {
        sc_start(SC_ZERO_TIME);
    }

    void Base::simulate(void)
    {
        this->msg->inf("Simulating...");

        while (!Verilated::gotFinish())
        {
            sc_start(1, SC_NS); 

            if (run_req_end)
            {
                this->msg->inf("Got a stop request");
                break;
            }
        }

        this->msg->inf("Stopped.");
    }
}

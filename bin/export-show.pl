#!/usr/bin/env perl
#%# Reports Makefile exports

# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

use Modern::Perl q(2019);
no  warnings     qw(experimental::smartmatch portable);
use English      qw( -no_match_vars ) ;

use integer;

use constant NL => qq{\n};
use constant SP => q{ };
use constant PD => q{.};
use constant CM => q{,};
use constant SQ => q{'};
use constant DQ => q{"};
use constant EQ => q{=};

use constant MSG_USAGE => q(
    usage: %s shell-variable...
);

my $stdout = *STDOUT;
my $stderr = *STDERR;
my ($PROG) = $0 =~ m#(?:.*/)?([^/]*)#;

exit main(@ARGV);

sub const_str
{
    my $fmt = $ARG[0];

    for ($fmt)
    {
        s/^\n//g;
        s/^\s{4}//g;
        s/\n\s{4}/\n/g;
    }

    return($fmt);
}

sub usage
{
    $stderr->printf(const_str(MSG_USAGE).NL, $PROG);
    exit(1);
}

sub main
{
    if (scalar(@ARG) < 1)
    {
        usage();
    }

    my @vars  = ();
    my @decls = qx(bash -c 'export -p');
    my $width = 0;

    for (@ARG)
    {
        push(@vars, $ARG);

        if ($width < length($ARG))
        {
            $width = length($ARG);
        }
    }

    for (@vars)
    {
        my $var = $ARG;

        for (@decls)
        {
            my $decl = $ARG;

            if ($decl =~ m|^\s*declare\s*-x\s*([^=]+)=(.*)$|)
            {
                my $decl_var = $1;
                my $decl_val = $2;


                if ($var eq $decl_var)
                {
                    if ($decl_val =~ m|:|)
                    {
                        $stdout->printf("%s = ".DQ, $var);
                        $decl_val =~ s|"||g;
                        
                        for (split(":", $decl_val))
                        {
                            $stdout->printf(NL.(SP x 4)."%s", $ARG);
                        }

                        $stdout->printf(NL.DQ.NL);
                    }
                    else
                    {
                        $stdout->printf("%-".($width + 1)."s = %s".NL, $var, $decl_val);
                    }
                }

            }
        }
    }

    exit(0);
}


# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

#
# 1. Required Makefile Variables
#
#         TMP
#         CFG
#         MAKE
#         CWD
#
# 2. Requires script git-repo.bash
#
# 3. Configure REPO
#
#     Each repo REPO has a configuration subdirectory
#     under cfg/Git.  The subdirectory contains 3 files.
#
#        cfg
#         └─ cfg/Git
#            └── cfg/Git/$REPO
#                ├── cfg/Git/$REPO/build_targets
#                ├── cfg/Git/$REPO/remote_branch
#                └── cfg/Git/$REPO/remote_url
#
#     The following subsections describe the files.
#     
# 3.1. build_targets
# 
#     A list of targets that need to be built within
#     for the repo to be used in the local project.
#     When this file is missing or empty, no targets
#     will be built.
#
# 3.2. remote_branch
# 
#     Branch to pull $REPO from.
#
# 3.2. remote_url
# 
#     URL to pull $REPO from.
#
#     
# 4. Hint show-cfg-git
#
#        $(eval $(call hints_targ , show-cfg-git , Show git repo config - aka 'scg' ))
#
# 5. Target show-cfg-git
#     
#        show-cfg-git : $(NULL)      ; @ $(CFG_SHOW_EXE) $(CFG)/Git
#        scg          : show-cfg-git ; $(NULL)
#
# 6. Hint show-git
#
#        $(eval $(call hints_targ , show-git , Show git status - aka 'sg' ))
#
# 7. Target show-git
#
#        show-git : $(NULL)  ; $(show_git_recipe)
#        sg       : show-git ; $(NULL)
#
# 8. Hints for clones
#
#        $(eval $(call hints_targ , clone-hints  , Hints for cloning individual auxiliary Git repos ))
#        $(eval $(call hints_targ , clones       , Clone all auxiliary Git repos                    ))
#        $(eval $(call hints_targ , clones-clean , Eliminate all cloned repos                       ))
#
# 9. Targets for clones
#
#        clone-hints  : $(GIT_HINTS)  ; $(NULL)
#        clones       : $(GIT_TARGS)  ; $(NULL)
#        clones-clean : $(GIT_CLEANS) ; $(NULL)
#
# 10. PHONYs
#     
#     scg, sg
#
#

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(INCLUDED_HINTS),)
    include bin/include-hints.mk
endif

ifeq ($(INCLUDED_SEPARATOR),)
    include bin/include-separator.mk
endif

ifeq ($(INCLUDED_CONFIG),)
    include bin/include-config.mk
endif

ifeq ($(GIT),)
    GIT := $(call exe_chk, git)
endif

ifeq ($(GIT_REPO_UTIL),)
    GIT_REPO_UTIL := $(call exe_chk, git-repo.bash)
endif

ifeq ($(ENV_TF),)
    ENV_TF := %-24s
endif

ifeq ($(CFG_GIT),)
    CFG_GIT := $(CFG)/Git
endif

ifeq ($(PPR4),)
    PPR4 := pr -t -o 4
endif

GIT_REPOS     := $(if $(wildcard $(CFG_GIT)),$(shell ls $(CFG_GIT)),$(NULL))
GIT_HINTS     := $(if $(wildcard $(CFG_GIT)),hints-hdr,$(NULL))
GIT_ENV       := $(NULL)
GIT_TARGS     := $(NULL)
GIT_BUILDS    := $(NULL)
GIT_CLEANS    := $(NULL)

# get_clone_path <name>
define get_clone_path
    $(TMP)/$(strip $(1))
endef

# get_clone_targ <name>
define get_clone_targ
    $(TMP)/Cloned_$(strip $(1))
endef

define show_git_env_bash
    $(if $(strip $(GIT_ENV)),echo;)
    $(if $(strip $(GIT_ENV)),printf "$(ENV_TF) %s\n" "Cloned repo(s)" "$(NULL)";)
    $(if $(strip $(GIT_ENV)),printf "$(ENV_TF) %s\n" "--------------" "$(NULL)";)
    $(if $(strip $(GIT_ENV)),$(GIT_ENV))
endef

# show_git_def <dir>
define show_git_def
    @ test ! -d "$(1)" || echo "Repo at $(1)/ ($$(cd $(1) && $(GIT) log -1 --pretty=format:%h))"
    @ test ! -d "$(1)" || (cd $(1) && $(GIT) -c 'color.ui=always' status -sb | $(PPR4))
endef

define show_git_recipe
    $(call show_git_def,.)
    $(foreach NAME,$(GIT_REPOS),$(call show_git_def,$(TMP)/$(NAME))$(MAKE_NL))
endef

# git_clone_chk_bash <name>
define git_clone_chk_bash
    if [ ! -d $(TMP)/$(1) ] ; then
        echo "[ERR] No clone of $(1) present" ;
        exit 1 ;
    fi ;
endef

# git_clean_force_bash <name>
define git_clean_force_bash
    rm -rf $(TMP)/$(1) ;
    rm -rf $(TMP)/Cloned_$(1) ;
    rm -rf $(TMP)/Ready_$(1) ;
endef

# git_clean_bash <name>
define git_clean_bash
    $(call git_clone_chk_bash,$(1))
    if (cd $(TMP)/$(1) && git diff-index --quiet HEAD) ; then
        echo "[INF] No changes pending in $(1) clone..." ;
        $(call git_clean_force_bash,$(1))
        echo "[INF] Eliminated tmp/$(1)" ;
    else
        echo "[ERR] Changes pending in clone of $(1).  Stop." ;
        exit 1 ;
    fi ;
endef

# git_reset_hard_bash <name> <commit>
define git_reset_hard_bash
    if [ "$(2)" != "head" ] ; then
	cd $(TMP)/$(1) ;
        $(GIT) reset --hard $(2) ;
    fi ;
endef

# git_build_recipe <name> <targets>
define git_build_recipe
    @ $(strip $(call print_separator_bash,begin git_build_recipe for $(1)))
    cd $(TMP)/$(1) && $(MAKE) $(2) && touch $(CWD)/$(TMP)/Ready_$(1)
    @ $(strip $(call print_separator_bash,end git_build_recipe for $(1)))
endef

# git_clone_recipe <name> <url> <branch> <commit>
define git_clone_recipe
    @ $(strip $(call print_separator_bash,begin git_clone_recipe for $(1)))
    @ echo "[INF] Cloning $(1)..."
    @ printf "%-7s %s\n" "branch:" "$(3)"        | $(PPR4)
    @ printf "%-7s %s\n" "commit:" "$(4)"        | $(PPR4)
    @ printf "%-7s %s\n" "from:"   "$(2)"        | $(PPR4)
    @ printf "%-7s %s\n" "to:"     "$(TMP)/$(1)" | $(PPR4)
    rm -rf $(TMP)/$(1)
    $(GIT) clone --branch $(3) $(2) $(TMP)/$(1)
    $(if $(findstring head,$(4)),$(NULL),$(strip $(call git_reset_hard_bash,$(1),$(4))))
    touch $(TMP)/Cloned_$(1)
    @ echo "[INF] Done."
    @ $(strip $(call print_separator_bash,end git_clone_recipe for $(1)))
endef

# git_repo_vars <name>
define git_repo_vars
    GIT_U_$(1) := $(call get_str,  $(CFG_GIT)/$(1)/remote_url)
    GIT_B_$(1) := $(call get_str,  $(CFG_GIT)/$(1)/remote_branch)
    GIT_C_$(1) := $(call get_str,  $(CFG_GIT)/$(1)/remote_commit)
    GIT_C_$(1)     += $(if $(GIT_C_$(1)),$(NULL),head)
    GIT_T_$(1) := $(call get_list, $(CFG_GIT)/$(1)/build_targets)


    GIT_ENV += printf "%-8s %s\n" "$(1)"        | pr -t -o 4 ;
    GIT_ENV += printf "%-8s %s\n" "URL "    "$(shell $(GIT_REPO_UTIL) $(TMP)/$(1) url)"    | pr -t -o 8 ;
    GIT_ENV += printf "%-8s %s\n" "Branch " "$(shell $(GIT_REPO_UTIL) $(TMP)/$(1) branch)" | pr -t -o 8 ;
endef

# git_repo_hints <name>
define git_repo_hints
   $(1)-clone-hint       : $(NULL) ; @ $(call hints_def , $(1)-clone       , Clone $(1) repo to $(TMP)/$(1)           )
   $(1)-clean-hint       : $(NULL) ; @ $(call hints_def , $(1)-clean       , Remove $(TMP)/$(1) if no changes pending )
   $(1)-clean-force-hint : $(NULL) ; @ $(call hints_def , $(1)-clean-force , Remove $(TMP)/$(1)                       )

    GIT_HINTS += $(1)-clone-hint
    GIT_HINTS += $(1)-clean-hint
    GIT_HINTS += $(1)-clean-force-hint
    GIT_HINTS += $(if $(GIT_T_$(1)),$(1)-build-hint)
    PHONYS    += $(1)-clone-hint
    PHONYS    += $(1)-clean-hint
    PHONYS    += $(1)-clean-force-hint
    PHONYS    += $(if $(GIT_T_$(1)),$(1)-build-hint)
endef

define git_hint_rules
    $(if $(GIT_REPOS),$(eval clone-hints  : $(GIT_HINTS)  ))
    $(if $(GIT_REPOS),$(eval clones       : $(GIT_TARGS)  ))
    $(if $(GIT_REPOS),$(eval clones-clean : $(GIT_CLEANS) ))
endef

define git_hint_hints
    $(if $(GIT_REPOS),$(eval $(call hints_targ , clone-hints  , Hints for cloning individual auxiliary Git repos )))
    $(if $(GIT_REPOS),$(eval $(call hints_targ , clones       , Clone all auxiliary Git repos                    )))
    $(if $(GIT_REPOS),$(eval $(call hints_targ , clones-clean , Eliminate all cloned repos                       )))
endef

# git_repo_build_rules <name> <targets>
define git_repo_build_rules
    $(TMP)/Ready_$(1) : $(TMP)/Cloned_$(1) ; $$(call git_build_recipe,$(1),$(2))
    $(1)-build        : $(TMP)/Ready_$(1)  ; $(NULL)
    $(1)-build-hint   : $(NULL)            ; @ $(call hints_def , $(1)-build , Build in repo $(TMP)/$(1) )

    GIT_TARGS += $(TMP)/Ready_$(1)
    GIT_HINTS += $(1)-build-hint
    PHONYS    += $(1)-build
    PHONYS    += $(1)-build-hint
endef

# git_repo_clone_rules <name>
define git_repo_clone_rules
    $(TMP)/Cloned_$(1) : $(CFG_GIT)/$(1)/*  ; $$(call git_clone_recipe,$(1),$(GIT_U_$(1)),$(GIT_B_$(1)),$(GIT_C_$(1)))
    $(1)-clone         : $(TMP)/Cloned_$(1) ; $(NULL)
    $(1)-clean         : $(NULL)            ; @ $(strip $(call git_clean_bash,$(1)))
    $(1)-clean-force   : $(NULL)            ; @ $(strip $(call git_clean_force_bash,$(1)))

    $(if $(GIT_T_$(1)),$(call git_repo_build_rules,$(1),$(GIT_T_$(1))))

    GIT_TARGS  += $(TMP)/Cloned_$(1)
    GIT_CLEANS += $(1)-clean
    PHONYS     += $(1)-clone
    PHONYS     += $(1)-clean
    PHONYS     += $(1)-clean-force
endef

$(foreach NAM,$(GIT_REPOS),$(eval $(call git_repo_vars,$(NAM))))
$(foreach NAM,$(GIT_REPOS),$(eval $(call git_repo_hints,$(NAM))))
$(foreach NAM,$(GIT_REPOS),$(eval $(call git_repo_clone_rules,$(NAM))))

INCLUDED_GIT := $(TRUE)

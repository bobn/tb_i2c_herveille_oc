
# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(PERL),)
    PERL := $(call exe_chk, perl)
endif


# perl_left_shift <file>
# perl_replace <line-match> <from> <to> <file>
# perl_append <line-match> <append-line> <file>
# perl_prepend <line-match> <prepend-line> <file>
# perl_remove <line-match> <file>

define perl_left_shift
    $(PERL) -i -sane 's|^\s{4}||;print $$_' -- $(strip $(1))
endef

define perl_replace
    $(PERL) -i -sane 'if(m|$(strip $(1))|){s|$(strip $(2))|$(strip $(3))|g};print($$_)' -- $(strip $(4))
endef

define perl_append
    $(PERL) -i -sane 'print($$_);if(m|$(strip $(1))|){print("$(strip $(2))"."\n")}' -- $(strip $(3))
endef

define perl_prepend
    $(PERL) -i -sane 'if(m|$(strip $(1))|){print("$(strip $(2))"."\n")};print($$_)' -- $(strip $(3))
endef

define perl_remove
    $(PERL) -i -sane 'if(m|$(strip $(1))|){next}else{print($$_)}' -- $(strip $(2))
endef

INCLUDED_SED := $(TRUE)

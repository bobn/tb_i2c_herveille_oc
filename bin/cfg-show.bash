#!/bin/bash
#%# show config

# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

MAGIC=theeZaelu8
MFILS=$(find . -name magic)
SHOW_CFG=""

for FIL in $MFILS ; do
    if [ ! -r $FIL ] ; then
        continue
    fi
    if [ "$(cat $FIL)" = "$MAGIC" ] ; then
        SHOW_CFG=$(basename $(dirname $FIL))
        break
    fi
done

if [ -z "$SHOW_CFG" ] ; then
    echo "ERR: no good config dir found"
    exit 1
fi

if [ ! -d "$SHOW_CFG" ] ; then
    echo "ERR: bad config dir found \"$SHOW_CFG\""
    exit 1
fi

# echo "INF: config dir found \"$SHOW_CFG\""

START=${1:-${SHOW_CFG}/}
LEN=$(echo ${SHOW_CFG} | wc -c)
OFF='\033[0m'
ORN='\033[0;33m'
CYN='\033[0;36m'

if [ "${START:0:${LEN}}" != "${SHOW_CFG}/" ] ; then
    echo "[ERR] bad start path: \"${START}\""
    exit 1
fi

if [ -z "$START" ] ; then
    echo "ERR: no good config dir found"
    exit 1
fi

for OBJ in $(find $START -not -path '*/\.*' | sort) ; do
    WC=$(echo $OBJ | sed 's|/| |g' | wc -w)
    IN=$((WC + 1))
    IN=$((IN * 4))
    while ((WC > 0)) ; do
        printf "    "
        WC=$((WC - 1))
    done
    if [ -d "$OBJ" ] ; then
        printf "${ORN}%s${OFF}\n" $OBJ
    elif [ -f "$OBJ" ] ; then
        printf "${CYN}%s${OFF}\n" $OBJ
        pr -o $IN -t $OBJ
    fi
done

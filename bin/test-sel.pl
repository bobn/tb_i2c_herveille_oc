#!/usr/bin/env perl
#%# rewrites xilinx funcsim models

# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

use Modern::Perl q(2019);
no  warnings     qw(experimental::smartmatch portable);
use English      qw( -no_match_vars ) ;

use integer;

use constant NL => qq{\n};
use constant SP => q{ };
use constant PD => q{.};
use constant CM => q{,};
use constant SQ => q{'};

use constant MSG_USAGE => q(
    usage: %s src-file
);

use constant TEST_PRELOAD => [
    "cfg-show.bash",
    "git-repo.bash",
    "include-base.mk",
    "include-config.mk",
    "include-eval.mk",
    "include-git.mk",
    "include-gtkwave.mk",
    "include-hints.mk",
    "include-sed.mk",
    "include-separator.mk",
    "include-systemc.mk",
    "include-verilator.mk",
    "include-xilinx.mk",
    "include-yosys.mk",
    "lec-prims.pl",
    "make-env",
    "test-sel.pl",
];

my $stdout = *STDOUT;
my $stderr = *STDERR;
my ($PROG) = $0 =~ m#(?:.*/)?([^/]*)#;

exit main(@ARGV);

sub const_str
{
    my $fmt = $ARG[0];

    for ($fmt)
    {
        s/^\n//g;
        s/^\s{4}//g;
        s/\n\s{4}/\n/g;
    }

    return($fmt);
}

sub usage
{
    $stderr->printf(const_str(MSG_USAGE).NL, $PROG);
    exit(1);
}

sub get_tests
{
    my $fnam  = $ARG[0];
    my $aref  = $ARG[1];
    my $ifh   = IO::File->new($fnam, "r") or die("cannot open $fnam for reading, $ERRNO");

    while (defined(my $line = $ifh->getline()))
    {
        if ($line !~ m|TEST_SEL_ITEM|) { next; }
        if ($line =~ m|\(\s*sel\s*==\s*"([^"]+)"\s*\)|)
        {
            push(@{$aref}, $1);
        }
    }

    $ifh->close();
}

sub main
{
    if (scalar(@ARG) != 1)
    {
        usage();
    }

    my $fnam  = $ARG[0];
    my @tests = ();
    my @col1  = ();
    my @col2  = ();
    my @col3  = ();
    my $width = 0;
    my $count = 0;
    my $fmt   = "";
    my $rows  = 0;
    my $items = 0;
    my $col   = 0;

    if (0)
    {
        for (@{(TEST_PRELOAD)})
        {
            push(@tests, $ARG);
        }
    }

    get_tests($fnam, \@tests);

    for (@tests)
    {
        $count++;

        if ($width < length($ARG))
        {
            $width = length($ARG);
        }

        if (0)
        {
            $stderr->print($ARG.NL);
        }
    }

    $fmt = "%-".($width + 1)."s";
    $fmt = $fmt.SP.$fmt.SP.$fmt.NL;
    
    if (0)
    {
        $stderr->print("width".SP.$width.NL);
        $stderr->print("fmt".SP.$fmt);
        $stderr->print("count".SP.$count.NL);
        $stderr->print("count/3".SP.($count/3).NL);
        $stderr->print("count%3".SP.($count%3).NL);
    }

    if (($count % 3) == 0)
    {
        $rows = $count / 3;
    }
    else
    {
        $rows = ($count / 3) + 1;
    }

    $items = $rows * 3;

    if (0)
    {
        $stderr->print("rows".SP.$rows.NL);
        $stderr->print("items".SP.$items.NL);
    }

    for (my $i = 0 ; $i < $items ; $i++)
    {
        my $idx = $i % $rows;
        my $item = "";

        if ($i >= $count)
        {
            $item = "";
        }
        else
        {
            $item = $tests[$i];
        }

        if (0)
        {
            $stderr->print("row-col".SP.$idx."-".$col.SP.SQ.$item.SQ.NL);
        }

        given ($col)
        {
            when (0) { push(@col1, $item); }
            when (1) { push(@col2, $item); }
            when (2) { push(@col3, $item); }
        }

        if ($idx == ($rows - 1))
        {
            $col++;
        }
    }

    for (my $i = 0 ; $i < $rows ; $i++)
    {
        $stdout->printf($fmt, $col1[$i], $col2[$i], $col3[$i]);
    }

    exit(0);
}


# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

# Command-line evaluation support
#
# * Add this after all variables are defined and before rules
# * Usage
#     
#     make eval 'verilator -Wno-multitop --lint-only  rtl/spec_bus_pkg.v'
#--

ifeq ($(firstword $(MAKECMDGOALS)),eval)
eval : $(NULL) ; @$(filter-out eval,$(MAKECMDGOALS))
%    : $(NULL) ; @true
endif

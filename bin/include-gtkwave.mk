
# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(INCLUDED_SED),)
    include bin/include-sed.mk
endif

ifeq ($(GTKWAVE),)
    GTKWAVE := $(call exe_chk, gtkwave)
endif

ifeq ($(TWINWAVE),)
    TWINWAVE := $(call exe_chk, twinwave)
endif



# -- Integrating GTKWave -------------------------------------------------------
#
# 1. No configuration files required for GTKWave
#
#
# 2. Variables that must be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | WAV                | Root of waveforms configuration tree
#     | SIM_TB_SEL         | Component of waveform configuration path
#     | SIM_WAV_DIR        | Directory containing GTKW file
#     | SIM_VCD_VLTR       | VCD file created by Verilator simulation
#     | SIM_GTKW_VLTR      | Path to Verilator GTKW file
#     | SIM_VCD_SYSC       | VCD file created by SystemC simulation
#     | SIM_GTKW_SYSC      | Path to SystemC GTKW file
#
#
# 2. Variables that may be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | GTKW_FNAM          | Name for GTKW file
#
#
# 3. Add hints to Makefile
#     * Add the following to the '$(eval $(call hints_targ, ...' sequence
#
#         $(wave_hints)
#
#
# 4. Add rules to Makefile
#     * Add the following to the rules sequence
#
#         $(wave_rules)
#
#--



# gtkwave_bash <vcd> <gtkw>
define gtkwave_bash
    (
        $(GTKWAVE) $(1) $(2) >& /dev/null ;
        echo ;
        echo "[INF] Trimming $(2)..." ;
        $(call perl_remove  , ^\[\*\]                   , $(2) ) ;
        $(call perl_remove  , ^\[dumpfile_mtime\]       , $(2) ) ;
        $(call perl_remove  , ^\[dumpfile_size\]        , $(2) ) ;
        $(call perl_replace , $(CWD), $(CWD)/ , $(NULL) , $(2) ) ;
        echo "[INF] Done." ;
    ) &
endef

# perl_replace <line-match> <from> <to> <file>
define twinwave_bash
    (
        $(TWINWAVE) $(SIM_VCD_VLTR) $(SIM_GTKW_VLTR) + $(SIM_VCD_SYSC) $(SIM_GTKW_SYSC) >& /dev/null ;
        echo ;
        echo "[INF] Trimming $(SIM_GTKW_VLTR)..." ;
        $(call perl_remove  , ^\[\*\]                   , $(SIM_GTKW_VLTR) ) ;
        $(call perl_remove  , ^\[dumpfile_mtime\]       , $(SIM_GTKW_VLTR) ) ;
        $(call perl_remove  , ^\[dumpfile_size\]        , $(SIM_GTKW_VLTR) ) ;
        $(call perl_replace , $(CWD), $(CWD)/ , $(NULL) , $(SIM_GTKW_VLTR) ) ;
        echo "[INF] Trimming $(SIM_GTKW_SYSC)..." ;
        $(call perl_remove  , ^\[\*\]                   , $(SIM_GTKW_SYSC) ) ;
        $(call perl_remove  , ^\[dumpfile_mtime\]       , $(SIM_GTKW_SYSC) ) ;
        $(call perl_remove  , ^\[dumpfile_size\]        , $(SIM_GTKW_SYSC) ) ;
        $(call perl_replace , $(CWD), $(CWD)/ , $(NULL) , $(SIM_GTKW_SYSC) ) ;
        echo "[INF] Done." ;
    ) &
endef

# wav_recipe <vcd> <gtkw>
define wav_recipe
    mkdir -p $(SIM_WAV_DIR)
    touch $(2)
    @ echo "[INF] Start GTKWave on $(1) with $(2)..."
    @ $(strip $(call gtkwave_bash,$(1),$(2)))
    @ echo "[INF] Started GTKWave."
endef

define twav_recipe
    mkdir -p $(SIM_WAV_DIR)
    touch $(SIM_GTKW_VLTR) $(SIM_GTKW_SYSC)
    @ echo "[INF] Start TwinWave on $(SIM_VCD_VLTR) with $(SIM_GTKW_VLTR) and $(SIM_VCD_SYSC) with $(SIM_GTKW_SYSC)..."
    @ $(strip $(call twinwave_bash))
    @ echo "[INF] Started TwinWave."
endef

define wave_hints
    $(eval $(call hints_targ , vwave , View Verilated output VCD (aka wave)     ))
    $(eval $(call hints_targ , swave , View Systemc output VCD                  ))
    $(eval $(call hints_targ , twave , View Verilated and Systemc output VCDs   ))
endef

define wave_rules
    $(eval vwave : $(NULL) ; $$(call wav_recipe,$(SIM_VCD_VLTR),$(SIM_GTKW_VLTR)))
    $(eval swave : $(NULL) ; $$(call wav_recipe,$(SIM_VCD_SYSC),$(SIM_GTKW_SYSC)))
    $(eval twave : $(NULL) ; $$(call twav_recipe))
    $(eval wave  : vwave   ; $(NULL))
endef


INCLUDED_GTKWAVE := $(TRUE)

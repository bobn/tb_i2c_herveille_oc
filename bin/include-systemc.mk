
# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

ifneq ($(INCLUDED_SYSTEMC),)
    $(warning systemc included earlier)
endif

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(INCLUDED_HINTS),)
    include bin/include-hints.mk
endif

ifeq ($(INCLUDED_SEPARATOR),)
    include bin/include-separator.mk
endif

ifeq ($(INCLUDED_CONFIG),)
    include bin/include-config.mk
endif

ifeq ($(INCLUDED_VERILATOR),)
    include bin/include-verilator.mk
endif

ifeq ($(SYSC_CFG),)
    SYSC_CFG := $(CFG)/Install/SystemC
endif

ifeq ($(SYSC_GXX),)
    SYSC_GXX := $(call exe_chk, g++)
endif

ifeq ($(SYSC_CFLAGS),)
    SYSC_CFLAGS := $(NULL)
endif

ifeq ($(SYSC_DFLAGS),)
    SYSC_DFLAGS := $(NULL)
endif


# -- Integrating SystemC -------------------------------------------------------
#
# 1. Configuration files that must exist (empty okay)
#
#     | File                                   | Description
#     |----------------------------------------|--------------------------------
#     | cfg/Testbench/$BENCH_NAME/lib_hdr      | Library header files, list of X.h file names
#     | cfg/Testbench/$BENCH_NAME/lib_obj      | Library object files, list of X.a file names
#     | cfg/Testbench/$BENCH_NAME/link_libs    | Linker -l components, libX format, converted to -lX in make
#     | cfg/Testbench/$BENCH_NAME/tb_csrc      | Testbench C++ code
#     | cfg/Testbench/$BENCH_NAME/tb_hsrc      | Testbench C++ headers
#
#
# 2. Configuration files that may exist
#
#     | File                                           | Description
#     |------------------------------------------------|--------------------------------
#     | cfg/Testbench/$BENCH_NAME/Defines/PlusArgs/*   | File names used as plusarg keys, containing value strings
#     | cfg/Testbench/$BENCH_NAME/Defines/Parameters/* | File names used as plusarg and parameter keys, containing value strings
#
#     Notes
#         1. PlusArgs and Parameters are passed into the SystemC testbench using
#            "+<name>=<value>" arguments; these values are accessed in SystemC
#            by calling the Verilated::commandArgsPlusMatch() function
#
#
# 3. Variables that must be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | SIM_CFG_DIR        | 
#     | SIM_WRK_DIR        | 
#     | SIM_TEST_CFG       | 
#     | SIM_TEST_SEL       | 
#
#
# 4. Variables that must be provided by include-verilator.mk
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | VLTR_INC           | 
#     | VLTR_INC_STD       |
#     | VLTR_OBJS          | 
#     | VLTR_TARG          | 
#     | VLTR_COV_REQ       |
#
#
# 5. Variables that may be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | SYSC_CFG           | 
#     | SYSC_GXX           | 
#     | SYSC_CFLAGS        | 
#     | SYSC_DFLAGS        | 
#
#
# 6. Add hints to Makefile
#     * Add the following to the '$(eval $(call hints_targ, ...' sequence
#
#         $(sysc_build_hints)
#         $(sysc_run_hints)
#
#
# 7. Add rules to Makefile
#     * Add the following to the rules sequence
#
#         $(sysc_build_rules)
#         $(sysc_run_rules)
#
#--

SYSC_DBG := $(FALSE)

SYSC_VERS_CFG     := $(SYSC_CFG)/version
SYSC_VERS_DFLT    := 2.3.3
SYSC_VERS_SEL     := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SYSC_VERS_CFG),$(SYSC_VERS_DFLT))))
SYSC_HOME_CFG     := $(SYSC_CFG)/home
SYSC_HOME_DFLT    := /opt/systemc
SYSC_HOME_SEL     := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SYSC_HOME_CFG),$(SYSC_HOME_DFLT))))
SYSC_INC_DIR      := $(SYSC_HOME_SEL)/$(SYSC_VERS_SEL)/include
SYSC_LIB_DIR      := $(SYSC_HOME_SEL)/$(SYSC_VERS_SEL)/lib-linux64
SYSC_LIB_ARCHIVE  := $(SYSC_LIB_DIR)/libsystemc.a

SYSC_PARM_DIR    := $(SIM_CFG_DIR)/Defines/Parameters
SYSC_PARM_CFG    := $(wildcard $(VLTR_PARM_DIR)/*)
SYSC_PARM_LST    := $(notdir $(VLTR_PARM_CFG))
SYSC_PARG_DIR    := $(SIM_CFG_DIR)/Defines/PlusArgs
SYSC_PARG_CFG    := $(wildcard $(SYSC_PARG_DIR)/*)
SYSC_PARG_LST    := $(notdir $(SYSC_PARG_CFG))


SYSC_CFG_CSRC_FIL := $(SIM_CFG_DIR)/tb_csrc
SYSC_CFG_CSRC_LST := $(call get_list, $(SYSC_CFG_CSRC_FIL))
SYSC_CFG_HSRC_FIL := $(SIM_CFG_DIR)/tb_hsrc
SYSC_CFG_HSRC_LST := $(call get_list, $(SYSC_CFG_HSRC_FIL))
SYSC_CFG_HLIB_FIL := $(SIM_CFG_DIR)/lib_hdr
SYSC_CFG_HLIB_LST := $(call get_list, $(SYSC_CFG_HLIB_FIL))
SYSC_CFG_ALIB_FIL := $(SIM_CFG_DIR)/lib_obj
SYSC_CFG_ALIB_LST := $(call get_list, $(SYSC_CFG_ALIB_FIL))
SYSC_CFG_LLIB_FIL := $(SIM_CFG_DIR)/link_libs
SYSC_CFG_LLIB_LST := $(call get_list, $(SYSC_CFG_LLIB_FIL))
                  
SYSC_BLD_EXE      := $(SIM_WRK_DIR)/exe

SYSC_BLD_IDIR     := $(foreach DIR,$(sort $(dir $(SYSC_CFG_HSRC_LST) $(SYSC_CFG_HLIB_LST))),-I$(DIR))
SYSC_BLD_IDIR         += -I$(SIM_WRK_DIR)
SYSC_BLD_IDIR         += -I$(VLTR_INC)
SYSC_BLD_IDIR         += -I$(VLTR_INC_STD)
SYSC_BLD_IDIR         += -I$(SYSC_INC_DIR)
                  
SYSC_BLD_OBJS     := $(foreach FIL,$(SYSC_CFG_CSRC_LST),$(SIM_WRK_DIR)/$(basename $(notdir $(FIL))).o)
SYSC_BLD_OBJS         += $(patsubst %,$(SIM_WRK_DIR)/%,$(VLTR_OBJS))
SYSC_BLD_OBJS         += $(VLTR_TARG)
SYSC_BLD_OBJS         += $(SYSC_CFG_ALIB_LST)
SYSC_BLD_OBJS         += $(SYSC_LIB_ARCHIVE)
                  
SYSC_BLD_LOPT     := $(foreach LIB,$(SYSC_CFG_LLIB_LST),$(patsubst lib%,-l%,$(LIB)))
SYSC_BLD_LOPT         += -pthread
SYSC_BLD_LOPT         += -static
                  
SYSC_BLD_REQS     := $(VLTR_TARG)
SYSC_BLD_REQS         += $(SYSC_CFG_CSRC_FIL) $(SYSC_CFG_CSRC_LST)
SYSC_BLD_REQS         += $(SYSC_CFG_HSRC_FIL) $(SYSC_CFG_HSRC_LST)
SYSC_BLD_REQS         += $(SYSC_CFG_HLIB_FIL) $(SYSC_CFG_HLIB_LST)
SYSC_BLD_REQS         += $(SYSC_CFG_ALIB_FIL)
SYSC_BLD_REQS         += $(SYSC_CFG_LLIB_FIL)
SYSC_BLD_TARG     := $(SYSC_BLD_EXE)
                  
SYSC_PLUSARG_COV  := $(if $(VLTR_COV_REQ),1,0)

SYSC_RUN_ARGS     := +SEL=$(SIM_TEST_SEL) +WRK=$(SIM_WRK_DIR) +COV=$(SYSC_PLUSARG_COV)
SYSC_RUN_ARGS         += $(foreach PARM,$(SYSC_PARM_LST),+$(PARM)=$(call get_str,$(SYSC_PARM_DIR)/$(PARM)))
SYSC_RUN_ARGS         += $(foreach PARG,$(SYSC_PARG_LST),+$(PARG)=$(call get_str,$(SYSC_PARG_DIR)/$(PARG)))
SYSC_RUN_REQS     := $(SYSC_BLD_EXE) $(SIM_TEST_CFG) $(SYSC_PARG_CFG)
SYSC_RUN_TARG     := $(SIM_WRK_DIR)/trace_sysc.vcd


#-- Build ----------------------------------------------------------------------
define sysc_cxx_debug_recipe
    @ echo "[INF] GXX is \"$$(type -p $(SYSC_GXX))\""
    @ echo "[INF] CFLAGS are \"$(SYSC_CFLAGS)\""
    @ echo "[INF] DFLAGS are \"$(SYSC_DFLAGS)\""
endef

# build_cxx_obj_recipe <csrc>
define build_cxx_obj_recipe
    @ echo $(SYSC_GXX) -c $(SYSC_CFLAGS) $(SYSC_DFLAGS) -o $(SIM_WRK_DIR)/$(basename $(notdir $(1))).o
    @ $(SYSC_GXX) -c $(SYSC_CFLAGS) $(SYSC_DFLAGS) -o $(SIM_WRK_DIR)/$(basename $(notdir $(1))).o $(SYSC_BLD_IDIR) $(1)
endef

define sysc_build_cxx_all_recipe
    @ echo "[INF] Compiling testbench C++ source..."
    $(foreach FIL,$(SYSC_CFG_CSRC_LST),$(call build_cxx_obj_recipe,$(FIL))$(MAKE_NL))
    @ echo "[INF] Compile done."
endef

define sysc_build_link_recipe
    @ echo "[INF] Link objects ..."
    $(SYSC_GXX) -o $(SYSC_BLD_EXE) $(SYSC_BLD_OBJS) $(SYSC_BLD_LOPT)
    @ echo "[INF] Link done."
endef

define sysc_build_recipe
    @ $(strip $(call print_separator_bash,begin sysc_build_recipe))
    $(if $(SYSC_DBG),$(sysc_cxx_debug_recipe))
    $(sysc_build_cxx_all_recipe)
    $(sysc_build_link_recipe)
    @ $(strip $(call print_separator_bash,end sysc_build_recipe))
endef

define sysc_build_hints
    $(eval $(call hints_targ , build       , Build testbench executable ))
    $(eval $(call hints_targ , build-clean , Build testbench executable ))
endef

define sysc_build_rules
    $(eval $(SYSC_BLD_TARG) : $(SYSC_BLD_REQS) ; $$(sysc_build_recipe)  )
    $(eval build            : $(SYSC_BLD_TARG) ; $(NULL)                )
    $(eval build-clean      : $(NULL)          ; rm -f $(SYSC_BLD_TARG) )
endef


#-- Run ------------------------------------------------------------------------
define sysc_run_recipe
    @ $(strip $(call print_separator_bash,begin sysc_run_recipe))
    rm -f $(SIM_WRK_DIR)/*.vcd
    set -o pipefail && $(SYSC_BLD_EXE) $(SYSC_RUN_ARGS)
    @ $(strip $(call print_separator_bash,end sysc_run_recipe))
endef

define sysc_rerun_recipe
    rm -f $(SIM_WRK_DIR)/*.vcd
    $(sysc_run_recipe)
endef

define sysc_run_hints
    $(eval $(call hints_targ , run   , Run testbench executable    ))
    $(eval $(call hints_targ , rerun , Re-run testbench executable ))
endef

define sysc_run_rules
    $(eval $(SYSC_RUN_TARG) : $(SYSC_RUN_REQS) ; $$(sysc_run_recipe)   )
    $(eval run              : $(SYSC_RUN_TARG) ; $(NULL)               )
    $(eval rerun            : $(SYSC_RUN_TARG) ; $$(sysc_rerun_recipe) )
endef

export SYSTEMC_INCLUDE := $(SYSC_INC_DIR)
export SYSTEMC_LIBDIR  := $(SYSC_LIB_DIR)

$(eval $(call make_pkg_export_var, SC_COPYRIGHT_MESSAGE, DISABLE  ))

INCLUDED_SYSTEMC := $(TRUE)

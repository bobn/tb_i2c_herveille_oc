
# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

ifneq ($(INCLUDED_VERILATOR),)
    $(warning verilator included earlier)
endif

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(INCLUDED_HINTS),)
    include bin/include-hints.mk
endif

ifeq ($(INCLUDED_SEPARATOR),)
    include bin/include-separator.mk
endif

ifeq ($(INCLUDED_CONFIG),)
    include bin/include-config.mk
endif

ifeq ($(CFG_VLTR),)
    CFG_VLTR := $(CFG)/Install/Verilator
endif

ifeq ($(GENHTML),)
    GENHTML := $(call exe_chk, genhtml)
endif



# -- Integrating Verilator -----------------------------------------------------
#
# 1. Configuration files that must exist (empty okay)
#
#     | File                                         | Description
#     |----------------------------------------------|--------------------------------
#     | cfg/Testbench/active_testbench               | Active testbench
#     | cfg/Testbench/$BENCH_NAME/active_test        | Active test name
#     | cfg/Testbench/$BENCH_NAME/cover_lines        | Enable line coverage
#     | cfg/Testbench/$BENCH_NAME/cover_toggles      | Enable toggle coverage
#     | cfg/Testbench/$BENCH_NAME/lib_hdr            | See include file for SystemC
#     | cfg/Testbench/$BENCH_NAME/lib_obj            | See include file for SystemC
#     | cfg/Testbench/$BENCH_NAME/link_libs          | See include file for SystemC
#     | cfg/Testbench/$BENCH_NAME/tb_csrc            | Testbench C++ code
#     | cfg/Testbench/$BENCH_NAME/tb_hsrc            | Testbench C++ headers
#     | cfg/Testbench/$BENCH_NAME/tb_vinc            | Testbench SystemVerilog include dirs (-y)
#     | cfg/Testbench/$BENCH_NAME/tb_vsrc            | Testbench SystemVerilog sources
#     | cfg/Testbench/$BENCH_NAME/tb_vsrc_top        | SystemVerilog top module
#     | cfg/Testbench/$BENCH_NAME/Defines/PlusArgs   | File names used as plusarg keys containing value strings
#     | cfg/Testbench/$BENCH_NAME/Defines/Parameters | File names used as plusarg keys and parameters containing value strings
#
#
# 2. Configuration files that may exist
#
#     | File                                           | Description
#     |------------------------------------------------|--------------------------------
#     | cfg/Testbench/$BENCH_NAME/Defines/PlusArgs/*   | File names used as plusarg keys, containing value strings
#     | cfg/Testbench/$BENCH_NAME/Defines/Parameters/* | File names used as plusarg and parameter keys, containing value strings
#
#     Warning
#         1. As currently implemented, make-env cannot pass <size>'<base><value>
#            format arguments to Verilator
#
#     Notes
#         1. Parameters are used by Verilator; they are passed into Verilator
#            using "-pvalue+<name>=<value>" arguments
#         2. PlusArgs and Parameters are passed into the SystemC testbench using
#            "+<name>=<value>" arguments; these values are accessed in SystemC
#            by calling the Verilated::commandArgsPlusMatch() function, and
#            accessed in the Verilated model using the $value$plusargs() task
#         3. See SystemC include file for PlusArgs implementation
#
#
# 3. Variables that must be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | SIM_WRK_DIR        | 
#     | SIM_CFG_DIR        | 
#
#
# 4. Variables that may be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | CFG_VLTR           | 
#
#
# 5. Add hints to Makefile
#     * Add the following to the '$(eval $(call hints_targ, ...' sequence
#
#         $(vltr_lib_hints)
#         $(vltr_cov_hints)
#
#
# 6. Add rules to Makefile
#     * Add the following to the rules
#
#         $(vltr_lib_rules)
#         $(vltr_cov_rules)
#
#--


VLTR_VERS_CFG    := $(CFG_VLTR)/version
VLTR_VERS_DFLT   := v4.226
VLTR_VERS_SEL    := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_VERS_CFG),$(VLTR_VERS_DFLT))))
VLTR_HOME_CFG    := $(CFG_VLTR)/home
VLTR_HOME_DFLT   := /opt/verilator
VLTR_HOME_SEL    := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_HOME_CFG),$(VLTR_HOME_DFLT))))

VLTR_BIN_PATH    := $(VLTR_HOME_SEL)/$(VLTR_VERS_SEL)/bin
VLTR_INC         := $(VLTR_HOME_SEL)/$(VLTR_VERS_SEL)/share/verilator/include
VLTR_INC_STD     := $(VLTR_HOME_SEL)/$(VLTR_VERS_SEL)/share/verilator/include/vltstd
VLTR_CFLAGS      := $(if $(FALSE),$(foreach FLAG,$(CFLAGS),-CFLAGS $(FLAG)))

VLTR_COV_L_CFG   := $(SIM_CFG_DIR)/cover_lines
VLTR_COV_L_DFLT  := 0
VLTR_COV_L_SEL   := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_COV_L_CFG),$(VLTR_COV_L_DFLT))))
VLTR_COV_T_CFG   := $(SIM_CFG_DIR)/cover_toggles
VLTR_COV_T_DFLT  := 0
VLTR_COV_T_SEL   := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_COV_T_CFG),$(VLTR_COV_T_DFLT))))
VLTR_COV_REQ     := $(or $(call cast_bool,$(VLTR_COV_L_SEL)),$(call cast_bool,$(VLTR_COV_T_SEL)))

VLTR_PARM_DIR    := $(SIM_CFG_DIR)/Defines/Parameters
VLTR_PARM_CFG    := $(wildcard $(VLTR_PARM_DIR)/*)
VLTR_PARM_LST    := $(notdir $(VLTR_PARM_CFG))

VLTR_TOP_CFG     := $(SIM_CFG_DIR)/tb_vsrc_top
VLTR_TOP_SEL     := $(call get_str, $(VLTR_TOP_CFG))
VLTR_INC_CFG     := $(SIM_CFG_DIR)/tb_vinc
VLTR_INC_LST     := $(call get_list, $(VLTR_INC_CFG))
VLTR_SRC_CFG     := $(SIM_CFG_DIR)/tb_vsrc
VLTR_SRC_LST     := $(call get_list, $(VLTR_SRC_CFG))

VLTR_VOPT        := $(VLTR_CFLAGS) --sc --trace
VLTR_VOPT            += $(if $(call cast_bool,$(VLTR_COV_L_SEL)),--coverage-line,$(NULL))
VLTR_VOPT            += $(if $(call cast_bool,$(VLTR_COV_T_SEL)),--coverage-toggle,$(NULL))
VLTR_VOPT            += $(if $(VLTR_COV_REQ),+define+COVERAGE,$(NULL))
VLTR_VOPT            += $(foreach PARM,$(VLTR_PARM_LST),-pvalue+$(PARM)=$(call get_str,$(VLTR_PARM_DIR)/$(PARM)))
VLTR_OBJS        := verilated.o verilated_vcd_c.o verilated_vcd_sc.o verilated_cov.o

VLTR_REQS        := $(VLTR_SRC_LST) $(VLTR_SRC_CFG) $(SIM_TB_CFG) $(VLTR_COV_L_CFG) $(VLTR_COV_T_CFG) $(VLTR_PARM_CFG)
VLTR_ONLY_TARG   := $(SIM_WRK_DIR)/V$(VLTR_TOP_SEL).mk
VLTR_TARG        := $(SIM_WRK_DIR)/V$(VLTR_TOP_SEL)__ALL.a

$(eval $(call make_pkg_export_prepath, PATH, $(VLTR_BIN_PATH) ))

VERILATOR           := $(call exe_chk, verilator)
VERILATOR_COVERAGE  := $(call exe_chk, verilator_coverage)



# -- Verilated Library ---------------------------------------------------------
define vltr_chk_src_cfg_recipe
    $(if $(VLTR_SRC_CFG),$(NULL),$(error Verilator source config file path variable VLTR_SRC_CFG is empty))
    $(if $(wildcard $(VLTR_SRC_CFG)),$(NULL),$(error Verilator source config path $(VLTR_SRC_CFG) does not exist))
    @echo "[INF] Verilator source config okay."
endef

define vltr_chk_src_list_recipe
    $(if $(VLTR_SRC_LST),$(NULL),$(error No verilator sources in VLTR_SRC_LST))
    @echo "[INF] Verilator source list okay."
endef

define vltr_chk_top_name_cfg_recipe
    $(if $(VLTR_TOP_CFG),$(NULL),$(error Verilator top module name config file variable VLTR_TOP_CFG is empty))
    $(if $(wildcard $(VLTR_TOP_CFG)),$(NULL),$(error Verilator top module name config file $(VLTR_TOP_CFG) does not exist))
    @echo "[INF] Top RTL module name config okay."
endef

define vltr_chk_top_name_recipe
    $(if $(VLTR_TOP_SEL),$(NULL),$(error Verilator top module name variable VLTR_TOP_SEL is empty))
    @echo "[INF] Top RTL module name is $(VLTR_TOP_SEL)."
endef

define vltr_chk_target_path_recipe
    $(if $(VLTR_TARG),$(NULL),$(error Verilator target archive name variable VLTR_TARG is empty))
    @echo "[INF] Target archive name is $(VLTR_TARG)."
endef

define vltr_lib_create_dir_recipe
    $(if $(SIM_WRK_DIR),$(NULL),$(error Verilator working directory path variable SIM_WRK_DIR is empty))
    @echo "[INF] (Re)creating Verilator working directory $(SIM_WRK_DIR)..."
    rm -rf $(SIM_WRK_DIR)
    mkdir -p $(SIM_WRK_DIR)
    @echo "[INF] Verilator working directory created."
endef

define vltr_lib_verilate_recipe
    @echo "[INF] Verilating..."
    $(VERILATOR) $(VLTR_VOPT) --Mdir $(SIM_WRK_DIR) --top-module $(VLTR_TOP_SEL) -y $(VLTR_INC_LST) $(VLTR_SRC_LST)
    @echo "[INF] Done verilating."
endef

# vltr_lib_make_check_recipe <fil>
define vltr_lib_make_check_recipe
    @ echo "[INF] Checking $(strip $(1))..."
    @ test -f $(strip $(1)) || (echo "[ERR] Failed to create $(strip $(1))" ; exit 1)
endef

define vltr_lib_make_recipe
    @echo "[INF] Running make in $(SIM_WRK_DIR)..."
    cd $(SIM_WRK_DIR) && $(MAKE) -f V$(VLTR_TOP_SEL).mk default $(VLTR_OBJS)
    $(call vltr_lib_make_check_recipe, $(VLTR_TARG))
    $(foreach OBJ,$(VLTR_OBJS),$(call vltr_lib_make_check_recipe, $(SIM_WRK_DIR)/$(OBJ))$(MAKE_NL))
    @echo "[INF] Archive okay."
endef

define vltr_lib_recipe
    @ $(strip $(call print_separator_bash,begin vltr_lib_recipe))
    $(vltr_chk_src_cfg_recipe)
    $(vltr_chk_src_list_recipe)
    $(vltr_chk_top_name_cfg_recipe)
    $(vltr_chk_top_name_recipe)
    $(vltr_chk_target_path_recipe)
    $(vltr_lib_create_dir_recipe)
    $(vltr_lib_verilate_recipe)
    $(vltr_lib_make_recipe)
    @ $(strip $(call print_separator_bash,end vltr_lib_recipe))
endef

define vltr_lib_hints
    $(eval $(call hints_targ , verilate       , Create testbench RTL library archive              ))
    $(eval $(call hints_targ , verilate-only  , Verilate testbench RTL and stop                   ))
    $(eval $(call hints_targ , verilate-clean , Eliminate testbench RTL library working directory ))
endef

define vltr_lib_rules
    $(eval $(VLTR_TARG)      : $(VLTR_REQS)      ; $$(vltr_lib_recipe)          )
    $(eval $(VLTR_ONLY_TARG) : $(VLTR_REQS)      ; $$(vltr_lib_verilate_recipe) )
    $(eval verilate          : $(VLTR_TARG)      ; $(NULL)                      )
    $(eval verilate-only     : $(VLTR_ONLY_TARG) ; $(NULL)                      )
    $(eval verilate-clean    : $(NULL)           ; rm -rf $(SIM_WRK_DIR)        )
endef



# -- Coverage Analysis ---------------------------------------------------------
VCOV_DIR      := $(SIM_WRK_DIR)/coverage
VCOV_INFO     := $(VCOV_DIR)/coverage.info
VCOV_ANNOTATE := --annotate $(VCOV_DIR) --annotate-all --annotate-min 1
VCOV_WR_INFO  := --write-info $(VCOV_INFO)

define cov_recipe
    rm -rf $(SIM_WRK_DIR)/coverage
    mkdir $(SIM_WRK_DIR)/coverage
    $(VERILATOR_COVERAGE) $(VCOV_ANNOTATE) $(VCOV_WR_INFO) $(SIM_WRK_DIR)/coverage.dat
    $(GENHTML) $(VCOV_INFO) --output-directory $(VCOV_DIR)
    @echo [INF] HTML at file://$(CWD)/$(VCOV_DIR)/index.html
endef

define vltr_cov_hints
    $(eval $(call hints_targ , cov-report , Create a coverage report at $(VCOV_DIR)/index.html ))
endef

define vltr_cov_rules
    $(eval cov-report : $(NULL) ; $$(cov_recipe) )
endef

INCLUDED_VERILATOR := $(TRUE)

#!/bin/bash
#%# Emit string with git repo status

# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

PROG=${0##*/}
REPO=$1
OPER=$2
VERB=$3

function usage ()
{
    echo "usage: $PROG repo-dir url|branch|status"
    echo "  repo-dir must contain a .git subdirectory"
    echo
    echo "  url     - emit url"
    echo "  branch  - emit branch and branch status"
    echo "  status  - exit with status"
    exit 1
}

function main ()
{
    if [ ! -d "$REPO" ] ; then
        echo "Nil"
        exit 0
    fi

    cd $REPO

    if [ ! -d ".git" ] ; then
        echo "Nil"
        exit 0
    fi

    case $OPER in
        url)
            URL=$(git config remote.origin.url)
            if [ -z "$URL" ] ; then
                echo "Nil"
            else
                echo "$URL"
            fi
            exit 0
        ;;
        branch)
            BRANCH=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
            if [ "$?" != "0" ] ; then
                echo "Nil"
                exit 0
            fi
            if (git diff-index --quiet HEAD 2> /dev/null) ; then
                STATUS="(no pending changes)"
            else 
                STATUS="(pending changes)"
            fi
            echo "$BRANCH $STATUS"
            exit 0
        ;;
        status)
            if (git diff-index --quiet HEAD 2> /dev/null) ; then
                echo "[INF] No pending changes"
                exit 0
            else 
                echo "[ERR] Pending changes"
                exit 1
            fi
        ;;
        *)
            exit 0
        ;;
    esac
}

main

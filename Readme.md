# Anti-features
Investing time in this project, as presented, requires laboring under
the following "features":

* Non-standard build and run environment
* The package manager version of Verilator is not supported
* The package manager version of libsystemc is not supported
* The testbench drives only the I2C byte and bit controllers;
  no bus-level modules are instantiated there
* There are no assertions or functional coverage in the testbench

# Features
* A SystemC/Verilator Testbench with full line and toggle coverage
  of the byte and bit controllers

# Prerequisites
This project was developed under Debian Bullseye with these
upgrades:

* Make 4.4
* Accellera SystemC Reference Implementation version 2.3.3
  with libsystemc.a for static linking
* Verilator version v4.266

# Getting started
Getting started requires modifying the SystemC and Verilator install
configuration.  See the Configuration database section below for details.

1. Run <code>_make_</code> with no targets to initialize the configuration
   database
1. Modify the SystemC install configuration to match the local environment
1. Modify the Verilator install configuration to match the local environment
1. Clone the default I2C RTL repository using <code>_make clones_</code>
1. Build and run the default test using <code>_make run_</code>
1. View waveforms of the verilated model using <code>_make wave_</code>
1. View waveforms of the SystemC testbench using <code>_make swave_</code>

# Make usage overview
Typing <code>_make_</code> on the command line will report a list of
Make targets and hints about what they are used for.

Typing <code>_make show-env_</code> on the command line will report the
current state of the Make environment.

Note that the Makefile SHELL variable is assigned /usr/bin/bash in
bin/include-base.mk.

# Project subdirectories
| Variable  | Value     | Description                                                   |
|-----------|-----------|---------------------------------------------------------------|
| BIN       | bin       | Contains scripts and Makefile includes                        |
| CFG       | cfg       | Contains the project configuration database                   |
| SIM       | sim       | Contains SystemC and SystemVerilog code for the testbench(es) |
| TMP       | tmp       | Contains subdirectories and files created under sim           |
| WAV       | wav       | Contains per-testbench subdirectories for GTKWAVERC files     |

## Reserved subdirectory names
The list of reserved subdirectory names is

* Install

no source-code subdirectories may have one of these names.  This is
because they conflict with directory names ignored by Git.

# Project variables
The variables specified here are used in the following descriptions.

| Variable  | Description                                         |
|-----------|-----------------------------------------------------|
| REPO_TAG  | Git repository tag                                  |
| TOOL      | Tool tag                                            |
| TB_TOP    | Selected testbench                                  |

# Configuration database
Configuration items are specified within files under a configuration
directory hierarchy.  This feature is inspired by Cygwin's Registry API.

The database is used to configure how testbenches are built and run.
It resides within the $CFG directory.  The database is read every time
that <code>_make_</code> is run.

To view the entire configuration database, use <code>_make
show-cfg_</code>.  Other Make targets (show-cfg-install,
show-cfg-testbench) show limited portions of the configuration database.

## Database elements
* Subdirectories
    * Major subdirectories
        * Git
        * Install
        * Testbench
    * Git subdirectories
        * Git subdirectory names are tags for cloned repositories
    * Install subdirectories
        * Install subdirectory names are tags for installed tools
    * Testbench subdirectories
        * Testbench subdirectory names specify testbench names
* Files
    * Files may contain Lists, Strings, or Booleans
    * Whitespace characters are the ASCII characters SPACE, HT, and LF
* File contents
    * List values
        * A file containing multiple strings bounded by whitespace characters
    * String values
        * A file containing a single sequence of non-whitespace characters
    * Boolean values
        * A file containing a string made up of a single "1" or "0" character

## Version control of database elements
Database files may be controlled by Git or may be created
dynamically by Make

* Those controlled by Git
    * Will appear upon clone, as usual
    * When modified, may be retained and shared when the file is part of a Git
      commit
* Those dynamically created
    * Will take a default value upon the first invocation of Make
    * Will, when modified, retain the modified value until modified again

## Modifying configuration files
* Changes can be made to the configuration by modifying the files containing
  List, String, or Boolean values
* A configuration entry file may be changed in a text editor
* Alternatively, a configuration entry file may be changed using a shell redirect
    <pre>
    echo "value" > file
    </pre>

## Configuration directories and files
### $CFG/Git/$REPO_TAG/
Repositories configured here can be cloned into $TMP/$REPO_TAG with the
Make command line <code>_make clones_</code>.  The <code>_clones_</code>
target executes <code>_git clone \-\-branch remote_branch remote_url
$TMP/$REPO_TAG_</code>.

Finer-grain Make targets can be listed with the Make command line
<code>_make clone-hints_</code>.

* remote_branch
    * Specifies the branch to clone
    * String type
    * Controlled by Git
* remote_url
    * Specifies the remote URL to clone from
    * String type
    * Controlled by Git

### $CFG/Install/$TOOL/
For each $TOOL subdirectory, the install directory is _home/version_.
These configuration files are created upon the first invocation of Make
and will need to be modified for the local environment.

* home
    * Specifies the home directory of the installation
    * String type
    * Absoute path
    * Dynamically created
* version
    * Specifies the version within the home directory
    * String type
    * Dynamically created

### $CFG/Testbench/
* active_testbench
    * Specifies which testbench is active at Make run-time
    * String type
    * Dynamically created

### $CFG/Testbench/$TB_TOP/
* active_test
    * Specifies which test is active at Make run-time
    * String type
    * The String must match a test in the top-level SystemC
      testbench source
    * Controlled by Git
* cover_lines
    * Specifies --coverage-line in verilator
    * Boolean type
    * Controlled by Git
* cover_toggles
    * Specifies --coverage-toggle in verilator
    * Boolean type
    * Controlled by Git
* lib_hdr
    * External library header files
    * List type
    * A list of X.h file names
    * Absoute paths
    * Controlled by Git
* lib_obj
    * External library object files
    * List type
    * A list of X.a file names
    * Absoute paths
    * Controlled by Git
* link_libs
    * Linker -l components
    * List type
    * A list in libX format, converted to -lX in Make
    * Controlled by Git
* tb_csrc
    * Testbench C++ code files
    * List type
    * Relative paths
    * Controlled by Git
* tb_hsrc
    * Testbench C++ header files
    * List type
    * Relative paths
    * Controlled by Git
* tb_vinc
    * Testbench SystemVerilog include dirs (-y)
    * List type
    * Relative paths
    * Controlled by Git
* tb_vsrc
    * Testbench SystemVerilog sources
    * List type
    * Relative paths
    * Controlled by Git
* tb_vsrc_top
    * Verilated top module name
    * String type
    * Controlled by Git

### $CFG/Testbench/$TB_TOP/Defines
String type values to be passed into the verilated model or into the
SystemC testbench are specified here.

#### $CFG/Testbench/$TB_TOP/Defines/Parameters
Files in this directory contain String type content.  This content is
passed to verilator as -pvalue parameters.  It is also passed to the
SystemC testbench executable as PlusArgs.

For each file in this directory, a
<code>-pvalue+_file_name=file_content_</code> parameter is added to the
verilator command line.  Also, a <code>_+file_name=file_content_</code>
parameter is added the the SystemC testbench executable command line.

#### $CFG/Testbench/$TB_TOP/Defines/PlusArgs
Files in this directory contain String type content.  This content is
passed to the SystemC testbench executable as PlusArgs.

For each file in this directory, a <code>_+file_name=file_content_</code>
parameter is added the the SystemC testbench executable command line.

### $CFG/magic
String type value marking the $CFG directory as a valid Configuration
database.

# HOWTOs
## Coverage
You need coverage statistics:

1. Set $CFG/Testbench/$TB_TOP/cover_lines to 1
    <pre>
    echo 1 > Set $CFG/Testbench/$TB_TOP/cover_lines
    </pre>
1. Set $CFG/Testbench/$TB_TOP/cover_toggles to 1
    <pre>
    echo 1 > Set $CFG/Testbench/$TB_TOP/cover_toggles
    </pre>
1. <code>_make run_</code>
1. <code>_make cov-report_</code>
    * Note the file:/// URL reported at the end of
    the Make run
1. View the HTML coverage report at the file:/// URL

## Create a new testbench
You need to add another testbench $TB_TOP:

1. Create and populate the testbench code sub-directory $SIM/tb/$TB_TOP
    * The top SystemC testbench source file must be named tb.cxx
    * Test name "test-name" is found within tb.cxx; the utility
      $BIN/test-sel.pl finds test names using the patterns
        1. <code>/\* TEST_SEL_ITEM \*/</code>
        1. <code>(sel == "test-name")</code>
1. Create and populate the testbench config sub-directory $CFG/Testbench/$TB_TOP
1. Create and populate the GTKW sub-directory $WAV/$TB_TOP
    * Optional, for use with <code>_make wave_</code>,
    <code>_make swave_</code>, and <code>_make twave_</code>.
1. Change the active testbench to $TB_TOP
    <pre>
    make show-env
    echo $TB_TOP > $CFG/Testbench/active_testbench
    make show-env
    </pre>
1. Change the active test
    <pre>
    make show-env
    echo test-name > $CFG/Testbench$TB_TOP/active_test
    make show-env
    </pre>

## Report a Makefile variable
You need the value of a Make variable.

Any Make variable VAR within the Makefiles or Makefile includes
can be reported using <code>_make dbg-VAR_</code>.  For instance:

<pre>
    make dbg-SIM_TB_SEL
</pre>

reports

<pre>
    SIM_TB_SEL = "tb_01"
</pre>

## Run a shell command in the environment created by the Makefile
You need to run a shell CLI command in the Makefile environment.

A shell command can be evaluated in the Makefile environment using
the Make target 'eval'.  For instance:

<pre>
    make eval 'verilator --help'
</pre>

reports

<pre>
    Verilator 4.226 2022-08-31 rev v4.226
</pre>


# Licenses

## License for code

The code in this project is licensed under the GPLv3

## License for this project summary

This Readme is licensed under the Creative Commons Attribution-ShareAlike 3.0
Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/. 

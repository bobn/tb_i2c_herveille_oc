
# Copyright 2023 Robert Newgard
# 
# This file is part of tb_i2c_herveille_oc.
# 
# tb_i2c_herveille_oc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# tb_i2c_herveille_oc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with tb_i2c_herveille_oc.  If not, see <http://www.gnu.org/licenses/>.

# -- Make Base Package ---------------------------------------------------------
include bin/include-base.mk


# -- Make Environment ----------------------------------------------------------
DATE      := $(call exe_chk, date)
HOSTNAME  := $(call exe_chk, hostname)
SED       := $(call exe_chk, sed)
TREE      := $(call exe_chk, tree)
VIRT_CK   := $(call exe_chk, systemd-detect-virt)

HINTS_TF  := %-24s
ENV_TF    := %-24s
PPR4      := pr -t -o 4
PPR8      := pr -t -o 8
PPR12     := pr -t -o 12
PPR16     := pr -t -o 16

include bin/include-hints.mk
include bin/include-separator.mk
include bin/include-config.mk
include bin/include-sed.mk

define show_tree_bash
    IGN="" ;
    set -f ;
    for PAT in $$(cat .gitignore) ; do
        ITM=$$(echo $$PAT | $(SED) 's|/||') ;
        if [ -z $$IGN ] ; then
            IGN=$$ITM ;
        else
            IGN=$$(printf "%s|%s" $$IGN $$ITM) ;
        fi ;
    done ;
    set +f ;
    $(TREE) --noreport -fCI "$$IGN" . ;
endef



# -- Project Environment -------------------------------------------------------
AU  := au
AG  := ag
WAV := wav

CFG_SHOW_EXE     := $(call exe_chk, cfg-show.bash)
GIT_REPO_EXE     := $(call exe_chk, git-repo.bash)
TEST_SEL_EXE     := $(call exe_chk, test-sel.pl)
EXPORT_SHOW_EXE  := $(call exe_chk, export-show.pl)

include bin/include-git.mk

define show_env_bash
    printf "\n" ;
    printf "$(ENV_TF) %s\n" "Host Name:"            "$$($(HOSTNAME))" ;
    printf "$(ENV_TF) %s\n" "Virtualization:"       "$$($(VIRT_CK))" ;
    printf "$(ENV_TF) %s\n" "bash Version:"         "$$(echo $$BASH_VERSION)" ;
    printf "$(ENV_TF) %s\n" "make Version:"         "$(MAKE_VERSION)" ;
    printf "$(ENV_TF) %s\n" "Verilator Version:"    "$(VLTR_VERS_SEL)" ;
    printf "$(ENV_TF) %s\n" "SystemC Version:"      "$(SYSC_VERS_SEL)" ;
    printf "$(ENV_TF) %s\n" "Project Clone URL:"    "$$($(GIT_REPO_EXE) . url)" ;
    printf "$(ENV_TF) %s\n" "Project Clone Branch:" "$$($(GIT_REPO_EXE) . branch)" ;
    printf "\n" ;
    printf "%-12s %s\n" "Exported Variables:"   "$(NULL)" ;
    printf "%-12s %s\n" "------------------"    "$(NULL)" ;
    $(EXPORT_SHOW_EXE) $(SH_VARS) | $(PPR4) ;
    $(show_git_env_bash)
    printf "\n" ;
    printf "%-12s %s\n" "Testbench"             "$(NULL)" ;
    printf "%-12s %s\n" "---------"             "$(NULL)" ;
    printf "%-12s %s\n" "Active:"               "$(SIM_TB_SEL) ($(SIM_TB_CFG))" | $(PPR4) ;
    printf "%-12s %s\n" "Coverage:"             "$(if $(VLTR_COV_REQ),enabled,disabled)" | $(PPR4) ; 
    printf "%-12s %s\n" "Available:"            "$(NULL)"  | $(PPR4) ;
    (cd $(CFG)/Testbench && ls -dC $(SIM_TB_ALL) | $(PPR8)) ;
    printf "\n" ;
    printf "%-12s %s\n" "Test"                  "$(NULL)" | $(PPR4);
    printf "%-12s %s\n" "----"                  "$(NULL)" | $(PPR4);
    printf "%-12s %s\n" "Active:"               "$(SIM_TEST_SEL) ($(SIM_TEST_CFG))" | $(PPR8) ;
    printf "%-12s %s\n" "Available:"            "$(NULL)"  | $(PPR8) ;
    $(TEST_SEL_EXE) sim/tb/$(SIM_TB_SEL)/tb.cxx | $(PPR12) ;
    printf "\n" ;
endef



# -- Simulation -----------------------------------------------------------------
SIM_TB_CFG        := $(CFG)/Testbench/active_testbench
SIM_TB_DFLT       := tb_01
SIM_TB_SEL        := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SIM_TB_CFG),$(SIM_TB_DFLT))))
SIM_TB_ALL        := $(notdir $(filter-out $(SIM_TB_CFG),$(wildcard $(CFG)/Testbench/*)))
SIM_CFG_DIR       := $(CFG)/Testbench/$(SIM_TB_SEL)
SIM_TEST_CFG      := $(SIM_CFG_DIR)/active_test
SIM_TEST_DFLT     := regression
SIM_TEST_SEL      := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SIM_TEST_CFG),$(SIM_TEST_DFLT))))
SIM_WRK_DIR       := $(TMP)/sysc/$(SIM_TB_SEL)/vltd
SIM_VCD_FNAM_VLTR := trace_vltr.vcd
SIM_VCD_FNAM_SYSC := trace_sysc.vcd
SIM_VCD_VLTR      := $(SIM_WRK_DIR)/$(SIM_VCD_FNAM_VLTR)
SIM_VCD_SYSC      := $(SIM_WRK_DIR)/$(SIM_VCD_FNAM_SYSC)
SIM_WAV_DIR       := $(WAV)/$(SIM_TB_SEL)
SIM_GTKW_VLTR     := $(SIM_WAV_DIR)/trace_vltr.gtkw
SIM_GTKW_SYSC     := $(SIM_WAV_DIR)/trace_sysc.gtkw



# -- Verilated RTL --------------------------------------------------------------
include bin/include-verilator.mk



# -- SystemC Testbench Build and Run --------------------------------------------
include bin/include-systemc.mk



# -- Waveforms -----------------------------------------------------------------
include bin/include-gtkwave.mk



# -- Eval Package --------------------------------------------------------------
include bin/include-eval.mk



# -- Hints ----------------------------------------------------------------------
$(eval $(call hints_targ , show-cfg           , Show project config - aka 'sc'                          ))
$(eval $(call hints_targ , show-cfg-git       , Show project git config - aka 'scg'                     ))
$(eval $(call hints_targ , show-cfg-install   , Show project install config - aka 'sci'                 ))
$(eval $(call hints_targ , show-cfg-testbench , Show project testbench config - aka 'sct'               ))
$(eval $(call hints_targ , show-tree          , Show managed files in tree - aka 'st'                   ))
$(eval $(call hints_targ , show-git           , Show git status - aka 'sg'                              ))
$(eval $(call hints_targ , show-env           , Show simulation environment - aka 'se'                  ))
$(eval $(call hints_targ , clone-hints        , Hints for cloning individual auxiliary Git repos        ))
$(eval $(call hints_targ , clones             , Clone all auxiliary Git repos                           ))
$(eval $(call hints_targ , clones-clean       , Eliminate all cloned repos                              ))

$(vltr_lib_hints)
$(vltr_cov_hints)
$(sysc_build_hints)
$(sysc_run_hints)
$(wave_hints)



# -- rules ---------------------------------------------------------------------
nil                    : $(NULL)               ; @true
dbg-%                  : $(NULL)               ; @echo '$* = "$(strip $($*))"'
show-cfg               : $(NULL)               ; @ $(CFG_SHOW_EXE)
sc                     : show-cfg              ; $(NULL)
show-cfg-install       : $(NULL)               ; @ $(CFG_SHOW_EXE) $(CFG)/Install
sci                    : show-cfg-install      ; $(NULL)
show-cfg-testbench     : $(NULL)               ; @ $(CFG_SHOW_EXE) $(CFG)/Testbench/$(SIM_TB_SEL)
sct                    : show-cfg-testbench    ; $(NULL)
show-cfg-git           : $(NULL)               ; @ $(CFG_SHOW_EXE) $(CFG)/Git
scg                    : show-cfg-git          ; $(NULL)
show-env               : $(NULL)               ; @ ($(strip $(show_env_bash))) | $(PPR4)
se                     : show-env              ; $(NULL)
show-tree              : $(NULL)               ; @ $(strip $(show_tree_bash))
st                     : show-tree             ; $(NULL)
show-git               : $(NULL)               ; $(show_git_recipe)
sg                     : show-git              ; $(NULL)
clone-hints            : $(GIT_HINTS)          ; $(NULL)
clones                 : $(GIT_TARGS)          ; $(NULL)
clones-clean           : $(GIT_CLEANS)         ; $(NULL)

$(hints_rules)
$(vltr_lib_rules)
$(vltr_cov_rules)
$(sysc_build_rules)
$(sysc_run_rules)
$(wave_rules)

.PHONY : nil sc sci scg st sg se $(PHONYS)
.DEFAULT_GOAL := hints
